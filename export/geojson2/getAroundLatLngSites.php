<?phpheader('Content-type: application/json; charset=utf-8');include "../../config/connection.php";$latref = 3.14159265 * $_GET['lat']   / 180;$lngref= 3.14159265 * $_GET['lng']   / 180;$distance = $_GET['distance'];$count=0;$style='brief';

if (isset ($_GET['style'] )) $style= $_GET['style'] ; 

$query = "SELECT id, lat, lng FROM site where lat!= 0 and lng != 0";
$result = mysqli_query($bdd, $query);

while ($fetchR = mysqli_fetch_array($result)){

	$latRad = 3.14159265 * $fetchR['lat']   / 180;
	$lngRad = 3.14159265 * $fetchR['lng']   / 180;
	$d = round(6366 * acos(cos($latRad) * cos($latref) * cos($lngref-$lngRad) + sin($latRad) * sin($latref)),2);

	if ($d < $distance){
	  
	  $qDetail = "SELECT id, name, iso,
	  lat, lng, takeoff_altitude,
	  landing_lat, landing_lng, landing_altitude,
	  takeoff, landing,
	  N, NE, E, SE, S, SW, W, NW,
	  hike, soaring, thermal, xc, flatland, winch, hanggliding,
	  comments, access, rules, contact, weather, web
		FROM site
		WHERE id = ".$fetchR['id'];
	  $rDetail = mysqli_query($bdd, $qDetail);
	  
	  while ($val = mysqli_fetch_array($rDetail)) {
	  
		$distance_meter[$count] = $d * 1000;
		$name[$count] = $val['name'];
		$id_site[$count] = $val['id'];
		$iso[$count] = $val['iso'];

		$lng[$count] = $val['lng'];
		$lat[$count] = $val['lat'];
		$takeoff_altitude[$count] = $val['takeoff_altitude'];
		
		$landing_lat[$count] = $val['landing_lat'];
		$landing_lng[$count] = $val['landing_lng'];
		$landing_altitude[$count] = $val['altitude_landing'];
		
		$landing_description[$count] = $val['landing'];
		$takeoff_description[$count] = $val['takeoff'];

		$N[$count] = $val['N'];
		$NE[$count] = $val['NE'];
		$E[$count] = $val['E'];
		$SE[$count] = $val['SE'];
		$S[$count] = $val['S'];
		$SW[$count] = $val['SW'];
		$W[$count] = $val['W'];
		$NW[$count] = $val['NW'];

		$hike[$count] = $val['hike'];
		$soaring[$count] = $val['soaring'];
		$thermal[$count] = $val['thermal'];
		$xc[$count] = $val['xc'];
		$flatland[$count] = $val['flatland'];
		$winch[$count] = $val['winch'];
		$hanggliding[$count] = $val['hanggliding'];

		$takeoff_going_there[$count] = $val['access'];
		$takeoff_flight_rules[$count] = $val['rules'];
		$takeoff_contacts[$count] = $val['contact'];
		$takeoff_comments[$count] = $val['comments'];
		$weather[$count] = $val['weather'];
		
		$web[$count] = $val['web'];
		
		$count++;
	}
  }
}

$limit= count($name);

if (isset ($_GET['limit'])) { $limit = min ($_GET['limit'], count ($name));}

if ($limit>0) {
	array_multisort( $distance_meter, SORT_ASC,
		$name, $iso, $id_site,
		$lat, $lng, $takeoff_altitude, 
		$landing_lat, $landing_lng, $landing_altitude, 
		$takeoff_description, $landing_description,
		$hike, $soaring, $thermal, $xc ,$flatland,$winch, $hanggliding,  
		$takeoff_comments, $weather, $takeoff_going_there, $takeoff_flight_rules, $takeoff_contacts,
		$web,
		$N,$NE,$E,$SE,$S,$SW,$W,$NW
	);
}
$geojson = array( 'type' => 'FeatureCollection', 'features' => array());

for ($k=0;$k<$limit;$k++){
   $marker = array(
         'type' => 'Feature',      'features' => array(		'type' => 'Feature',        'properties' => array(			'name' => "$name[$k]", 		//json_encode($name[$k]), 			'place' => "paragliding takeoff", 					'distance' => "$distance_meter[$k]", 			'countryCode' => "".$iso[$k]."", 			'takeoff_altitude' => "".$takeoff_altitude[$k]."", 			'takeoff_description' => "$takeoff_description[$k]", 
			'N'  =>  "$N[$k]",
			'NE' => "$NE[$k]",
			'E'  =>  "$E[$k]",
			'SE' => "$SE[$k]",
			'S'  =>  "$S[$k]",
			'SW' => "$SW[$k]",
			'W'  =>  "$W[$k]",
			'NW' => "$NW[$k]",
			'paragliding' => "1", 			'hanggliding' => "".$hanggliding[$k]."", 			'thermals' => "".$thermal[$k]."", 			'soaring' => "".$soaring[$k]."", 			'winch' => "".$winch[$k].""         ),        'geometry' => array(            'type' => 'Point',            'coordinates' => array(                $lng[$k] + 0.0,               $lat[$k] + 0.0            )        )     )   );
      array_push($geojson['features'], $marker['features']); 
}

echo json_encode($geojson);

?>
