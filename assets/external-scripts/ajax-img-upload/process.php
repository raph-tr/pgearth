<?php
session_start();

include '../../../config/connection.php';

function getLastImageIndex( $siteId ) {   // get last image stored in db
	$qPic = "SELECT MAX(number) as maxNumber FROM `picture` WHERE `site` = ".$siteId;
	$rPic = mysqli_query($bdd, $qPic);
	$vPic = mysqli_fetch_array($rPic);
	return $vPic['maxNumber'];
}



############ Configuration ##############
$config["generate_image_file"]			= true;
$config["generate_thumbnails"]			= false;
$config["image_max_size"] 			= 1024; //Maximum image size (height and width)
$config["thumbnail_size"]  			= 100; //Thumbnails will be cropped to 200x200 pixels
$config["thumbnail_prefix"]			= "thumb_"; //Normal thumb Prefix
$config["destination_folder"]			= '../../img/sites-pictures/'; //upload directory ends with / (slash)
$config["thumbnail_destination_folder"]		= 'uploads/'; //upload directory ends with / (slash)
$config["upload_url"] 				= "http://paragliding.earth/assets/img/sites-pictures/"; 
$config["quality"] 				= 90; //jpeg quality
$config["random_file_name"]			= true; //randomize each file name


if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
	exit;  //try detect AJAX request, simply exist if no Ajax
}

//specify uploaded file variable
$config["file_data"] = $_FILES["__files"]; 

//pge site id
$config["siteId"] = $_POST['siteId'];

//pge last pic in db index
$config["lastPic"] = $_POST['lastPic'];


//include sanwebe impage resize class
include("resize.class.php"); 


//create class instance 
$im = new ImageResize($config); 


try{
	
	$responses = $im->resize(); //initiate image resize
	
	//echo '<strong>Image(s) uploaded, add comment(s) if you wish :</strong><br />';
	$returnedImages=0;
	$strImagesOutput = "";
	//output thumbnails
	
	require '../../../config/connection.php';
	
    //print_r($responses["images"]);
	foreach($responses["images"] as $response){
	//	print_r($response);
		$returnedImages++;
		$strImagesOutput .= ' <img src="'.$config["upload_url"].$response["name"].'" width="100px" title="'.$response["name"].'" />  ';
        $strImagesOutput .= ' <input type="text" name="'.$response["name"].'"></input> <br/>';	
		$qImg = "INSERT INTO `picture` 
						(`id`, `name`, `site`, `pro`, `number`, `date`, `author`, `comment`)
				 VALUES (NULL, '".$response["name"]."', '".$response["site"]."', '0', '".$response["index"]."', CURRENT_DATE(), '".$_SESSION["userId"]."', '');";
		if ( ! $rImg = mysqli_query($bdd, $qImg)) echo " debug : Problem saving img in db !<br />".$qImg;
		
		$changed = 'picture';
		$id = $response["site"];
		include("../../ajax/updateItem/saveSiteEditedByUser.php");

	}
		
	echo '<br /><div id="imageUploadResults"><div><strong><span id="processedImages">'.$returnedImages.'</span> image(s) uploaded, add comment(s)</strong></div>';
	echo "<form id='commentsForm'>";
	echo $strImagesOutput;
	echo "<input type='Submit'  class='button' />";
	echo "</form></div>";

}catch(Exception $e){
	echo '<div class="error">';
	echo $e->getMessage();
	echo '</div>';
}

		
?>
