<?php
//header ("Content-type: image/png");
include ("../../../config/connection.php");

$id_site = $_GET['site'];

$img_height=$size+2;
$img_width=$size+2;

$img_size=$_GET['size'];

$img_width= $img_size;
$y_offset = 25;
$img_height= $img_size/2 + $y_offset;

$space = floor(0.15 * $img_width/12);

// create image
$image_source = imagecreatefrompng ("calendar_linear_bg.png")
	 or die ("Impossible de cr�e un flux d'image GD");

// chargement
$image = imagecreatetruecolor($img_width, $img_height);

// Redimensionnement
imagecopyresized($image, $image_source, 0, 0, 0, 0, $img_width, $img_height-$y_offset, 240, 240);

// colors
$grey		= imagecolorallocate ($image, 197, 197, 197); 
$lightgrey	= imagecolorallocate ($image, 220, 220, 220); 
$darkgrey	= imagecolorallocate ($image, 51, 51, 51); 
$blue 		= imagecolorallocate ($image, 12, 12, 255); 
$white 		= imagecolorallocate ($image, 255, 255, 255); 

// create imagebackground
$image_bg = @imagecreatetruecolor ($img_width, $img_height)
	 or die ("Impossible de cr�e un flux d'image GD");

//transparent color
$transparent_color = imagecolorallocate($image, 255, 255, 255);
imagecolortransparent ( $image , $transparent_color);    

//transparent color_bg
$transparent_color_bg = imagecolorallocate($image_bg, 255,255,255);
imagecolortransparent ( $image_bg , $lightgrey); 
imagefilledrectangle( $image_bg, 0, 0, $img_width, $img_height, $lightgrey);
imagefilledrectangle( $image, 0, $img_height-$y_offset, $img_width, $img_height, $lightgrey);

//$id_site = $val_sites['id'];

// draw the red/yellow/green shape
for ($i=1;$i<13;$i++){

	//make grey rectangles around the bars
	$top_x = (($i-1)*$img_width/12)-$space;
	$down_x = (($i-1)*$img_width/12)+$space;
	imagefilledrectangle( $image, $top_x, 0, $down_x, $img_height, $lightgrey);
	// end of make grey rectangles around the bars

	$query  = "select month_$i from site where id = $id_site";
	$result = mysqli_query($bdd, $query);
	$val    = mysqli_fetch_array($result);
	
	// echo "<br />$i : ".$val['month_'.$i];

	// erase top of columns
	$top_x = (($i-1)*$img_width/12)+$space;
	$down_x = ($i*$img_width/12)-$space;
	if (! is_null($val['month_'.$i])) {
		$down_y = ($img_height-$y_offset) * ((3-$val['month_'.$i]) / 4 );
	} else $down_y = ($img_height-$y_offset);
	
	imagefilledrectangle( $image, $top_x, 0, $down_x, $down_y, $lightgrey);
	// end of erase top of columns

	//draw blue line on the left of the column
	if (! is_null($val['month_'.$i])) imageline( $image, $top_x, $down_y+2, $top_x,  $img_height-$y_offset, $blue);
	//else imageline( $image, $top_x,  $img_height-$y_offset, $top_x, $down_y-2, $blue);
	//draw blue line at the bottom of column
	imageline( $image, $down_x,  $img_height-$y_offset, $top_x,  $img_height-$y_offset, $blue);
	//imageline( $image, $down_x, $down_y, $down_x,  $img_height-10, $blue);

}
// end of draw the red/yellow/green shape

//erase the last part after 12th month
imagefilledrectangle( $image, $img_width-$space, 0, $img_width, $img_height, $lightgrey);
//end of erase the last part after 12th month

//draw levels
if ($_GET['level']=="yes"){
	for ($i=0; $i<4; $i++){
		imageline($image, 0, ($img_height-$y_offset) * $i / 4, $img_width, ($img_height-$y_offset) * $i / 4, $grey);
	}
}
//end of draw levels

//write months name
if ($_GET['text']=="yes"){
	imagestringup($image, 1, -1, $img_height-6, 'Jan', $darkgrey);
	imagestringup($image, 1, $img_width/4, $img_height-6, 'Apr', $darkgrey);
	imagestringup($image, 1, $img_width/2, $img_height-6, 'Jul', $darkgrey);
	imagestringup($image, 1, $img_width-8, $img_height-7, 'Dec', $darkgrey);
}
//end of write months name


imagecopy ($image_bg,
		$image,
		0, 0,
		0, 0,
		$img_size,$img_size);

// save the image
//imagepng($image_bg);
if ( imagepng($image_bg, $_GET['size'].'/'.$id_site.'.png') ) echo  $_GET['size'].'/'.$id_site.'.png';
else "no";
?>
