<?php

include("../../config/connection.php");
$size = $_GET['size'];

$id = $_GET['id'];

$img_height=$size+2;
$img_width=$size+2;




if ($id=="all") {
	$query  = "select id,N,E,S,W,NE,SE,SW,NW from site";
	$result = mysqli_query($bdd, $query);
	while ($val=mysqli_fetch_array($result)){


		// create image
		$image = @imagecreatetruecolor ($img_width, $img_height)
			 or die ("Impossible de crée un flux d'image GD");
			 
		$bg = imagecolorallocate($image,0,0,0);

		imagecolortransparent($image, $bg);

		$white = imagecolorallocate ($image, 255, 255, 255);
		$lightgreen = imagecolorallocate ($image, 107, 255, 107);
		$green = imagecolorallocate ($image, 1, 187, 1);
		$gray = imagecolorallocate ($image, 187, 187, 187);
		$darkgray = imagecolorallocate ($image, 127, 127, 127);

		//imagefilledellipse($image, $img_width/2, $img_width/2, $size, $size, $gray);
		imagefilledellipse($image, $img_width/2, $img_width/2, $size*0.75, $size*0.75, $white);

		if ($val['N'] == "1") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 248, 293, $lightgreen, IMG_ARC_PIE);
		if ($val['NE']== "1") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 293, 338, $lightgreen, IMG_ARC_PIE);
		if ($val['E'] == "1") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 338, 23, $lightgreen, IMG_ARC_PIE);
		if ($val['SE']== "1") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 23, 68, $lightgreen, IMG_ARC_PIE);
		if ($val['S'] == "1") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 68, 113, $lightgreen, IMG_ARC_PIE);
		if ($val['SW']== "1") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 113, 158, $lightgreen, IMG_ARC_PIE);
		if ($val['W'] == "1") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 158, 203, $lightgreen, IMG_ARC_PIE);
		if ($val['NW']== "1") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 203, 248, $lightgreen, IMG_ARC_PIE);

		if ($val['N'] =="2") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 248, 293, $green, IMG_ARC_PIE);
		if ($val['NE']=="2") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 293, 338, $green, IMG_ARC_PIE);
		if ($val['E'] =="2") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 338, 23, $green, IMG_ARC_PIE);
		if ($val['SE']=="2") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 23, 68, $green, IMG_ARC_PIE);
		if ($val['S'] =="2") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 68, 113, $green, IMG_ARC_PIE);
		if ($val['SW']=="2") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 113, 158, $green, IMG_ARC_PIE);
		if ($val['W'] =="2") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 158, 203, $green, IMG_ARC_PIE);
		if ($val['NW']=="2") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 203, 248, $green, IMG_ARC_PIE);

		imagefilledellipse($image, $img_width/2, $img_width/2, $img_width/2, $img_width/2, $bg);

		if ($_GET['axes']=="yes"){
		   imageline($image, $img_width/2, 0, $img_width/2, $img_width, $darkgray);
		   imageline($image, 0, $img_width/2, $img_width, $img_width/2, $darkgray);
		}

		if (imagepng($image,'windrose/'.$size.'/'.$val['id'].'.png')) echo $val['id'].".png  done!<br />";
		
	}	
} else {

	// create image
	$image = @imagecreatetruecolor ($img_width, $img_height)
		 or die ("Impossible de crée un flux d'image GD");
		 
	$bg = imagecolorallocate($image,0,0,0);

	imagecolortransparent($image, $bg);

	$white = imagecolorallocate ($image, 255, 255, 255);
	$lightgreen = imagecolorallocate ($image, 107, 255, 107);
	$green = imagecolorallocate ($image, 1, 187, 1);
	$gray = imagecolorallocate ($image, 187, 187, 187);
	$darkgray = imagecolorallocate ($image, 127, 127, 127);

	//imagefilledellipse($image, $img_width/2, $img_width/2, $size, $size, $gray);
	imagefilledellipse($image, $img_width/2, $img_width/2, $size*0.75, $size*0.75, $white);


    $query  = "select N,E,S,W,NE,SE,SW,NW from site where id=".$id."";
    $result = mysqli_query($bdd, $query);
    $val    = mysqli_fetch_array($result);

	if ($val['N'] == "1") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 248, 293, $lightgreen, IMG_ARC_PIE);
	if ($val['NE']== "1") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 293, 338, $lightgreen, IMG_ARC_PIE);
	if ($val['E'] == "1") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 338, 23, $lightgreen, IMG_ARC_PIE);
	if ($val['SE']== "1") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 23, 68, $lightgreen, IMG_ARC_PIE);
	if ($val['S'] == "1") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 68, 113, $lightgreen, IMG_ARC_PIE);
	if ($val['SW']== "1") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 113, 158, $lightgreen, IMG_ARC_PIE);
	if ($val['W'] == "1") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 158, 203, $lightgreen, IMG_ARC_PIE);
	if ($val['NW']== "1") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 203, 248, $lightgreen, IMG_ARC_PIE);

	if ($val['N'] == "2") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 248, 293, $green, IMG_ARC_PIE);
	if ($val['NE']== "2") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 293, 338, $green, IMG_ARC_PIE);
	if ($val['E'] == "2") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 338, 23, $green, IMG_ARC_PIE);
	if ($val['SE']== "2") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 23, 68, $green, IMG_ARC_PIE);
	if ($val['S'] == "2") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 68, 113, $green, IMG_ARC_PIE);
	if ($val['SW']== "2") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 113, 158, $green, IMG_ARC_PIE);
	if ($val['W'] == "2") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 158, 203, $green, IMG_ARC_PIE);
	if ($val['NW']== "2") imagefilledarc($image, $img_width/2, $img_width/2, $size, $size, 203, 248, $green, IMG_ARC_PIE);

	imagefilledellipse($image, $img_width/2, $img_width/2, $img_width/2, $img_width/2, $bg);
	//imageellipse($image, $img_width/2, $img_width/2, $img_width/2, $img_width/2, $gray);
	//imageellipse($image, $img_width/2, $img_width/2, $size, $size, $gray);

	if ($_GET['axes']=="yes"){
	   imageline($image, $img_width/2, 0, $img_width/2, $img_width, $darkgray);
	   imageline($image, 0, $img_width/2, $img_width, $img_width/2, $darkgray);
	}

	if(imagepng($image, 'windrose/'.$size.'/'.$id.'.png')) echo $id.".png";
	else echo ":(";
}
?>
