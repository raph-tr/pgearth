<?php
include("../../../config/connection.php");

$dest = @imagecreatetruecolor(187, 25)
	 or die ("Impossible de crée un flux d'image GD");
$white = imagecolorallocate($dest, 255, 255, 255);

$items = ['hike', 'soaring', 'thermal', 'xc', 'flatland', 'winch', 'hanggliding'];
$x = 0; $i=0;

$id = $_GET['id'];

if ($id=="all"){   //  generate images for every sites in the db
	$result = mysqli_query($bdd, "select id, hike, soaring, thermal, xc, flatland, winch, hanggliding from site");
	while ($val = mysqli_fetch_array($result)) {
		// create our recipient image
		$dest = @imagecreatetruecolor(187, 25)
			 or die ("Impossible de crée un flux d'image GD");
		$bg = imagecolorallocate($dest,0,0,0);
		imagealphablending($dest, false);   // alpha setting
		imagesavealpha($dest, true);        // alpha setting
		imagefill($dest, 0, 0, $white);
		imagecolortransparent($dest, $white);  // transparent image

		foreach ($items as $item){
			if ($val[$item]) $src = imagecreatefrompng($item."25.png");
			else $src = imagecreatefrompng($item."25no.png");
			imagecopy($dest, $src, $x, 0, 0, 0, 25, 25);
			$x += 27;
		}
		$x = 0;
		imagecolortransparent($dest, $white);  // transparent image

		imagepng($dest,'25/'.$val['id'].'.png');	
		imagedestroy($dest);
			$i++;//echo $val['id_site']." done<br />";		
	} echo $i." images created";
}
else {    //  generate images for one site in the db
	if ($result = mysqli_query($bdd, "select hike, soaring, thermal, xc, flatland, winch, hanggliding from site where id = $id;")){
		// create our recipient image
		$bg = imagecolorallocate($dest,0,0,0);
		imagefill($dest, 0, 0, $white);
		imagecolortransparent($dest, $white);  // transparent image
		imagealphablending($dest, false);   // alpha setting
		imagesavealpha($dest, true);        // alpha setting
		imagecolortransparent($dest, $white);  // transparent image

		$val = mysqli_fetch_array($result);
		foreach ($items as $item){
			if ($val[$item]) $src = imagecreatefrompng($item."25.png");
			else $src = imagecreatefrompng($item."25no.png");
			imagecopy($dest, $src, $x, 0, 0, 0, 25, 25);
		imagecolortransparent($dest, $white);  // transparent image
			$x += 27;
		}
		imagealphablending($dest, true);   // alpha setting
		imagesavealpha($dest, true);        // alpha setting
		imagecolortransparent($dest, $white);  // transparent image
		if (imagepng($dest,'25/'.$id.'.png')) echo "ok !";
	}
}

imagedestroy($dest);
imagedestroy($src);
?>
