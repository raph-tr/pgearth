<?php
header('Content-Type: application/json');

include "../../config/connectionReadOnly.php";

$onMapSites = $_GET['test'];
$country = $_GET['country'];
$kinds   = $_GET['kinds'];
$winds   = $_GET['winds'];
$flyable = $_GET['flyable'];

$notYetTestedSites = $onMapSites;

$onMapClause = "(";
foreach ($onMapSites as $key => $value) {
//	print_r($value);
	$onMapClause .= " site.id = ".$value." or "; 
}
if ($onMapClause != "(") {
	$onMapClause = substr($onMapClause, 0, -3);
	$onMapClause .= " ) and ";
} else {
	$onMapClause = "";
}

$countryClause = "";
if ($country<>"all" and $country<>"") {
	$countryClause  = " site.iso = '".$country."' and ";
}

$windClause = "(";
foreach($winds as $wind){
	if ($windClause <> "(" ) $windClause .= " or ";
	$windClause .= " ".$wind." > 0 "; 
}
if ($windClause == "(") $windClause = "";
else $windClause .= " ) and ";


$kindClause="";
foreach($kinds as $kind){
	$kindClause .= " ".$kind." = 1 and "; 
}



$out = '[
	{ "query": "done"}
	';

 // /*  debug : */ $out .= ' "debug": '.json_encode($onMapClause.$windClause.$kindClause).'';



//** Query site that match the filters *********
$out .= queryDB( 1 , $onMapClause, $kindClause, $windClause , $bdd);



//*** Query sites that just don't have relevant information ********
$onMapClause = "(";
foreach ($notYetTestedSites as $key => $value) {
	$onMapclause .= " site.id = ".$value." or "; 
}
if ($onMapClause != "(") {
	$onMapClause = substr($onMapClause, 0, -3);
	$onMapclause .= " ) and ";
} else $onMapClause = "";
if ($kindClause <> "") {
	$kindClause = " 'hike'=0 and 'soaring'=0 and 'thermal'=0 and 'xc'=0 and 'flatland'=0 and 'winch'=0 and 'hanggliding'= 0 and ";
}
if ($windClause <> ""){
	$windClause = " 'n'=0 and 'ne'=0 and 'e'=0 and 'se'=0 and 's'=0 and 'sw'=0 and 'w'=0 and 'nw'=0 and";
}
$out .= queryDB(-1 , $onMapClause, $kindClause, $windClause , $bdd);


//******* Query sites that we know DON'T match the filters *********
$onMapClause = "(";
foreach ($notYetTestedSites as $key => $value) {
	$onMapclause .= " site.id = ".$value." or "; 
}
if ($onMapClause != "(") {
	$onMapClause = substr($onMapClause, 0, -3);
	$onMapclause .= " ) and ";
	$windClause = "";
	$kindClause = "";
	$out .= queryDB(0 , $onMapClause, $kindClause, $windClause , $bdd);
} else {
	$out .= ',{"dont":"nope"}';
}


//********  finished ! ********
$out .= '
]';

echo $out;

 
//********  query the DB and generate ajax response *****
function queryDB( $filteringResult, $onMapClause, $kindClause, $windClause, $db ) {
	
	global $notYetTestedSites;

	$query = "SELECT id
					FROM site
					WHERE ".$onMapClause.$kindClause.$windClause. " 1 = 1";

	$foo .= ',{ "query": '.json_encode($query).'}';
	//$foo = "";


	$result = mysqli_query($db, $query);

	while ($val = mysqli_fetch_array($result)){

	    $foo .= ',
	{	 "id": '. $val['id'] .', 
	 "filters" : '.$filteringResult.'
	}';
/*		"properties": {
			"NAME": '. json_encode($val['name']) .',
			"country": '.json_encode(strtolower($val['iso'])).',
			"closed": '.json_encode($val['closed']).',
			"takeoff_altitude": '.json_encode($val['takeoff_altitude']).',
			"landing_altitude": '.json_encode($val['landing_altitude']).',
			"ranking": '.json_encode($val['ranking']).',
			"number_votes": '.json_encode($val['number_votes']).',
			"filters" : '.json_encode($filteringResult).'
		},
		"geometry": {	
			"type" : "Point",
			"coordinates" : ['. $val['lng'] .', '. $val['lat'] .']
		}
	},';
	*/

	deleteElementFromArray($val['id']);   // the site has been tested !
	$foo .= ',{"remaining": '.json_encode($notYetTestedSites).'}';
	}
	

	return $foo;
}


//**** remove item from array *****
function deleteElementFromArray($element){
	global $notYetTestedSites;
    $index = array_search($element, $notYetTestedSites);
    if($index !== false){
        unset($notYetTestedSites[$index]);
    }
}

?>
