<?php
  //header("application/json; charset=utf-8"); 

  include "../../config/connectionReadOnly.php";

  $south	= $_GET['south'];
  $north	= $_GET['north'];
  $east		= $_GET['east'];
  $west 	= $_GET['west'];
  $limit  	= $_GET['limit'];
  $country 	= $_GET['country'];
  $adminArea_id = $_GET['adminArea_id'];
 
  $ignorePros = $_GET['ignore'];
  
  $ignoreProsClause = "";
  if(count($ignorePros) > 0){
	  foreach($ignorePros as $ignorePro){
		  $ignoreProsClause .= " pro.id != ".$ignorePro." and ";
		  }
  }
  

  $countryWhereClause = "";
  if ($country<>"all") {
  	  $countryWhereClause  = " pro.iso = '".$country."' and ";
  }

  $adminAreaWhereClause = "";
  if ($limit=='adminArea' && $adminArea_id<>"" ) {
  	  $adminAreaWhereClause  = "";//site.region = ".$adminArea_id." and ";
  }

  $hasGPSClause = " and (pro.lat!=0 and pro.lng !=0) ";
  $idWhereClause = " and 1=1 ";
  
  $maxNumberOfSitesToQuery= 100 -  count($ignorePros) ;
 

  if ($east < $west) {
       $query = "select pro.lng, pro.lat, pro.id, pro.name, tel, adress, adress2, city, zipCode, website, iso from pro where $ignoreProsClause $countryWhereClause $adminAreaWhereClause  (pro.lng > $west or pro.lat < $east) and pro.lat > $south and pro.lat < $north $idWhereClause $hasGPSClause ORDER BY RAND() LIMIT ".  $maxNumberOfSitesToQuery.";";
       $queryAll = "select pro.id from pro where $countryWhereClause $adminAreaWhereClause (pro.lng < $east or pro.lng > $west) and pro.lat > $south and pro.lat < $north $hasGPSClause";
  } else {
       $query = "select pro.lng, pro.lat, pro.id, pro.name, tel, website, adress, adress2, city, zipCode, iso from pro where $ignoreProsClause $countryWhereClause $adminAreaWhereClause pro.lng < $east and pro.lng > $west and pro.lat > $south and pro.lat < $north $idWhereClause $hasGPSClause ORDER BY RAND() LIMIT  ".  $maxNumberOfSitesToQuery.";";
       $queryAll = "select pro.id from pro where $countryWhereClause $adminAreaWhereClause pro.lng < $east and pro.lng > $west and pro.lat > $south and pro.lat < $north $hasGPSClause ";
  }
  
  //echo $query."<br />";
  
  $res = mysqli_query($bdd, $query);
  $queried = mysqli_num_rows($res) ;
  $numDisplayed = $queried  + count($ignorePros);
  
  if ($numDisplayed > 99 ) {
    $resAll = mysqli_query($bdd, $queryAll);
    $numAll = mysqli_num_rows($resAll);
  } else {
    $numAll = $numDisplayed;
  }  


$out='{
  "total": '. $numAll .',
  "displayed": '. $numDisplayed .',

"type": "FeatureCollection",                                                             
"features": [';
while ($result = mysqli_fetch_array($res)){
	$adressJson = json_encode($result['adress'].' '.$result['adress2'].' '.$result['city'].' - '.$result['zipCode'] .' '.$result['iso']);
    $out .='{"type": "Feature", "id": '. $result['id'] .', "properties": {
          "NAME": '. json_encode($result['name']) .',
          "tel": '. json_encode($result['tel']) .',
          "adress": '. $adressJson .',
          "url": '. json_encode($result['website']) .',
          "country": '.json_encode(strtolower($result['iso'])).'},
          "geometry": {
			"type": "Point", "coordinates": ['. $result['lng'] .','. $result['lat'] .']}
      },';
}
if ($numDisplayed > 0) $out = substr($out, 0, -1);
$out .= '
   ] 
}';

echo $out;
?>
