<?php
require '_checkLoggedIn.php';

include("../../../config/connection.php");


$query = "
	INSERT INTO `site` 
		( `site`, `name`,  `iso`, `lng`, `lat`, `takeoff_altitude`, `date_site`, `author`, `date_site_creation`, `paragliding`)
	VALUES
		( '1', '".mysqli_real_escape_string( $bdd, $_REQUEST['name'])."', '".$_REQUEST['iso']."', '".$_REQUEST['lng']."', '".$_REQUEST['lat']."', '".$_REQUEST['alt']."', NOW(), '".$_SESSION['userId']."' , NOW(), '1')";

		$response_array['query'] = $query;   /* query */

	if ($result = mysqli_query($bdd, $query)) {
		
		$id = mysqli_insert_id($bdd);  /* get site id */ 
		
		$changed = 'creation';
		include("saveSiteEditedByUser.php");

		copy ( "../../../assets/img/windrose/26/0.png",  "../../../assets/img/windrose/26/".$id.".png");   /* create windrose img */ 
		copy ( "../../../assets/img/flying/25/0.png"  ,  "../../../assets/img/flying/25/".$id.".png");     /* create flight type image */ 
		copy ( "../../../assets/img/calendar/80/0.png"  ,  "../../../assets/img/calendar/80/".$id.".png");     /* create flight type image */ 

		$response_array['itemID'] =  $id; /* site */ 
		$response_array['status'] = 'success'; /* match error string in jquery if/else */ 
		$response_array['message'] = 'it worked!';   /* add custom message */ 
	} else {
		$response_array['siteID'] = 0;
		$response_array['status'] = 'error'; /* match error string in jquery if/else */ 
		$response_array['message'] = 'didnt work!';   /* add custom message */ 
	}
		header('Content-type: application/json');
		echo json_encode($response_array);
		//console.log($query);
?>
