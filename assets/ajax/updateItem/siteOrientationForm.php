<?php
require '_checkLoggedIn.php';
?>
<style>
	.yellow{background:yellow;}
	.paleGreen{background:paleGreen;}
	.green{background:green;}
	.centered {text-align:center; margin: 17px; color:black !important}
	.centered > td {padding : 6px;}
	.pseudoForm{min-width: 32px; padding: 6px; cursor: pointer; color:black; font-weight: bold}
</style>
	
<?php
include("../../../config/connection.php");

if (isset($_POST['id']) or isset($_GET['id'])) {
	if (isset($_POST['id'])) $id = $_POST['id'];
	if (isset($_GET['id'])) $id = $_GET['id'];
	
	$orientations = ["north" => "N","north-east" => "NE","east" => "E","south-east" => "SE","south" => "S","south-west" => "SW","west" => "W","north-west" => "NW"]; 
	$query= 'select   N,  NE,  E,  SE,  S,  SW,  W,  NW from site where id = '.$id;
	$result = mysqli_query($bdd, $query);
	$row = mysqli_fetch_assoc($result);
		echo '	<input type="hidden" name="id" id="input-id" value="'.$id.'"/>
		';

	foreach($orientations as $name => $orientation) {
		echo '	<input type="hidden" name="'.$orientation.'" id="input-'.$orientation.'" value="'.$row[$orientation].'"/>
	';
	}
}
?>
<div  class="alert-warning">
<div class="small">Suitable orientations (click to modify..) :</div>
<table class="small centered">
	<tr>
		<td id="NW" class="pseudoForm" style="border-radius: 24px 0 0 0;">NW</td> 
		<td id="N"  class="pseudoForm">N</td>
		<td id="NE" class="pseudoForm" style="border-radius: 0 24px 0 0;">NE</td>
		<td style="padding-left:17px"> legend : </td>
		<td class="yellow">avoid</td>
	</tr>
	<tr>
		<td id ="W" class="pseudoForm">W</td>
		<td></td>
		<td id="E"  class="pseudoForm">E</td>
		<td></td>
		<td class="paleGreen">possible</td>
	</tr>
	<tr>
		<td id="SW" class="pseudoForm" style="border-radius: 0 0 0 24px;">SW</td>
		<td id="S"  class="pseudoForm">S</td>
		<td id="SE" class="pseudoForm" style="border-radius: 0 0 24px 0;">SE</td>
		<td></td>
		<td class="green">good</td>
	</tr>
</table>
<button id="submitOrientation"  class="btn btn-primary btn-sm">submit orientations</button>
 <button id="cancelOrientation"  class="btn btn-secondary btn-sm">cancel</button>

</div>

<script>
	var orientations= {'N':0, 'NE':0, 'E':0, 'SE':0, 'S':0, 'SW':0, 'W':0, 'NW':0};
	
	$.each(orientations, function(orientation, value) {
		if ($("#input-"+orientation).val()==1) { $("#"+orientation).addClass("paleGreen"); orientations[orientation] = 1; }
		else if ($("#input-"+orientation).val()==2) { $("#"+orientation).addClass("green"); orientations[orientation] = 2; }
		else { $("#"+orientation).addClass("yellow");  orientations[orientation] = 0; }
		
		$("#"+orientation).on("click", function(){
			$("#"+orientation).removeClass("green paleGreen yellow");
			orientations[orientation]++; if (orientations[orientation]==3) orientations[orientation]=0;
			
			if ( orientations[orientation] == 2 ) {
				$("#"+orientation).addClass("green");
				$("#input-"+orientation).val("2");
			}
			if ( orientations[orientation] == 1 ) {
				$("#"+orientation).addClass("paleGreen");
				$("#input-"+orientation).val("1");
			}
			if ( orientations[orientation] == 0 )  {
				$("#"+orientation).addClass("yellow");
				$("#input-"+orientation).val("0");
			}
				console.log(orientations);

		});
		
	});
	
	$("#cancelOrientation").on("click", function(){
		$("#orientationForm").toggle("slow");
		});
	
	$("#submitOrientation").on("click", function(){
		orientations.id = <?php echo $id; ?>;
		console.log(orientations);
		$.post("assets/ajax/updateItem/siteOrientationSave.php", orientations, function(data){
				//alert(data);
				$.get( "assets/img/windrose_create_img.php", { "id": <?php echo $id; ?>, "size": 26 }, function(data){
					d = new Date();
					$("#windroseImg").attr("src", "assets/img/windrose/26/<?php echo $id; ?>.png?t="+d.getTime());
					$("#orientationForm").toggle("slow");
					} );
			});
		});
	
</script>
