<?php
require '_checkLoggedIn.php';

include("../../../config/connection.php");

if ($_REQUEST['id']) {
	
	$id = $_REQUEST['id'];

	$query = "SELECT ranking, number_votes FROM site WHERE id =".$id.";";

//	echo $query." /// ";
	
	$result= mysqli_query($bdd, $query);
	$val   = mysqli_fetch_array($result);
	
//	printf($val);

	$ranking = $val['ranking'];
	$number_votes = $val['number_votes'];
	
//	echo $number_votes." / ".$ranking." /// ";

	$ranking = (( $number_votes * $ranking ) + $_POST['vote'] ) / ( $number_votes + 1);
	$number_votes++;


	$query = "UPDATE site SET ranking = ".$ranking.", number_votes = ".$number_votes." WHERE id= ".$id.";";

	if ( $result = mysqli_query($bdd, $query) ) {

		$response_array['ranking'] = round($ranking*100)/100; /* match error string in jquery if/else */ 
		$response_array['ranking_roundedHalf'] = round($ranking*2) /2 ; /* match error string in jquery if/else */ 
		$response_array['number_of_votes'] = $number_votes;   /* add custom message */ 
		header('Content-type: application/json');
		echo json_encode($response_array);

	} else {
		/* 
		In case of incorrect value or error you should return HTTP status != 200. 
		Response body will be shown as error message in editable form.
		*/
		header('HTTP/1.0 400 Bad Request', true, 400);
		echo "Problem updating the database :(";
		echo $query;
	//	console.log($query);
	}
}
?>
