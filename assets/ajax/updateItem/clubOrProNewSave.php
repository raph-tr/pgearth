<?php
require '_checkLoggedIn.php';

include("../../../config/connection.php");

$query = " INSERT INTO `".$_REQUEST['item']."` ( `name`, `iso`, `city`, `lng`, `lat`, `auteur`,`date`)
					VALUES ( 	'".mysqli_real_escape_string( $bdd, $_REQUEST['name'])."',
								'".$_REQUEST['iso']."',
								'".mysqli_real_escape_string( $bdd, $_REQUEST['city'])."',
								'".$_REQUEST['lng']."',
								'".$_REQUEST['lat']."',
								'".$_SESSION['userId']."',
								NOW())";
					
$response_array['query'] = $query;   /* query */

if ($result = mysqli_query($bdd, $query)) {
	$id = mysqli_insert_id($bdd);  /* get item id */
	$response_array['itemID'] =  $id; /* site */ 
	
	$item_type = $_REQUEST['item'];
	$changed   = 'creation';
	include("saveSiteEditedByUser.php");
	
	$response_array['status'] = 'success'; /* match error string in jquery if/else */ 
	$response_array['message'] = 'it worked!';   /* add custom message */ 
} else {
	$response_array['itemID'] = 0;
	$response_array['status'] = 'error'; /* match error string in jquery if/else */ 
	$response_array['message'] = 'didnt work!';   /* add custom message */ 
}

header('Content-type: application/json');
echo json_encode($response_array);
//console.log($query);
?>
