<?php
require '_checkLoggedIn.php';

include("../../../config/connection.php");

if ($_REQUEST['id'] != '0') {
	// console.log("there");
	$id = $_REQUEST['id'];
	
	$item = $_REQUEST['item'];
	
	if ($item == 'takeoff' or $item == 'landing' ) { $description_field = $item; }
	else { $description_field = $item."_description"; }
	
	if ($item == 'takeoff') { $lat_field = "lat"; $lng_field = "lng"; }
	else  { $lat_field = $item."_lat"; $lng_field = $item."_lng"; }
	
	$query = "UPDATE `site` SET 
				`".$lat_field."` = '".$_REQUEST['lat']."',
				`".$lng_field."` = '".$_REQUEST['lng']."',
				`".$item."_altitude` = '".$_REQUEST['altitude']."',
				`".$description_field."` = '".mysqli_real_escape_string($bdd, $_REQUEST['description'])."',
				`".$item."_name` = '".mysqli_real_escape_string($bdd, $_REQUEST['name'])."'
			  WHERE `site`.`id` = ".$id.""; 

	$response_array['query'] = $query;


	if ($result = mysqli_query($bdd, $query)) {
		
		$changed = 'map';
		include("saveSiteEditedByUser.php");
		
		$response_array['status'] = 'success'; /* match error string in jquery if/else */ 
		$response_array['message'] = 'it worked!';   /* add custom message */ 
		header('Content-type: application/json');
		echo json_encode($response_array);
	} else {
		$response_array['status'] = 'error'; /* match error string in jquery if/else */ 
		$response_array['message'] = 'didnt work!';   /* add custom message */ 
		header('Content-type: application/json');
		echo json_encode($response_array);
		console.log($query);
	}
}

?>
