<?php
require '_checkLoggedIn.php';

include("../../../config/connection.php");


if (isset($_POST['id']) or isset($_GET['id'])) {
	if (isset($_POST['id'])) $id = $_POST['id'];
	if (isset($_GET['id'])) $id = $_GET['id'];
	
	$flyings = ["hike", "soaring", "thermal", "xc", "flatland", "winch", "hanggliding"]; 
	$query= 'select hike, soaring, thermal, xc, flatland, winch, hanggliding from site where id = '.$id;
	$result = mysqli_query($bdd, $query);
	$row = mysqli_fetch_assoc($result);
		echo '	<input type="hidden" name="id" id="input-id" value="'.$id.'"/>
		';

	foreach($flyings as $flying) {
		echo '	<input type="hidden" name="'.$flying.'" id="input-'.$flying.'" value="'.$row[$flying].'"/>
	';
	}
}
?>
<div class="alert-warning">
Flying made here (click to modify..) :
<ul>
	<li id="hike"><img id="hikeImg" src="assets/img/flying/hike25no.png" /> Hike and fly</li>
	<li id ="soaring"><img id="soaringImg" src="assets/img/flying/soaring25no.png" /> Soaring</li>
	<li id="thermal"><img id="thermalImg" src="assets/img/flying/thermal25no.png" /> Flying 'local' in thermals</li>
	<li id="xc"><img id="xcImg" src="assets/img/flying/xc25no.png" /> Great X-Country mountain potential</li>
	<li id="flatland"><img id="flatlandImg" src="assets/img/flying/flatland25no.png" /> Flatland xc flying</li>
	<li id="winch"><img id="winchImg" src="assets/img/flying/winch25no.png" /> Winch start</li>
	<li id="hanggliding"><img id="hangglidingImg" src="assets/img/flying/hanggliding25no.png" /> Hanggliding site</li>
</ul>

<button id="submitFlying"  class="btn btn-primary btn-sm">submit flying</button>
 <button id="cancelFlying"  class="btn btn-secondary btn-sm">cancel</button>
</div>
<script>
	var flyings= {"hike":0, "soaring":0, "thermal":0, "xc":0, "flatland":0, "winch":0, "hanggliding":0};
	
	$.each(flyings, function(flying, value) {
		if ($("#input-"+flying).val()==1) {
			 flyings[flying] = 1; 
			 $("#"+flying+"Img").attr("src", "assets/img/flying/"+flying+"25.png");
		} else { flyings[flying] = 0; }
		
		$("#"+flying).on("click", function(){
			if (flyings[flying]==0) flyings[flying]=1;
			else  flyings[flying]=0;
			
			if ( flyings[flying] == 1 ) {
				$("#input-"+flying).val("1");
				$("#"+flying+"Img").attr("src", "assets/img/flying/"+flying+"25.png");
			}
			if ( flyings[flying] == 0 )  {
				$("#input-"+flying).val("0");
				$("#"+flying+"Img").attr("src", "assets/img/flying/"+flying+"25no.png");
			}
//				console.log(flyings);
		});
		
	});
	
	$("#cancelFlying").on("click", function(){
			$("#flyingForm").toggle("slow");
		});
	
	$("#submitFlying").on("click", function(){
		flyings.id = <?php echo $id; ?>;
//		console.log(flyings);
		$.post("assets/ajax/updateItem/siteFlyingSave.php", flyings, function(data){
				//alert(data);
				$.get( "assets/img/flying/flying25.php", { "id": <?php echo $id; ?> }, function(data){
					d = new Date();
					$("#flyingImg").attr("src", "assets/img/flying/25/<?php echo $id; ?>.png?t="+d.getTime());
					$("#flyingForm").toggle("slow");
					// alert(data);
					} );
			});
		});
	
</script>
