<?php
require '_checkLoggedIn.php';

include "../../../config/connection.php";

/*
  Script for update record from X-editable.

  You will get 'pk', 'name' and 'value' in $_POST array.
*/
$pk = $_POST['pk'];
$name = $_POST['name'];
$value = $_POST['value'];
/*
 Check submitted value
*/
if(!empty($value)) {
	/*
	  If value is correct you process it (for example, save to db).
	  In case of success your script should not return anything, standard HTTP response '200 OK' is enough.
	  
	*/
	$query= 'update site set '.mysqli_real_escape_string($bdd, $name).'="'.mysqli_real_escape_string($bdd, $value).'" where id = "'.mysqli_real_escape_string($bdd, $pk).'"';
	if ($result = mysqli_query($bdd, $query)) {
		
		$id = $pk;
		$changed = 'text';
		include("saveSiteEditedByUser.php");

		//here, for debug reason we just return dump of $_POST, you will see result in browser console
		//console.log($_POST);
	} else {
		/* 
		In case of incorrect value or error you should return HTTP status != 200. 
		Response body will be shown as error message in editable form.
		*/
		header('HTTP/1.0 400 Bad Request', true, 400);
		echo "Problem updating the database :(";
		//console.log($query);

	}
} else {
	/* 
	In case of incorrect value or error you should return HTTP status != 200. 
	Response body will be shown as error message in editable form.
	*/
	header('HTTP/1.0 400 Bad Request', true, 400);
	echo "This field is required!";
}
?>
