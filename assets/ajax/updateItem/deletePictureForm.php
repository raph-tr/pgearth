<?php
//session_start();
include '../../../member/ASEngine/AS.php';
include '../../../config/connection.php';

$isModo = false;

if (! app('login')->isLoggedIn() ) exit('Log in, please...');
$user = app('current_user');

if ( $user->role_id >= 2 ) $isModo = true;

?>
<script>console.log(<?php echo $user->role_id;?>);</script>
<div class="alert-danger">

	<strong>So you want this pic <?php echo $pic;?> to be deleted ? </strong>

<?php
/***  get logged in member id  */
if ( $_SESSION['username'] != "" ) $userId = $_SESSION['userId'];
else $userId = -1;

/****** get picture details (author, site) */
$picName = $_REQUEST['picture'];
$q = "select * from picture where name LIKE '".$picName."'";
$r = mysqli_query($bdd, $q);
$pic = mysqli_fetch_array($r);
?>
	<div class="row">
		
		<div class="col-xs-3" style="text-align:center">
			<img src="assets/img/s	ites-pictures/<?php echo $picName; ?>" width="100px" />
			<script>console.log('pic delete form');</script>
		</div>
		<div class="col-sm-9"  id="pictureDeleteText">
	<?php
	/******** if picture belongs to member, offer him to delete it ***/
	if ($pic['author'] == $userId or $isModo) {
	
		if ($isModo)  {
			?>
			<p>As a moderator, you can delete it by clicking here.<br />
			You'd better be sure, there will be no way back...</p>
			<?php
		} else {
			?>
			<p>As this is a picture of yours, you can delete it by clicking here.<br />
			You'd better be sure, there will be no way back...</p>
			<?php
		} ?>
		<p><button id="deletePicBtn" class="btn btn-primary">Delete !</button></p>
		<?php
	} else {
	/***    if not a member or not the pic author, offer to login or report picture   ***/ ?>
		<p>As you are not the author of this picture, you can report it to moderators.<br />
		To do so, open the "About" menu link and use the form in "contact" tab.</p>
		<?php if($userId == -1) { ?>
			<p>If this your picture, you might want to <a href="#" modalToOpen="loginModal" class="openAnotherModal">login to the site</a>.</p>
		<?php } ?>
		<p><button modalToOpen="reportItemModal" class="openAnotherModal btn btn-primary" itemType="picture" itemName="<?php echo $pic['name']?>" itemId="<?php echo $pic['id']?>" >Report picture</button></p>
	<?php	
	}
	?>
		</div>
	</div>
	&nbsp;
</div>

<?php
	if ($pic['author'] == $userId or $isModo) {
?>

<script>
	$("#deletePicBtn").on ('click', function() { 
		$.post("assets/ajax/updateItem/deletePictureSave.php", { picture: "<?php echo $picName; ?>"}, function(){
			$("#siteDetailsModal").modal('toggle');
			alert('Picture is deleted');
			siteModal(<?php echo $pic['site']?>);
			});
	});
</script>

<?php } ?>
