<?php
require '_checkLoggedIn.php';

include("../../../config/connection.php");

if ($_REQUEST['id']) {
//	console.log("there");
	$id = $_REQUEST['id'];
	$flyings = ["hike"=>0,"soaring"=>0,"thermal"=>0,"xc"=>0,"flatland"=>0,"winch"=>0,"hanggliding"=>0];
	$string = "";
	foreach($flyings as $flying=>$value){
			$string .= " ".$flying." = ".$_REQUEST[$flying].",";
	}
	$string = substr( $string, 0, -1);
	$query= 'update site set '.$string.' where id = '.$id ;

	if ($result = mysqli_query($bdd, $query)) {
		
		$changed = 'flying';
		include("saveSiteEditedByUser.php");
		
		//here, for debug reason we just return dump of $_POST, you will see result in browser console
		echo 'ok!';
	//	console.log($_REQUEST);
	} else {
		/* 
		In case of incorrect value or error you should return HTTP status != 200. 
		Response body will be shown as error message in editable form.
		*/
		header('HTTP/1.0 400 Bad Request', true, 400);
		echo "Problem updating the database :(";
	//	console.log($query);
	}
}

?>
