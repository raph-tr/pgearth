<?php
require '_checkLoggedIn.php';

include("../../../config/connection.php");

if ($_REQUEST['id']) {
	// console.log("there");
	$id = $_REQUEST['id'];
	$orientations = ["N"=>0,"NE"=>0,"E"=>0,"SE"=>0,"S"=>0,"SW"=>0,"W"=>0,"NW"=>0];
	$string = "";
	foreach($orientations as $orientation=>$value){
			$string .= " ".$orientation." = ".$_REQUEST[$orientation].",";
	}
	$string = substr( $string, 0, -1);
	$query= 'update site set '.$string.' where id = '.$id ;

	if ($result = mysqli_query($bdd, $query)) {
		
		$changed = 'orientations';
		include("saveSiteEditedByUser.php");

		//here, for debug reason we just return dump of $_POST, you will see result in browser console
		echo 'ok!';
		//console.log($_REQUEST);
	} else {
		/* 
		In case of incorrect value or error you should return HTTP status != 200. 
		Response body will be shown as error message in editable form.
		*/
		header('HTTP/1.0 400 Bad Request', true, 400);
		echo "Problem updating the database :(";
		// console.log($query);
	}
}

?>
