<?php

include "../../config/connectionReadOnly.php";
?>
<html>
	<head>
		<script src="../js/Chart.bundle.min.js"></script>
	</head>
	<body>
		<canvas id="myChart" width="400" height="400"></canvas>
	<script>
		var ctx = document.getElementById("myChart");

		var myLineChart = new Chart(ctx, {
			
			type: 'line',

			data: {
				labels: ["-7", "-6", "-5", "-4", "-3", "-2", "-1", "0"],
				datasets: [{
					label: '# of edited sites',
					data : [
			
						
						<?php
					$query = "SELECT DATE_FORMAT(date, '%Y-%m-%d') AS the_date
						 , COUNT(*) AS count 
					  FROM sites_users
					 WHERE date BETWEEN DATE_FORMAT((NOW() - INTERVAL 7 DAY), '%Y-%m-%d') AND DATE_FORMAT(NOW(), '%Y-%m-%d') 
					 GROUP 
						BY the_date";
						
					$result = mysqli_query($bdd, $query); $j=0;

					while ($val = mysqli_fetch_array($result)){
						
						if ($j > 0) echo ",
						";
						$j++;
						echo "{x:".$j/*$val['the_date']*/.", y: ".$val['count']."}";
					}
					?>
		
					]
				}]
			},
			
			options:  {}
			
		});


	</script>
	</body>
</html>

