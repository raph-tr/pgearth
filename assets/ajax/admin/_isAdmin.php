<?php
include '../../../member/ASEngine/AS.php';
include '../../../config/connection.php';

if (! app('login')->isLoggedIn() ) exit('Log in, please...');
$user = app('current_user');

if ( $user->role_id < 2 ) exit('Restricted area here, dude...');
?>
