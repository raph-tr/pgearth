<?php

include "../../config/connection.php";


$query = "
SELECT 	sites_users.item_id,  sites_users.user,  sites_users.date,
		GROUP_CONCAT(DISTINCT sites_users.modification) AS modifications,
		pro.id, pro.name, pro.iso, as_users.username
		FROM `sites_users` 
		left join pro on sites_users.item_id = pro.id
		left join as_users on sites_users.user =  as_users.user_id
		WHERE DATE(sites_users.date) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY) 
		  AND item_type LIKE 'pro'
		GROUP BY  sites_users.user, sites_users.item_id,  sites_users.date
		ORDER BY sites_users.date DESC, username, name";

$result = mysqli_query($bdd, $query);

$currentDate = "2012-12-12";
$currentUser ="totoPoil";

echo "<ul>";

while ($val = mysqli_fetch_array($result)){
	
	if ($val['date'] != $currentDate) {
		$currentDate = $val['date'];
		echo "</ul>".$val['date']." :<ul>";
	}
	echo "<li><a href='#' class='openAnotherModal' modalToOpen='memberModal' member='".$val['username']."'>".$val['username']."</a> edited  <a href='#' class='openAnotherModal' modalToOpen='featureModal' feature='pro' id='".$val['item_id']."'>".$val['name']."</a> <img class='countryFlag' iso='".$val['iso']."'>  &rarr; ".$val['modifications']."</li>";
}
echo "</ul>";

?>
