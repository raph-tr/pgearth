<?php
header('Content-Type: application/json; charset=utf-8');
//session_start();

$site_id  = $_REQUEST['id'];  //   "REQUEST" so wecan it from POST (ajax) or GET (debug)...
include "../../config/connection.php";

/****************  starting to build our jeson response, called "out" **********/
$out = '{
	"items" : [   '	;

/***************** listing 'standard' items (landing and parkings) ************/

function itemJSON( $type, $name, $lat, $lng ) {  // a function that will do the job
	if ( $lat != 0 and $lng != 0 ) {  // we have an item, guys !
		if ( strpos($type, "parking") > 0 ) $type = "parking";
		$output = '
			{
			  "type": '.json_encode( $type ).',
			  "name": '.json_encode( $name ).',
			  "lat":  '.json_encode( $lat ).',
			  "lng":  '.json_encode( $lng ).'
			},';
	} else {
		$output = '';
	}
	return $output;
}

$query = "SELECT 
			`lng`, `lat`, `name`,
			`landing_lng`, `landing_lat`, `landing_name`,
			`takeoff_parking_lat`, `takeoff_parking_lng`, `takeoff_parking_name`,
			`landing_parking_lat`, `landing_parking_lng`, `landing_parking_name`
			FROM `site` WHERE id = ".$site_id .";";  
			
$result = mysqli_query($bdd, $query);
$valItem = mysqli_fetch_array($result);

/****** we output the json for each item ***/
$out .= itemJSON( 'takeoff', 		 $valItem['name'], 					$valItem['lat'], 				 $valItem['lng'] );
$out .= itemJSON( 'landing', 		 $valItem['landing_name'], 			$valItem['landing_lat'], 		 $valItem['landing_lng'] );
$out .= itemJSON( 'landing_parking', $valItem['landing_parking_name'],  $valItem['landing_parking_lat'], $valItem['landing_parking_lng'] );
$out .= itemJSON( 'takeoff_parking', $valItem['takeoff_parking_name'],  $valItem['takeoff_parking_lat'], $valItem['takeoff_parking_lng'] );




/****************  listing extra items *****************/

$query = "SELECT * FROM site_extra_items WHERE site =".$site_id .";"; // used to get some info 
$result = mysqli_query($bdd, $query);


while ($valItem = mysqli_fetch_array($result)){
	$out .= '
		{
		  "name": '.json_encode($valItem['name']).',
		  "type":  '.json_encode($valItem['type']).',
		  "id":  '.json_encode($valItem['id']).',
		  "lat": '.json_encode($valItem['lat']).',
		  "lng": '.json_encode($valItem['lng']).',
		  "altitude": '.json_encode($valItem['altitude']).',
		  "description": '.json_encode($valItem['description']).'
		},';
}


/***** let's strip the trailing comma : ","  ***/
$out = substr($out, 0, -1);  


/****************** close our json string **********/
$out .='
	]
}';

/***** echo the output **/
echo $out;

?>
