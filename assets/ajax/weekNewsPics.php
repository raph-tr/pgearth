<?php
include "../../config/connectionReadOnly.php";

$query =	"SELECT picture.name, picture.comment, picture.site, site.name as siteName, site.iso
			FROM `picture`
			LEFT JOIN as_users ON author =  as_users.user_id
			LEFT JOIN site ON picture.site =  site.id
			WHERE DATE(picture.date) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)
			ORDER BY iso, siteName ASC ";

$result = mysqli_query($bdd, $query);
$iso = "";

echo "<div>";

$htmlNo = "no pictures...";
$html = "";

while ($val = mysqli_fetch_array($result)){
	if ($iso != $val['iso']){
		$htmlNo = "";
		$qCountry = "select name from country where iso LIKE '".$val['iso']."'";
		$rCountry =  mysqli_query($bdd, $qCountry);
		$vCountry = mysqli_fetch_array($rCountry);
		$iso = $val['iso'];
		$html .= "<p><img src='assets/img/flag/".strtolower($val['iso']).".png' title='".$vCountry['name']."' /> - ".$vCountry['name']."</p>";
		}
	$html .= "
			<a class='imgSpan' href='assets/img/sites-pictures/".$val['name']."' data-lightbox='imageNews' data-title='".addslashes( $val['comment'] )." - Site: ".addslashes($val['siteName'])."'>
				<img src='assets/img/sites-pictures/".$val['name']."' title='".$val['comment']."' width='100px' height='100px' class='sitePicImg' />
				<div class='sitePicLegend'>
					<span onclick=\"document.location.href='?site=".$val['site']."'\">".$val['siteName']."</span>
				</div>
			</a>
			";
	}

echo $htmlNo.$html;
echo "</div>";

?>
