<?php
  //header("application/json; charset=utf-8");

  include "../../config/connectionReadOnly.php";

  $south = $_GET['south'];
  $north = $_GET['north'];
  $east  = $_GET['east'];
  $west  = $_GET['west'];
  $limit  = $_GET['limit'];
  $country = $_GET['country'];
  $adminArea_id = $_GET['adminArea_id'];
  
  $ignoreClubs = $_GET['ignore'];
  
  $ignoreClubsClause = "";
  if(count($ignoreClubs) > 0){
	  foreach($ignoreClubs as $ignoreClub){
		  $ignoreClubsClause .= " club.id != ".$ignoreClub." and ";
		  }
  }
  

  $countryWhereClause = "";
  if ($country<>"all") {
	 
  	  $countryWhereClause  = " club.iso = '".$country."' and ";
  }

  $adminAreaWhereClause = "";
  if ($limit=='adminArea' && $adminArea_id<>"" ) {
  	  $adminAreaWhereClause  = "";//site.region = ".$adminArea_id." and ";
  }

  $hasGPSClause = " and (club.lat!=0 and club.lng !=0) ";
  $idWhereClause = " and 1=1 ";
  
  $maxNumberOfSitesToQuery= 100 - count($ignoreClubs);
 
  if ($east < $west) {
       $query = "select club.lng, club.lat, club.id, club.name, tel, adress, adress2, city, zipCode, website, iso from club  where $ignoreClubsClause $countryWhereClause $adminAreaWhereClause  (club.lng > $west or club.lat < $east) and club.lat > $south and club.lat < $north $idWhereClause $hasGPSClause ORDER BY RAND() LIMIT ".  $maxNumberOfSitesToQuery.";";
       $queryAll = "select club.id from club where $countryWhereClause $adminAreaWhereClause (club.lng < $east or club.lng > $west) and club.lat > $south and club.lat < $north $hasGPSClause";
  } else {
       $query = "select club.lng, club.lat, club.id, club.name, tel, adress, adress2, city, zipCode, website,  iso from club where $ignoreClubsClause $countryWhereClause $adminAreaWhereClause club.lng < $east and club.lng > $west and club.lat > $south and club.lat < $north $idWhereClause $hasGPSClause ORDER BY RAND() LIMIT  ".  $maxNumberOfSitesToQuery.";";
       $queryAll = "select club.id from club where $countryWhereClause $adminAreaWhereClause club.lng < $east and club.lng > $west and club.lat > $south and club.lat < $north $hasGPSClause ";
  }
  
  //echo $query."<br />";
  
  $res = mysqli_query($bdd, $query);
  $queried = mysqli_num_rows($res) ;
  $numDisplayed = $queried + count($ignoreClubs);
  
  if ($numDisplayed > 99 ) {
    $resAll = mysqli_query($bdd, $queryAll);
    $numAll = mysqli_num_rows($resAll);
  } else {
    $numAll = $numDisplayed;
  }  


$out='{
  "query": '.json_encode($query).',
  "total": '. $numAll .',
  "displayed": '. $numDisplayed .',

"type": "FeatureCollection",                                                             
"features": [';
while ($result = mysqli_fetch_array($res)){
    $out .='{"type": "Feature", "id": '. $result['id'] .', "properties": {
          "NAME": '. json_encode($result['name']) .',
          "tel": '. json_encode($result['tel']) .',
          "adress": '. json_encode($result['adress'].' '.$result['adress2'].' '.$result['city'].' - '.$result['zipCode'] .'  '.$result['iso']).',
          "url": '. json_encode($result['website']) .',
          "country": '.json_encode(strtolower($result['iso'])).'},
          "geometry": {
			"type": "Point", "coordinates": ['. $result['lng'] .','. $result['lat'] .']}
      },';
}
if ($numDisplayed > 0) $out = substr($out, 0, -1);
$out .= '
   ] 
}';

echo $out;
?>
