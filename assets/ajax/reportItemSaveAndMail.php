<?php

if (isset($_POST['name'])) {

	// Fetching Values from URL.
	$itemId = $_POST['itemId'];
	$itemName = $_POST['itemName'];
	$itemType = $_POST['itemType'];
	$name = $_POST['name'];
	$email = $_POST['email'];
	$message = $_POST['message'];
	$email = filter_var($email, FILTER_SANITIZE_EMAIL); // Sanitizing E-mail.

	include "../../config/connection.php";

/*************  keep log of the request in db ****************/
	$q = "INSERT INTO `reported_items`
			(`id`, `type`, `item_id`, `item_name`, `date`, `reason`, `requested_by_name`, `requested_by_email`, `done`)
			VALUES (NULL, '".$itemType."', '".$itemId."', '".mysqli_real_escape_string($bdd,$itemName)."', NOW(), '".mysqli_real_escape_string($bdd,$message)."', '".mysqli_real_escape_string($bdd,$name)."', '".mysqli_real_escape_string($bdd,$email)."', '0');";
	$r = mysqli_query($bdd, $q);
	
	// echo $q;
	

/************  SEND EMAILS *******************************/

	// Get moderators email adresses
	$q = "select email from as_users where user_role = 2;";
	$r = mysqli_query($bdd, $q);
	$strTo = "";
	while ($v = mysqli_fetch_array($r)) {
			$strTo .= $v['email'].", ";	
	}
	$strTo .= "raphael@disroot.org";
	

	// After sanitization Validation is performed
	if ( filter_var( $_POST['email'], FILTER_VALIDATE_EMAIL) ) {
		
		if ($itemType == 'picture') $itemUrl = "/assets/img/sites-pictures/".$itemName;
		else $itemUrl = "/?".$itemType."=".$itemId;

		$subject = "Delete a ".$itemType." on PgEarth";

		// To send HTML mail, the Content-type header must be set.
		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/plain; charset=utf-8' . "\r\n";
		$headers .= 'From:' . $email. "\r\n"; // Sender's Email
		$headers .= 'Cc:' . $email. "\r\n"; // Carbon copy to Sender

/*
		$template = "Hello " . $name . ",\n
you asked that the ".$itemType." : \r
 - name: ".$itemName." ,\r
 - url: http://paragliding.earth".$itemUrl." ,\r
should be deleted from pgearh for the following reason :\r
" . $message . "\n
From :\r
  Name: " . $name . "\r
  Email: " . $email . "\n
We will deal with your request as soon as possible.\n
Thanks for your participation and for helping PgEarth stay up to date...";
*/
		$template = "Hello " . $name . ",\n
you asked that the ".$itemType.": \r
  ".$itemName." \r
  http://paragliding.earth".$itemUrl." \n
  
should be deleted from pgearh for the following reason :\n
" . $message . "\n
\n
We will deal with your request as soon as possible.\r
Thanks for your participation and for helping PgEarth stay up to date...\n
Request by:\r
  " . $name . "\r
  " . $email . "";
 
 
		$sendmessage = $template;

		// Message lines should not exceed 70 characters (PHP rule), so wrap it.
		$sendmessage = wordwrap($template, 70);

		// Send mail by PHP Mail Function.
		mail($strTo, $subject, $sendmessage, $headers);
	
		echo "Request was sent to site moderators, thanks for your participation.";
		
	} else {
		echo "<span>* invalid email *</span>";
	}
}
?>
