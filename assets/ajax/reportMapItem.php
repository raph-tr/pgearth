<html>
  <head>
	<link rel="stylesheet" href="../css/localhost/bootstrap.min.css">
  </head>
  <body>
    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">
				<div  id="returnmessage"></div>
                <form class="form-horizontal" id="formDelete" action="reportMapItemSaveAndMail.php" method="POST">
                    <fieldset>
						
						<legend>
							So you think the <?php echo $_GET['itemType'];?> "<?php echo $_GET['itemName'];?>" should be erased ?
						</legend>
						
						<?php if ( $_GET['itemType'] == "landing") { ?>
							<div class="alert-danger">You are about to suggest that a main landing place should be erased :
							as we think all flying sites should have a (at least one) landing place, we would like to have a good reason to delete this.<br />
							If there is an error with this place, perhaps it should be wiser to correct it than erase it...<br />
							If you do want to ask moderators that this place must be erased instead of corrected, please explain in the form below.<br />
							Thanks anyway for helping us keeping the data correct ;)
							</div>
						<?php } ?>
						
						
							<input id="itemId"   name="itemId"   type="hidden" value="<?php echo $_GET['itemId'];?>"   >
							<input id="itemName" name="itemName" type="hidden" value="<?php echo $_GET['itemName'];?>" >
							<input id="itemType" name="itemType" type="hidden" value="<?php echo $_GET['itemType'];?>" >
							<input id="siteId"   name="siteId" 	 type="hidden" value="<?php echo $_GET['siteId'];?>" >
 
                        <div class="form-group">
                            <span class="col-md-12"><i class="fa fa-user bigicon"></i> * Your name</span>
                            <div class="col-md-12">
                                <input id="nameRequestDelete" name="name" type="text" placeholder="* Your name" class="form-control">
                            </div>
                        </div>
 
                        <div class="form-group">
                            <span class="col-md-12"><i class="fa fa-envelope-o bigicon"></i> * Your email</span>
                            <div class="col-md-12">
                                <input id="emailRequestDelete" name="email" type="text" placeholder="* Your email" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-12"><i class="fa fa-pencil-square-o bigicon"></i> * Reason(s) to delete the <?php echo $_GET['itemType'];?></span>
                            <div class="col-md-12">
                                <textarea class="form-control" id="messageRequestDelete" name="message" placeholder="* Please let us know why this <?php echo $_GET['itemType'];?> should be erased." rows="7"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <input type="submit" id="submitDelete" class="btn btn-primary btn-lg" value="Send"/>
                                <a href="../../includes/siteMap.php?id=<?php echo $_GET['siteId'];?>" class="btn btn-default btn-lg">Cancel</a>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
  </body>
</html>
