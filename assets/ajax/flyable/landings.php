<?php
	include ('../../../config/connection.php');
	header('Content-Type: application/json');
	
	// query sites
	$whereClause = " id=0 ";
	if (isset($_GET['iso']))  $whereClause = " iso LIKE '".$_GET['iso']."' ";
	if (isset($_GET['iso']) and $_GET['iso'] == "all") $whereClause = " 1 ";
	
	$qSite = 	"SELECT landing_name as name , iso, landing_lat as lat, landing_lng as lng, landing_altitude AS elev
				FROM site
				WHERE $whereClause AND (landing_lat != 0 OR landing_lng !=0)"; 
	$rSite = mysqli_query($bdd, $qSite);
	
	$json = '{ "landings" : [
';
	$i = 0;
	
	// browse sites
	while ($vSite = mysqli_fetch_array($rSite)) {
		 
		if ($i > 0) {
			$json.=',
';
		}
		$json .='  {
'/*    "name" : '.json_encode($vSite['name']).',*/
.'    "lat" : '.$vSite['lat'].',
    "lng" : '.$vSite['lng'].',
    "elev": '.$vSite['elev'].'
  }';
		$i++;
	}
	
	$qExtras  ="SELECT lat, lng, altitude as elev  
	FROM `site_extra_items` 
	left join site on site_extra_items.site=site.id 
	WHERE `type` LIKE 'landing' and $whereClause AND (site_extra_items.lat != 0 OR site_extra_items.lng !=0)";
	$rExtras = mysqli_query($bdd, $qExtras);
	
	// browse extra landings
	while ($vExtras = mysqli_fetch_array($rExtras)) {
		 
		$json .=',
  {
    "lat" : '.$vExtras['lat'].',
    "lng" : '.$vExtras['lng'].',
    "elev": '.$vExtras['elev'].'
  }';
		$i++;
	}
	
	
$json .= '
]
}';
echo $json;
?>
