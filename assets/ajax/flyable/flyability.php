<?php
	header('Content-Type: application/json');
	
	$siteId = $_GET['site'];
	$timeOffset =   $_GET['timeOffset'];
	
	$forecastFile = "forecasts/".$siteId.".json";
	
	if ( ! file_exists( $forecastFile ) or ( time()-filemtime($forecastFile) ) >  12*3600 ) {
		include ('import_weather.php');
	}
	
	$waitForecast = true;
	while ( $waitForecast ) {
		if ( file_exists($forecastFile) ) $waitForecast = false;
	}
	
	$data = file_get_contents($forecastFile);
	$json = json_decode($data, true);
	
	$forecasts = $json['forecasts'];
	$searchDate = true;
	
	$output = '{ ';
	
	foreach( $forecasts as $date => $forecast ) {
		
		
		if ( $searchDate && (time()+$timeOffset*3600) < $forecast["timestamp"] ) {
			
			$output .= '
  "localTime" : "'.$date.'",
  "flyability" : '.$forecast["flyability"].',
  "reason" : "'.$forecast["reason"].'"
';
			
			$searchDate = false;
			
			
		}
	}
	$output.= '}';
	echo($output);

?>
