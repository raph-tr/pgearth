 <?php 
	include '../../../config/connectionReadOnly.php';
	
	// query sites
	$whereClause = " id=0 ";
	if (isset($siteId)) $whereClause = " id = ".$siteID." ";
	if (isset($_GET['site'])) $whereClause = " id = ".$_GET['site']." ";
	if (isset($_GET['iso']))  $whereClause = " iso LIKE '".$_GET['iso']."' ";
	
	$qSite = "SELECT id, name, iso, lat, lng, N, NE, E, SE, S, SW, W, NW, takeoff_altitude AS elev FROM site WHERE $whereClause "; 
	$rSite = mysqli_query($bdd, $qSite);
	
	$flyability = 100;
	$reason = "";
	
	$nbSites = 0;
	$start_time = microtime(TRUE);
	
	// browse sites
	while ($vSite = mysqli_fetch_array($rSite)) {
		
		$nbSites += 1;
		
		// our forecast json file
		$fileName = "forecasts/".$vSite['id'].".json";
		
		/*
		$importNeeded =		! file_exists( $fileName )											// the file doesn't exist
							or time()-filemtime($fileName) >  12*3600 ;  						// it is past 8am and the file is more than 12h old
							or ( (gmdate("h", time()) > 8 and gmdate("h", time()) < 20) and gmdate("h", filemtime($fileName)  );	// it is between 8am and 8pm and the file is older than today 8am
		*/


		// if file doesn't exist or stored file stored is more than 12hours old : write new file
		if ( ! file_exists( $fileName ) or time()-filemtime($fileName) >  12*3600 ) {
			
			// compile wind directions
			$orientationString = "".$vSite['N'] . $vSite['NE'] . $vSite['E'] . $vSite['SE'] . $vSite['S'] . $vSite['SW'] . $vSite['W'] . $vSite['NW'] ."";
			if ( $orientationString == "00000000" ) {
				$knownDir = false;
			} else {
				$knownDir = true;
			}
			
			
			// Let's start our JSON content for the site
			$importedJSON = '{';
			$importedJSON .= '
"site" : '.json_encode($vSite['name']).',
"id" : '.$vSite['id'].',
"orientations" : "'.$orientationString.'",
"forecasts" : { ';
			
			// if we know site directions,  compile forecasts (or do it even if we dont anyway... ! )
			if ($knownDir || ! $knownDir ) {
				
				// Read JSON file
				$json = file_get_contents("https://node.windy.com/forecast/ecmwf/".$vSite['lat']."/".$vSite['lng']."?source=pgearth");
				//Decode JSON
				$json_data = json_decode($json,true);
				
				//Compute data and build json
				foreach ($json_data['data'] as $day => $weathers){   // for each day
				
					foreach($weathers as $index => $weather){    // for each forecast of the day (one forecast every 3hours..)
					
						//testing the data
						testWindSpeed($weather['wind']);
						testWindDir($weather['windDir'], $vSite['N'], $vSite['NE'], $vSite['E'], $vSite['SE'], $vSite['S'], $vSite['SW'], $vSite['W'], $vSite['NW'] );
						testRain($weather['rain']);
						testCloud($weather['cbase'], $vSite['elev']);
						
						if ($reason <> "") $reason = substr($reason, 0, -2);
						if ($weather['cbase']) $cbase = $weather['cbase'];
						else $cbase = 'null';
						
						// writing the JSON forecast for the 
						$importedJSON .= '
	"'.$weather['origDate'].'" : {';
						$importedJSON .= '
		"windSpeed" : '.$weather['wind'].',
		"windDir" : '.$weather['windDir'].',
		"rain" : '.$weather['rain'].',
		"cloudbase" : '.$cbase.',
		"flyability" : '.$flyability.',
		"reason" : "'.$reason.'",
		"timestamp" :'.($weather['ts']/1000).'
		},';
				
						// reset criteria for next forecast
						$flyability = 100;
						$reason = "";
					}
				} // end of "each day"
			} // end of "if we know directions"
			
			$importedJSON = substr($importedJSON, 0, -1);  // strip last comma
			
			// end our JSON :
			$importedJSON .= '
	}
}';
			// writes json file
			file_put_contents ( "forecasts/".$vSite['id'].".json" , $importedJSON);
			
		} // endof new file was needed

	}  // end of "for each site from the db query"
	

/*	// script duration
	$end_time = microtime(TRUE);
	$time_taken =($end_time - $start_time);
	$time_taken = round($time_taken,5);
	 
	echo $nbSites." sites<br />";
	echo 'Page generated in '.$time_taken.' seconds.';
*/	
	
	
	
	
	////////////////// evaluating conditions ////////////////////////////
	 
	function testWindSpeed($wind){
		global $flyability, $reason;
		if ($wind > 5) {
			$flyability = $flyability * 0;
			$reason .= "windSpeed, ";
		}
	}
	
	function testWindDir($windDir, $n, $ne, $e, $se, $s, $sw, $w, $nw) {
		global $flyability, $reason, $knownDir;
			if ($knownDir) {
				if ( ! ( (($windDir >= 337.5 or $windDir < 22.5) and $n > 0 ) 
				or ($windDir >= 22.5  and $windDir < 67.5  and $ne > 0) 
				or ($windDir >= 67.5  and $windDir < 112.5 and $e > 0 ) 
				or ($windDir >= 112.5 and $windDir < 155.5 and $se > 0) 
				or ($windDir >= 155.5 and $windDir < 202.5 and $s > 0 ) 
				or ($windDir >= 202.5 and $windDir < 247.5 and $sw > 0) 
				or ($windDir >= 247.5 and $windDir < 292.5 and $w > 0 ) 
				or ($windDir >= 292.5 and $windDir < 337.5 and $nw > 0) ) ) {
					$flyability = $flyability * 0;
					$reason .= "windDir, ";
				}
			} else {
				$flyability = $flyability * -0.5;
				$reason .= "orientationsUnknown, ";
			}
	}
	
	function testRain($rain) {
		global $flyability, $reason;
		if ($rain != 0) {
			$flyability = $flyability * 0;
			$reason .= "rain, ";
		}
	}
	
	function testCloud($cbase, $elev) {
		global $flyability, $reason;
		if ($cbase and $cbase <= $elev ){
			$flyability = $flyability * 0;
			$reason .= "cloudbase, ";
		}
	}
?>
