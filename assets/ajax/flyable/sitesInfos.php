<?php
	include ('../../../config/connection.php');
	header('Content-Type: application/json');
	
	// query sites
	$whereClause = " id=0 ";
	if (isset($_GET['site'])) $whereClause = " id = ".$_GET['site']." ";
	if (isset($_GET['iso']))  $whereClause = " iso LIKE '".$_GET['iso']."' ";
	
	$qSite = 	"SELECT id, name, iso, lat, lng, N, NE, E, SE, S, SW, W, NW, takeoff_altitude AS elev
				FROM site
				WHERE $whereClause AND (lat != 0 OR lng !=0)"; 
	$rSite = mysqli_query($bdd, $qSite);
	
	$json = '{ "sites" : [
';
	$i = 0;
	
	// browse sites
	while ($vSite = mysqli_fetch_array($rSite)) {
		
		if ($i > 0) {
			$json.=',
';
		}
		$json .='  {
    "name" : '.json_encode($vSite['name']).',
    "id" : '.$vSite['id'].',
    "lat" : '.$vSite['lat'].',
    "lng" : '.$vSite['lng'].',
    "elev": '.$vSite['elev'].',
    "orientations" : "'.$vSite['N'] . $vSite['NE'] . $vSite['E'] . $vSite['SE'] . $vSite['S'] . $vSite['SW'] . $vSite['W'] . $vSite['NW'] .'"
  }';
		$i++;
	}
$json .= '
]
}';
echo $json;
?>
