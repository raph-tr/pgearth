<?php
include dirname(__FILE__) . '/../../member/ASEngine/AS.php';

if (! app('login')->isLoggedIn()) {
	$member_edit = 0;
} else {
	$member_edit = 1;
}

header('Content-Type: application/json; charset=utf-8');
//session_start();

$site_id  = $_GET['siteID'];
include "../../config/connection.php";
//echo $siteID;

//echo "logged : ".$_SESSION['user_login_status'];
if (isset($_SESSION['userFullName'])) $member_edit = 1;

/* , X(`position`) as lat, Y(`position`) as lng */
$query = "SELECT * FROM site_extra_items WHERE site =".$site_id .";"; // used to get some info 
//echo $query;

$result = mysqli_query($bdd, $query);


$out = '{
';
  
$out .= '
	"item" : [   '	;

while ($valItem = mysqli_fetch_array($result)){
	$out .= '
		{"name": '.json_encode($valItem['name']).',
		"type":  '.json_encode($valItem['type']).',
		"id":  '.json_encode($valItem['id']).',
		"lat": '.json_encode($valItem['lat']).',
		"lng": '.json_encode($valItem['lng']).',
		"altitude": '.json_encode($valItem['altitude']).',
		"description": '.json_encode($valItem['description']).'
		},';
}
$out = substr($out, 0, -1);
$out .='
	],';

$out .= '
	"member_edit": '.json_encode($member_edit).'}';


echo $out;

//echo $query;

?>
