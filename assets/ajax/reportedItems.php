<?php
include '../../member/ASEngine/AS.php';
include '../../config/connectionReadOnly.php';

if (! app('login')->isLoggedIn() ) exit('Log in, please...');
$user = app('current_user');

if ( $user->role_id < 2 ) exit('Restricted area here, dude...');

// list items

$q = "select * from reported_items where done = 0 order by type, date";
$r = mysqli_query($bdd, $q);
?>
<script>
	$(document).ready(function(){
		$('[data-toggle="popover"]').popover();
	});
	
	function dismissRequest(id){
		$.post(
			"assets/ajax/admin/reportRequestDismiss.php",
			{'id': id},
			function(data){
				if (data=="done!") $('#requestRow_'+id).hide(600);
			}
		);
	}
</script>
<?php
echo "<table class='table table-striped table-bordered table-condensed'>";
echo "<thead><tr><th>Date</th><th>Type</th><th>Link</th><th>Request by</th><th>Reason</th><th>Delete</th><th>Dismiss</th></tr></thead><tbody>";
while ($v = mysqli_fetch_array($r) ) {
	
	echo "<tr id='requestRow_".$v['id']."'>
	<td class='text-center'>".$v['date']."</td>
	<td class='text-center'>".$v['type']."</td>
	<td>";
	if ($v['type'] == "picture") { ?>
		      <a href="#" data-toggle="popover" data-trigger="hover" data-placement="right" title="pic" data-content="<img src='assets/img/sites-pictures/<?php  echo $v["item_name"];?>'>">view pic</a><br />
		      <a href="assets/img/sites-pictures/<?php  echo $v["item_name"];?>" data-lightbox="<?php echo $v['id'];?>" data-title="<?php echo $v['reason'];?>" title=""><img src='assets/img/sites-pictures/<?php  echo $v["item_name"];?>' height="60px"></a>
	<?php } else  if ($v['type'] == "site" or $v['type'] == "club" or $v['type'] == "pro" ){ ?>
		       <a href="/?<?php echo $v['type']."=".$v['item_id']; ?>"><?php echo $v['item_name']; ?></a>
	<?php } else if ( $v['type'] == "alternate takeoff" or $v['type'] == "alternate landing" or $v['type'] == "alternate parking" ) { 
			   $vSite = mysqli_fetch_array ( mysqli_query( $bdd, "select site from site_extra_items where id = ".$v['item_id'] ) );
			   echo $v['item_name']." (<a href='/?site=".$vSite['site']."'>site</a>)"; 
		  } else if ( $v['type'] == "landing" or $v['type'] == "landing parking" or $v['type'] == "takeoff parking" ) { 
			   echo $v['item_name']." (<a href='/?site=".$v['item_id']."'>site</a>)"; 
		  } 
	echo "</td>
	<td class='text-center'>".$v['requested_by_name']."</td>
	<td>".$v['reason']."</td>";
	if ($v['type'] == "site") echo " <td class='text-center'><a  onClick=\"return confirm('Are you REALLY sure ?'); \" href='/assets/ajax/admin/siteDelete.php?site=".$v['item_id']."' target='_blank' data-toggle='popover' data-trigger='hover' data-placement='left' title='Definitive !' data-content='delete item ! YOU MUST BE SURE !!!!'><i class='fa fa-trash'></i></a></td>";
	else if ($v['type'] == "picture") echo " <td class='text-center'><a  onClick=\"deleteReportedPicture('".$v['item_name']."', ".$v['id'].")\" href='#' data-toggle='popover' data-trigger='hover' data-placement='left' title='Definitive !' data-content='delete item ! YOU MUST BE SURE !!!!'><i class='fa fa-trash'></i></a></td>";
	else if ($v['type'] == "club" or $v['type'] == "pro" ) echo " <td class='text-center'><a  onClick=\"deleteReportedStructure('".$v['type']."', ".$v['item_id'].", ".$v['id'].")\" href='#' data-toggle='popover' data-trigger='hover' data-placement='left' title='Definitive !' data-content='delete item ! YOU MUST BE SURE !!!!'><i class='fa fa-trash'></i></a></td>";
	else if ($v['type'] == "picture") echo " <td class='text-center'><a  onClick=\"deleteReportedPicture('".$v['item_name']."', ".$v['id'].")\" href='#' data-toggle='popover' data-trigger='hover' data-placement='left' title='Definitive !' data-content='delete item ! YOU MUST BE SURE !!!!'><i class='fa fa-trash'></i></a></td>";
	else if ( stripos($v['type'], 'alternate') !== FALSE or stripos($v['type'], 'parking') !== FALSE or $v['type'] == "landing" ) echo " <td class='text-center'><a  onClick=\"deleteReportedItem('".$v['type']."', ".$v['item_id'].", ".$v['id'].")\" href='#' data-toggle='popover' data-trigger='hover' data-placement='left' title='Definitive !' data-content='delete item ! YOU MUST BE SURE !!!!'><i class='fa fa-trash'></i></a></td>";

	else echo " <td class='text-center'><i class='fa fa-trash'></i></td>";
	echo "<td class='text-center'><a href='#' data-toggle='popover' data-trigger='hover' data-placement='left' title='Dismiss request' data-content='item is kept, report is dismissed : for inappropriate requests' onclick='dismissRequest(".$v['id'].");'><i class='fa fa-times'></i></a></td>
</tr>";
}
echo "</tbody></table>";
?>


<script>
	function deleteReportedPicture( picName, id ) { 
		$.post("assets/ajax/updateItem/deletePictureSave.php", { picture: picName, reported: 'true' }, function(data){
			if (data == "file deleted - db entry deleted - reported_items entry ok" ) {
				alert("Picture deleted");
				$("#requestRow_"+id).hide(600);
			}
		});
	}

	function deleteReportedStructure( structure, id, reportId ) { 
		$.post("assets/ajax/admin/clubOrProDelete.php", { structure: structure, id: id, reportId : reportId }, function(data){
			if (data == "db entry deleted - reported_items entry ok" ) {
				alert(structure+" deleted");
				$("#requestRow_"+reportId).hide(600);
			}
		});
	}

	function deleteReportedItem( type, id, reportId ) { 
		$.post("assets/ajax/admin/siteMapItemDelete.php", { type: type , id: id, reportId : reportId }, function(data){
			if (data == "db entry deleted - reported_items entry ok" ) {
				alert(type+" deleted");
				$("#requestRow_"+reportId).hide(600);
			}
		});
	}

</script>
