<?php

include "../../config/connection.php";

$user = $_REQUEST['userName'];

$query = "
	SELECT 	sites_users.item_id,  sites_users.user,  sites_users.date,
			GROUP_CONCAT(DISTINCT sites_users.modification) AS modifications,
			site.id, site.name, as_users.username, site.iso
	FROM `sites_users` 
	LEFT JOIN site ON sites_users.item_id = site.id
	LEFT JOIN as_users ON sites_users.user =  as_users.user_id
	WHERE 	 as_users.username LIKE '".$user."' 
	   AND   item_type LIKE 'site'
	GROUP BY sites_users.user, sites_users.item_id, sites_users.date
	ORDER BY date DESC, username, name";

$result = mysqli_query($bdd, $query);

$currentDate = "1212-12-12";
$currentUser ="totoPoil";

echo "<ul>";

while ($val = mysqli_fetch_array($result)){
	
	if ($val['date'] != $currentDate) {
		$currentDate = $val['date'];
		echo "</ul>".$val['date']." :<ul>";
	}
	echo "<li>".$val['username']." edited  <a href='#' class='openAnotherModal' modalToOpen='siteDetailsModal' id='".$val['item_id']."'>".$val['name']."</a> <img class='countryFlag' iso='".$val['iso']."'> &rarr; ".$val['modifications']."</li>";
}
echo "</ul>";

?>
