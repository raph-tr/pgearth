<?php
include dirname(__FILE__) . '/../../member/ASEngine/AS.php';

if (! app('login')->isLoggedIn()) {
	$member_edit = 0;
} else {
	$member_edit = 1;
}

header('Content-Type: application/json; charset=utf-8');

include "../../config/connection.php";

$pro_id  = $_GET['id'];

$query = "SELECT * FROM pro WHERE pro.id=".$pro_id .";";
$queryBis = "SELECT * FROM pro WHERE pro.id=".$pro_id .";";
$result = mysqli_query($bdd, $query);
$val = mysqli_fetch_array($result);

$query_country = "SELECT name FROM country WHERE country.iso=".$val['iso'].";";
$result_country = mysqli_query($bdd, $query_country);
$val_country = mysqli_fetch_array($result_country);

$resultBis = mysqli_query($bdd, $queryBis);

$out = '{
';

$out .= '	"body": {';
while($row = mysqli_fetch_assoc($resultBis)) {
    foreach($row as $key => $value) {
	$out .= '
		"'.$key.'" : '.json_encode($value).',';
    }
}
$out  = substr($out, 0, -1);
$out .='
	},
';


  
$rPros = mysqli_query($bdd, "select pro_site.site, site.name, site.iso from pro_site left join site on pro_site.site = site.id where pro_site.pro = $pro_id ");
$out .= '	"sites" : [   '	;
while ($vPros = mysqli_fetch_array($rPros)){
	$out .= '
		{
		"site" : '.json_encode($vPros['name']).',
		"id": '.$vPros['site'].',
		"iso": '.json_encode(strtolower($vPros['iso'])).'
		},';
}
$out = substr($out, 0, -1);
$out .='],';




$out.= '	"flag": '.json_encode('<img src="assets/img/flag/'.strtolower($val['iso']).'.png" title="'.$val_country['country.name'].'" />').',
';

$out .= '	"iso": '.json_encode(strtolower($val['iso'])).',
	"country_name" : '.json_encode($val_country['name']).',
	"lat": '.$val['lat'].',
	"lng": '.$val['lng'].',
	"member_edit": '.json_encode($member_edit).'
}';


echo $out;
?>
