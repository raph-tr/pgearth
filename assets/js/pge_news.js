$("#newsModal").on("shown.bs.modal", function () { 

	$.post("assets/ajax/weekNews.php", function (data){
		$("#newsModalModifications").html(data);
	});
	
	$.post("assets/ajax/weekNewsPics.php", function (data){
		$("#picturesModalModifications").html(data);
	});
	
	$.post("assets/ajax/weekNewsMembers.php", function (data){
		$("#membersModalModifications").html(data);
	});
	
	$.post("assets/ajax/weekNewsClubs.php", function (data){
		$("#clubsModalModifications").html(data);
	});
	
	$.post("assets/ajax/weekNewsPros.php", function (data){
		$("#prosModalModifications").html(data);
	});	
	
});
