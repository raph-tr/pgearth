$.fn.editable.defaults.mode = 'inline';

$(document).ready(function() {
    $('#takeoff_edit').editable();
});

$(document).ready(function() {
    $('#landing_edit').editable();
});

$(document).ready(function() {
    $('#weather_edit').editable();
});

$(document).ready(function() {
    $('#rules_edit').editable();
});

$(document).ready(function() {
    $('#access_edit').editable();
});

$(document).ready(function() {
    $('#tourism_edit').editable();
});

$(document).ready(function() {
    $('#comments_edit').editable();
});

$(document).ready(function() {
    $('#contact_edit').editable();
});

$.fn.editableform.buttons = 
  '<button type="submit" class="btn btn-success editable-submit btn-mini"><i class="fa fa-check"></i></button>' +
 '<button type="button" class="btn editable-cancel btn-mini"><i class="fa fa-close"></i></button>';         
