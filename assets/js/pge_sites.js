/* ************ SITES    ************************************ */

/* Empty layer placeholder to add to layer control for listening when to add/remove sites to markerClusters layer */

var sites = L.geoJson(null, {			// sites layer...
	
  pointToLayer: function (feature, latlng) {
	
	if (feature.properties.closed == "1") {
		//console.log("closed !");
		var siteMarkerIcon = "assets/img/site-closed.png";
		var closedLabel = " - CLOSED SITE !";
	}
	else {
		//console.log("not closed");
		
		//todo : a function to set marker icon depending on filters or flyability
		var siteMarkerIcon  = "assets/img/site"+feature.properties.filters+".png";
		// setRelevantMarkerIcon(feature, feature.id, feature.properties.closed, feature.properties.filters);
		var closedLabel = "";
	}
    return L.marker(latlng, {   //  ...we build a marker....
      icon: L.icon({
        iconUrl: siteMarkerIcon,
        iconSize: [24, 28],
        iconAnchor: [12, 28],
        popupAnchor: [0, -25],
        labelAnchor: [4, -8],
        className: 'siteIcon '+feature.id+'siteIcon '+feature.properties.filters+'siteIcon'
        }),
      //title: feature.properties.NAME,
      alt : feature.properties.NAME,
      opacity: feature.properties.opacity,
      riseOnHover: true
      }).bindLabel(feature.properties.NAME + closedLabel+'<div id="'+feature.id+'flyableReason" class="flyableLabel" style="display:none"></div><div id="'+feature.id+'_labelInfo" style="display:none"></div>',{  // ... with label...
		  noHide : true,
		  direction:'right',
		  className : 'siteLabel '+feature.id+'siteLabel '+feature.properties.filters+'siteLabel',
		  opacity: feature.properties.opacity
	  });
  },
  
  onEachFeature: function (feature, layer) {   //... for each of these markers...
	
	// console.log('oneachfeature : ' + feature.id);  
    if (feature.properties) { 
	  
	  if (feature.properties.filters < 1) {  // site doesn't meet filters requirements
			layer.setOpacity(0.3, true);
	  }
		
	  // calculate site denivelation if possible
	  if (feature.properties.denivelation == -1 ) {  
		  alt = '?';
		  // console.log(alt);
	  }   else {
		alt = feature.properties.denivelation+'m';
	  }
	  deniv =  '<i class="fa fa-mountain"></i><i class="fa fa-arrows-v"></i> '+alt+'<br />';
		
		// ... a pop up content is defined... 
      var content = '<div style="background:white; display: inline-block; padding-right: 7px;">';
	  content += '<a href="#" onclick="siteModal('+feature.id+', false)" title="'+feature.properties.NAME+'"><img src="assets/img/windrose/26/'+feature.id+'.png" class="windrose26" style="float:left" /> <strong>'+feature.properties.NAME+'&nbsp;<img src="assets/img/flag/'+feature.properties.country+'.png" /></strong><br />';
	  content += '<img src="assets/img/ranking/'+Math.round(feature.properties.ranking*2)/2+'.png" title="ranked '+Math.round(feature.properties.ranking*10)/10+'/5 with '+feature.properties.number_votes+' votes" /><br/>';
	  content += '<img src="assets/img/flying/25/'+feature.id+'.png" class="flying25" /><br />';
	  content += deniv;
	  content += '<em>Click for details...</em></a>';
      content += '</div>';
      
      layer.on({
        click: function (e) {          //  ... and the pop up is added...
			var popup = L.popup({offset:[0,-15]}).setLatLng(L.latLng(feature.geometry.coordinates[1], feature.geometry.coordinates[0])).setContent(content);
			popup.openOn(map);
			highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
			showSiteItems (feature.id);
			//showSiteItems (feature.id, feature.geometry.coordinates[1], feature.geometry.coordinates[0]);
		},
		mouseover : function(e) {
			// detailed label content is built and shown
			if (feature.properties.denivelation == -1 ) { 
				deniv = '<span style="color:#a4a4a4">'; 
				alt = '?';
			}   else {
				alt = feature.properties.denivelation+'m';
				deniv = '<span>';
			}
			deniv +=  '<i class="fa fa-mountain"></i><i class="fa fa-arrows-v"></i> '+alt+'</span>';
			var contentLabel = '<div style="display: inline-block; padding-right: 7px;">'; 
			contentLabel += '<img src="assets/img/flying/25/'+feature.id+'.png" class="flying25" /><br />';
			contentLabel += '<img src="assets/img/windrose/26/'+feature.id+'.png" class="windrose26"/>&nbsp;&nbsp;';
			contentLabel += deniv;
			contentLabel += '&nbsp;&nbsp;<img src="assets/img/flag/'+feature.properties.country+'.png" />&nbsp;';
			contentLabel += '<img src="assets/img/ranking/'+Math.round(feature.properties.ranking*2)/2+'.png" />&nbsp;';
			contentLabel += '</div>';

			// add the label
			$("."+feature.id+"siteLabel").css({"background":"white", "z-index": ""+$("."+feature.id+"siteIcon").css("z-index")});
			$("#"+feature.id+"_labelInfo").html(contentLabel);
			$("#"+feature.id+"_labelInfo, #"+feature.id+"flyableReason").show("fast");
			this.setOpacity(1, true);

        },
        mouseout: function(e) {
			// label is "un-detailed"
        	$("."+feature.id+"siteLabel").css({"background":"rgba(235, 235, 235, 0.71)", "z-index":""+$("."+feature.id+"siteIcon").css("z-index")});
        	$("#"+feature.id+"_labelInfo, #"+feature.id+"flyableReason").hide("fast");
        	if (feature.properties.filters < 1 ) {
        		this.setOpacity(0.3, true);
        	}
        }
      });
      
    }
  }
});

function clickEditIcon(entry, id) {
	$("#"+entry+"_"+id+"").addClass("editable-click");
	$("#"+entry+"_"+id+"").editable("activate");
}



function siteModal(id, teleport = true){     // we define the modal content with an ajax call
	
	$.getJSON("assets/ajax/siteModalJSON.php",{id: id},  function (json) {
		
		if (json.result == 1) {         /* *************** We do have a site!!!: ( ********/
		
			if (json.body['closed']==1) {
				$('#site-modal-content').addClass('site-closed');
				// console.log('closed');
			} else {
				$('#site-modal-content').removeClass('site-closed');
			}
			
			var siteId = json.body['id'];
			var siteName = json.body['name'];
			var addSite = "";
			var ranking = Math.round(json.body['ranking']*2)/2;
				orientationForm = '<div id="orientationForm"></div>';
				flyingForm = '<div id="flyingForm"></div>';
				rankingForm = '<div id="rankingForm"></div>';
				calendarForm = '<div id="calendarForm"></div>';

			/* ***    french site => ffvl   *** */
			if (json.body['ffvl_suid'] != 0) 
				ffvlNotice = "<div class='alert-info ffvl-site-alert'><img src='assets/img/ffvl-icon.png' /> <a class='alert-link' href='http://federation.ffvl.fr/sites_pratique/voir/"+json.body['ffvl_suid']+"' target='_blank'>FFVL official site page [fr] <i class=\"fa fa-external-link\"></i></a><br />French flying sites are registered at <a href='http://carte.ffvl.fr/?mode=parapente&lat="+json.body['lat']+"&lon="+json.body['lng']+"&zoom=16' class='alert-link' target='_blank'>FFVL.FR</a>, the French Free Flight Association.<br />The most recent updates about this area <a href='http://carte.ffvl.fr/?mode=parapente&lat="+json.body['lat']+"&lon="+json.body['lng']+"&zoom=16' class='alert-link' target='_blank'>can be found on their website <i class=\"fa fa-external-link\"></i></a>.<br />We highly recommand you to check FFVL information about this site before you come and fly here.</div>";
			else if (json.iso == 'fr' && json.body['ffvl_suid'] == 0) {
				ffvlNotice = "<div class='alert-info ffvl-site-alert'><img src='assets/img/ffvl-icon.png' /> French flying sites are registered at <a href='http://carte.ffvl.fr/?mode=parapente&lat="+json.body['lat']+"&lon="+json.body['lng']+"&zoom=16' class='alert-link' target='_blank'>FFVL.FR</a>, the French Free Flight Association.<br />The most recent updates about this area <a href='http://carte.ffvl.fr/?mode=parapente&lat="+json.body['lat']+"&lon="+json.body['lng']+"&zoom=16' class='alert-link' target='_blank'>can be found on their website <i class=\"fa fa-external-link\"></i></a>.<br />We highly recommand you to check FFVL information about this site before you come and fly here.</div>";
			} else ffvlNotice = '';


			/* **********    header      *************** */
			ranking = "<img src='assets/img/ranking/"+ranking+".png' id='rankingImg' title='ranked "+Math.round(json.body['ranking']*10)/10+"/5 with "+json.body['number_votes']+" votes' />";
			windrose = '<img id="windroseImg" src="assets/img/windrose/26/'+id+'.png" class="windrose26" style="float:left; margin-top: 7px"  />';
			flying = '<img style="margin:4px" id="flyingImg" src="assets/img/flying/25/'+id+'.png" class="flying25" />';
			calendar = '<img id="calendarImg" src="assets/img/calendar/80/'+id+'.png" />';

			if (json.member_edit==1){			
				siteName = "<span onclick='clickEditIcon(\"name\", "+json.body['id']+")' id='name_"+json.body['id']+"' data-name='name' class='editable' data-type='text' data-pk='"+json.body['id']+"' data-url='assets/ajax/updateItem/siteTextField.php' data-title='Enter name'>"+ json.body['name'] +"</span>";
				editNotice = "<p class='small'> <i class='fa fa-user'></i> <i class='fa fa-pencil-square-o'></i> <em>edit any field by clicking/tapping it.</em></p>";
				if (addSiteToItem != "") {
					addSiteSplited = addSiteToItem.split("-");
					addSite = "<div class='small'><img src='assets/img/"+addSiteSplited[0]+".png' /> <img src='assets/img/icons/famfamfam/add.png' /> <a href='#' id='addSiteToItem' class='"+addSiteToItem+"' > Add the site to my "+addSiteSplited[0]+"</a></div>";
				}
			} else {
				siteName = json.body["name"];
				editNotice='<p class="small"><em> <a href="#" class="openAnotherModal" modalToOpen="loginModal" ><i class="fa fa-user"></i> Login to edit information</a></em></p>';
			}
			
			if (json.body['takeoff_altitude'] != 0 && json.body['takeoff_altitude'] != 0) alt = parseInt(json.body['takeoff_altitude'])-parseInt(json.body['landing_altitude']);
			else alt = '?';
			altitude= '<div id="altitude" class="small"  title="height drop"><i class="fa fa-mountain"></i><i class="fa fa-arrows-v"></i> '+alt+' m</div>';
			gps = '<div id="gps" class="small"> <i class="fa fa-globe"></i> lat, lng : '+json.body["lat"]+', '+json.body["lng"]+'<br /><span id="direction" class="small"> <i class="fa fa-car"></i> <a href="http://www.openstreetmap.org/directions?engine=osrm_car&route=;'+json.body["lat"]+','+json.body["lng"]+'#map=10/'+json.body["lat"]+'/'+json.body["lng"]+'" target="_blank"></i> direction <i class="fa fa-external-link"></i></a></span></div>';
			
			if( json.body["landing_lat"] != 0 && json.body["landing_lng"] != 0 && json.body["hike"] == 1 ){
				gps += '<div class="small"><span class="small"><i class="fa fa-hiking"></i>';
				gps += '<a target="_blank" href="http://www.openstreetmap.org/directions?engine=graphhopper_foot&route='+json.body["landing_lat"]+'%2C'+json.body["landing_lng"]+'%3B'+json.body["lat"]+'%2C'+json.body["lng"]+'&layers=G">';
				gps += ' LZ to TO hiking direction</a>';
				gps += ' ( <a href="https://graphhopper.com/api/1/route?point='+json.body["landing_lat"]+','+json.body["landing_lng"]+'&point='+json.body["lat"]+','+json.body["lng"]+'&vehicle=foot&debug=true&key=45b21a02-a2f4-49cd-8af8-34a491474b67&type=gpx">gpx</a>  /  ';
				gps += ' <a target="_blank" href="https://graphhopper.com/maps/?point='+json.body["landing_lat"]+'%2C'+json.body["landing_lng"]+'&point='+json.body["lat"]+'%2C'+json.body["lng"]+'&locale=en&vehicle=foot&weighting=fastest&elevation=true&use_miles=false&layer=TF Cycle">graphhopper</a> )  <i class="fa fa-external-link"></i></span></div>';
			}

			/* **********  modal title ************** */

			htmlString =  '<div class="row"><div class="col-12 col-sm-6">'+editNotice+windrose+'<div style="padding-left: 37px"><span style="font-size: large"> '+ siteName +'</span> <img class="countryFlag" iso="'+json.iso+'" /><br />'+ranking+'</div>';
			htmlString += orientationForm+rankingForm+'<div>'+flying+flyingForm+'</div>'+gps+addSite+'</div>';
			htmlString += '<div class="col-6 col-sm-2"> <br />'+altitude+' <br />'+calendar+calendarForm+'</div>';
			htmlString += '<div class="col-6 col-sm-4 small">';

			htmlString += '<div class="alert-danger"><a href="#" modalToOpen="reportItemModal" class="openAnotherModal" itemType="site" itemName="'+json.body['name']+'" itemId="'+json.body['id']+'"><i class="fa fa-trash"></i> Delete this site ?</a></div>';  // delete site

			/* *********** get orientation to link to the sim **************** */
			
			orientations = [json.body["N"], json.body["NE"], json.body["E"], json.body["SE"], json.body["S"], json.body["SW"], json.body["W"], json.body["NW"] ];
		//	console.log(orientations);
			averageOrientation = 0;
			for (i = 1; i < 8; i++) {
				if ( orientations[i] == 2) {
					averageOrientation = 45*i;
					j=i+1;
					while ( orientations[j] == 2  && j < 8 ) {
						averageOrientation += 22.5;
						j++;
					}
					break;
				}
				if ( orientations[i] == 1) {
					averageOrientation = 45*i;
					j=i+1;
					while ( orientations[j] == 1  && j < 8 ) {
						averageOrientation += 22.5;
						j++;
					}					
				i = j-1;
				}
			}
		//	console.log(averageOrientation);

			htmlString += '<div style="padding: 7px"><a href="http://paraglidinglogbook.com/simulator/custom-takeoff.php?startlat='+json.body["lat"]+'&startlon='+json.body["lng"]+'&orientation='+averageOrientation+'" target="_blank"><img src="assets/img/icons/flight_sim.png" /> Fly this site online with pglogbook.com simulator ! <i class="fa fa-external-link"></i></a></div>';
			htmlString += '</div>';  // end of third col
			htmlString += '</div>'; // end of title row div (?)
			

			$("#modal-site-title").html( htmlString );
			$("#orientationForm").toggle();
			$("#rankingForm").toggle();
			$("#flyingForm").toggle();
			$("#calendarForm").toggle();
			

			/* ********** do our ajax stuff here to save data from header fields ********** */
			$("#windroseImg").on("click", function(){
				$("#orientationForm").load("assets/ajax/updateItem/siteOrientationForm.php", { "id": id });
				$("#orientationForm").toggle("slow");
				}); 
			$("#rankingImg").on("click", function(){
				$("#rankingForm").load("assets/ajax/updateItem/siteRankingForm.php", { "id": id });
				//console.log('ranking');
				$("#rankingForm").toggle("slow");
				}); 
			$("#flyingImg").on("click", function(){
				$("#flyingForm").load("assets/ajax/updateItem/siteFlyingForm.php", { "id": id });
				$("#flyingForm").toggle("slow");
				}); 
			$("#calendarImg").on("click", function(){
				$("#calendarForm").load("assets/ajax/updateItem/siteCalendarForm.php", { "id": id });
				$("#calendarForm").toggle("slow");
				}); 
			$("#addSiteToItem").on("click", function(){
				$("#addSiteToItem").load("assets/ajax/updateItem/addSiteToItem.php", { "addSiteToItem": addSiteToItem+"-"+id });
		//		console.log(addSiteToItem+"-"+id);
				}); 
			$("#deleteSiteLink").on("click", function(){
				$("#deleteSiteForm").load("assets/ajax/deleteSiteRequestForm.php", { "id": id, "sitename": json.body["name"]});
				$("#deleteSiteForm").toggle("slow");
				}); 
			
			$("#orientationForm").hide();
			$("#flyingForm").hide();		
			$("#calendarForm").hide();		
			$("#deleteSiteForm").hide();		


			/* ********** text fields table tab     *************** */				
			var bodyHTML = "<table id='siteDescTable' class='table table-striped table-bordered table-condensed'><tbody>";
			var items= ['takeoff', 'landing', 'weather', 'rules', 'access', 'comments', 'contact', 'web', 'tourism'];
			items.forEach(function(entry) {

				bodyHTML += "<tr><th><strong>"+entry+"</strong></th>";
				bodyHTML += "<td class='siteDescCell'>";
				if (entry=='rules') bodyHTML += ffvlNotice;
				if (entry=='web' && json.body['ffvl_suid'] != 0)  bodyHTML+=' https://intranet.ffvl.fr/sites_pratique/voir/'+json.body['ffvl_suid'];
	 
				if (json.member_edit==1){
					var entryContent = json.body[entry];
					if (entryContent=="" || entryContent==" ") entryContent = "...";
					bodyHTML += "<div onclick='clickEditIcon(\""+entry+"\", "+json.body['id']+")' id='" + entry + "_"+json.body['id']+"' data-name='" + entry + "' class='editable' data-type='textarea' data-pk='"+json.body['id']+"' data-url='assets/ajax/updateItem/siteTextField.php' data-title='Enter "+entry+"'> "+ entryContent +"</div>";
				} else {
					bodyHTML += "<small>"+ textToLink(json.body[entry]) +"</small>";
				}

				bodyHTML += "</td></tr>";
			});
			bodyHTML+="</tbody></table>";
			
			/* ********** populate the modal...     *************** */		
			$("#modal-site-body-description").html( bodyHTML );
			
			/* ********** POPOVER CALL AND TWEAK TO REMAIN OPEN WHILE HOVERED *********/	
			$('[data-toggle="popover"]').popover(); 	        
				
			$(".pop").popover({ trigger: "manual" , html: true, animation:false})
				.on("mouseenter", function () {
					var _this = this;
					$(this).popover("show");
					$(".popover").on("mouseleave", function () {
						$(_this).popover('hide');
					});
				}).on("mouseleave", function () {
					var _this = this;
					setTimeout(function () {
						if (!$(".popover:hover").length) {
							$(_this).popover("hide");
						}
					}, 300);
			});

			
			/* ********** pictures tab     *************** */
			var picturesHTML = '';
			picturesArray=[];
			var hasPictures = 0;
			
			for(var k in json.pictures) {
				hasPictures++;
				picturesArray.push(json.pictures[k].picture);
		//		picturesHTML += "<li><img id='"+json.pictures[k].picture+"'  class='sitePictureThumbnail thumbed' src='assets/img/sites-pictures/"+json.pictures[k].picture+"' width='100px' alt=\"" + json.pictures[k].comment + "\" /><span class='thumbnailOverlay'><!--<a href='#'><i class='fa fa-trash'></i></a>--></span></li>";
				picturesHTML += '<span class="imgSpan">';
				picturesHTML += '<a href="assets/img/sites-pictures/'+json.pictures[k].picture+'" data-lightbox="siteGallery" data-title="' + json.pictures[k].comment + '" title="' + json.pictures[k].comment + '">';
				picturesHTML += '<img src="assets/img/sites-pictures/'+json.pictures[k].picture+'" width="100px" height="100px" class="sitePicImg" />';
				picturesHTML += '</a>';
	//			alertText = json.pictures[k].picture+' clicked';
				picturesHTML += '<div class="sitePicLegend"><span onclick="javascript:deletePicture(\''+json.pictures[k].picture+'\');" data-trigger="hover" data-toggle="tooltip" title="delete/report this image" > <i class="fa fa-trash"></i> / <i class="fa fa-minus-circle"></i></span></div>';
				picturesHTML += '</span>';
			}
			
			picturesHTML += "";
			if (hasPictures == 0) picturesHTML = "no pictures";
			$("#modal-site-body-pictures").html( picturesHTML );
			$('[data-toggle="tooltip"]').tooltip(); 
			$(".pictureComment").hide();
			
			$("#modal-site-body-pictures").append( '<div id="deletePicture"></div>' );
			
			if (json.member_edit==1){			
				$("#modal-site-body-pictures").append( '<div id="picturesUpload" class="alert-warning">load pictures...</div>' );
				$("#picturesUpload").load('assets/external-scripts/ajax-img-upload/index.php', {"memberEdit":1, "siteId": siteId });
			}

			

			/* ********** weather tab  *************** */
			
			//var windyWeather = '<iframe src="includes/windy.php?lat='+json.body["lat"]+'&lng='+json.body["lng"]+'" width="90%" height="400px" frameborder="0"></iframe>';
			var windySrc = 'https://embed.windy.com/embed2.html?lat='+json.body["lat"]+'&lon='+json.body["lng"]+'&zoom=9&level=surface&overlay=wind&menu=&message=&marker=true&calendar=now&pressure=&type=map&location=coordinates&detail=true&detailLat='+json.body["lat"]+'&detailLon='+json.body["lng"]+'&metricWind=m%2Fs&metricTemp=%C2%B0C&radarRange=-1';
				
			$('#modal-site-weather-tab').on('shown.bs.tab', function (e) {
				//$("#siteWeatherIframe").html( windyWeather ); 
				//console.log(windySrc);
				$('#windyIFrame').attr('src',windySrc);
				
			});
			
			/* ********** map tab  *************** */
			// siteMap = '<iframe width="100%" height="317" src="http://paragliding.earth/includes/siteMap.php?id='+id+'" frameborder="0"></iframe>'; 

			$("#siteMapIframe").html(""); 
			
			siteMap = '<div class="siteMapGpsAlt">takeoff : '+json.body["lat"]+', '+json.body["lng"]+' - '+json.body["takeoff_altitude"]+'m<br />'; 
			siteMap += 'landing : '+json.body["landing_lat"]+', '+json.body["landing_lng"]+' - '+json.body["landing_altitude"]+'m<br />'; 
			siteMap += 'parking TO : '+json.body["takeoff_parking_lat"]+', '+json.body["takeoff_parking_lng"]+' - '+json.body["takeoff_parking_altitude"]+'m<br />'; 
			siteMap += 'parking LZ : '+json.body["landing_parking_lat"]+', '+json.body["landing_parking_lng"]+' - '+json.body["landing_parking_altitude"]+'m<br /></div>'; 
			$("#siteMapText").html( siteMap );

			if ( json.body["landing_lat"] != 0 && json.body["landing_lng"] != 0 ) {
				siteMapDirection  = '<a target="_blank" href="http://www.openstreetmap.org/directions?engine=graphhopper_car&route='+json.body["landing_lat"]+'%2C'+json.body["landing_lng"]+'%3B'+json.body["lat"]+'%2C'+json.body["lng"]+'&layers=CG">';
				siteMapDirection += '<i class="fa fa-car"></i> LZ to TO driving direction</a><br />';
				siteMapDirection += '<a target="_blank" href="https://graphhopper.com/maps/?point='+json.body["landing_lat"]+'%2C'+json.body["landing_lng"]+'&point='+json.body["lat"]+'%2C'+json.body["lng"]+'&locale=en&vehicle=foot&weighting=fastest&elevation=true&use_miles=false&layer=TF Cycle">';
				siteMapDirection += '<i class="fa fa-hiking"></i> LZ to TO hiking direction</a>';
				siteMapDirection += ' ( <a href="https://graphhopper.com/api/1/route?point='+json.body["landing_lat"]+','+json.body["landing_lng"]+'&point='+json.body["lat"]+','+json.body["lng"]+'&vehicle=foot&debug=true&key=45b21a02-a2f4-49cd-8af8-34a491474b67&type=gpx">gpx</a>  /  ';
				siteMapDirection += ' <a target="_blank" href="http://www.openstreetmap.org/directions?engine=graphhopper_foot&route='+json.body["landing_lat"]+'%2C'+json.body["landing_lng"]+'%3B'+json.body["lat"]+'%2C'+json.body["lng"]+'&layers=CG">osm</a> )';
				
				$("#siteMapDirection").html( siteMapDirection );
			}

			$('#modal-site-map-tab').on('shown.bs.tab', function (e) {
				siteMap = '<iframe width="100%" height="617" src="includes/siteMap.php?id='+id+'" frameborder="0"></iframe>'; 
				$("#siteMapIframe").html( siteMap ); 
			});


			/* **** flyability tab :  ajax load of site flyability *******/
			$('#modal-site-flyable-tab').on('shown.bs.tab', function (e) {
				siteFlyableTab(siteId);
			});
			

			/* ********** pros &clubs tab  *************** */
			var usersHTML = "<i class='fa fa-user-check'></i>&nbsp;Pros : <ul>";
			var none = "<li>none</li>"; var some="";
			for(var k in json.pros) {
				none=""; some += "<li style='list-style-image:url(assets/img/flag/_tn_"+json.pros[k].iso+".png);'><a href='#' class='openAnotherModal' modalToOpen='featureModal' feature='pro' id='"+json.pros[k].id+"'>"+json.pros[k].pro+"</a></li>";
			}
			usersHTML += none+some+"</ul>";
			usersHTML += "<i class='fa fa-users'></i>&nbsp;Clubs : <ul>";
			none="<li>none</li>"; some="";
			for(var k in json.clubs) {
				none=""; some += "<li style='list-style-image:url(assets/img/flag/_tn_"+json.clubs[k].iso+".png);'><a href='#' class='openAnotherModal' modalToOpen='featureModal' feature='club' id='"+json.clubs[k].id+"'>"+json.clubs[k].club+"</a></li>";
			}
			usersHTML += none+some+"</ul>";
			
			if (json.member_edit==1){			
				usersHTML += "<p><em>To add a pro or a club activity on this site, start by opening the pro/club window : you will there find a link to click, then you can come back here to validate the relationship.</em></p>";
			}
			$("#modal-site-body-users").html( usersHTML );		


			/* ********** history tab  *************** */
			var historyHTML = "As far as we can tell, this site file was contributed to by: <ul class='siteHistory'>";
			var none = "<li>none</li>"; var some="";
			for(var k in json.history) {
				none="";
				some += '<li><a href="#" member="'+json.history[k].userName+'" class="openAnotherModal" modalToOpen="memberModal" >'+json.history[k].userName+'</a>, <em>'+json.history[k].userDate+', '+json.history[k].userModification+'</em></li>';
			}
			historyHTML += none+some+"</ul>Thanks to them ! :)";
			$("#modal-site-body-history").html( historyHTML );		


			/* *********** flights tab **************/
			$( "#modal-site-body-flights-leonardo" ).load( "assets/ajax/leonardoFlights.php", { lat: json.body['lat'], lng: json.body['lng']  } );
			$( "#modal-site-body-flights-xcontest" ).load( "assets/ajax/xcontestFlights.php", { lat: json.body['lat'], lng: json.body['lng']  } );
/*			$( "#simForm-location" ).val( siteName );
			$( "#simForm-startlat" ).val( json.body['lat']  );
			$( "#simForm-startlon" ).val( json.body['lng']  );
			// console.log( $( "#simForm-location" ).val() );
*/			$( "#modal-site-body-flights-sim" ).html( '<table class="table table-striped table-bordered table-condensed"><tbody><tr><td><strong><a href="http://paraglidinglogbook.com/simulator/custom-takeoff.php?startlat='+json.body["lat"]+'&startlon='+json.body["lng"]+'" target="_blank"><img src="assets/img/icons/flight_sim.png" /> Fly the pglogbook.com online simulator ! <i class="fa fa-external-link"></i></a></strong></td></tr></tbody></table>' );


			if(history.pushState) {
				history.pushState({}, siteName, "?site="+siteId);
				document.title = json.body['name']+" on paragliding Earth";
			}

			if (teleport)  map.setView([json.body['lat'], json.body['lng']], 13);
			// syncSidebar();
			
			// update delete link (if exists) with site id for admins..
			$('#deleteSiteAdminLink').attr('href','assets/ajax/admin/siteDelete.php?site='+siteId);
			$('#closedSiteLink').attr('href','assets/ajax/admin/siteIsClosed.php?closed=1&site='+siteId+'&siteName='+json.body['name']);

		} else {   			/* *************** We don't have a site!!!: ( ********/
			
			$("#modal-site-title").html("Ooooops, there is no flying site to show you here...");
			$("#modal-site-body").html("sorry for the inconveniance...");
			map.setView([0, 0], 2);

		}
	});

	$("#siteDetailsModal").modal("show");
	openModal = 'siteDetailsModal';
	//textToLink('siteDetailsModal');
	$('#siteDetailsModal').on('hidden.bs.modal', function (e) {
		showSiteItems (id); // do something...
		
	});
}

var sitesOnMapAfterLoad=[];


function updateSites(){ 

	//console.log("UPDATE SITES !!! ");

	sitesOnMapBeforeLoad= [];      // empty the array of visible sites
	
	//  if zoom out and many sites present or in case of applying filters : restart from scratch to avoid grouped sites on the map
	// 
	if ( ( zoomEnd < zoomStart & sitesOnMapBeforeLoad.length > 32 )  ||  refreshSites   ) {
		/*  clean all !!! */
		markerClusters.removeLayer(sites);	// clean cluster
		sites.clearLayers();				// clean sites Layer
		refreshSites = false; 
	} else {    
		var iSite = 0; // counter for sites
		sites.eachLayer(function (layer) {	// for each marker that was on the map
			iSite++;
			var latlng = L.latLng(layer.feature.geometry.coordinates[1], layer.feature.geometry.coordinates[0]);
			if (map.getBounds().contains(latlng)){  			// marker inside map bounds
			   sitesOnMapBeforeLoad.push(layer.feature.id); 	// this one is still on the map
			} else {											// marker outside map bounds
				markerClusters.removeLayer(layer);				// slice Cluster
				sites.removeLayer(layer);      					// remove marker
			}
		});
	}


	//  get new sites via ajax	
	var sitesMarkers = L.featureGroup();

	getSites();

	function getSites() {
		return 	$.getJSON("assets/ajax/sitesJSON.php",{
						south: map.getBounds().getSouth(),
						north: map.getBounds().getNorth(),
						east: map.getBounds().getEast(),
						west: map.getBounds().getWest(),
						ignore: sitesOnMapBeforeLoad ,
						country: filterCountry,
						kinds : filterKinds,
						winds : filterWinds,
						flyable : flyableMode,
						async: false
					} ,  function (data) {	
					}
				)
				.done( function(data) {
					  
						sites.addData(data);  //process new data and fill sitesLayer
						markerClusters.addLayer(sites); // add site to cluster layer

						$(".siteLabel").css( "z-index" , -1000);

						/* ***** manage the display of the number of sites displayed on the map *****/
						$("#sitesCount").html(" Sites ("+data.displayed+"/"+data.total+")");
						if (data.displayed < data.total) {
							sitesZoomAdvice=true; 
							$("#sitesCount").addClass("redCount");
							$("#sitesCount").removeClass("greenCount");
						} else {
							sitesZoomAdvice=false;
							$("#sitesCount").removeClass("redCount");
							$("#sitesCount").addClass("greenCount");
						}
						toggleFlyableIcons(showFlyable, timeOffset);
				});
	
	}
	
	zoomAdvice();
}



if (zoomOnItem === 'site') {
	siteModal(zoomOnItemId, true);
}


var siteItemIcon = [];
siteItemIcon['takeoff']  = L.AwesomeMarkers.icon({ icon: 'play',	markerColor: 'blue',	prefix: 'fa' }); 
siteItemIcon['landing']  = L.AwesomeMarkers.icon({ icon: 'pause', markerColor: 'green',	prefix: 'fa' }); 
siteItemIcon['parking']  = L.AwesomeMarkers.icon({ icon: 'car',	markerColor: 'orange',	prefix: 'fa' }); 


function showSiteItems (id, lat, lng) {
	weGotBounds = false;
    siteItemsLayer.clearLayers();
   // map.removeLayer(siteItemsLayer);

	$.post('assets/ajax/siteItems.php', {'id': id}, function(data){
		$.each(data.items, function(index, item) {
//			console.log(index+':'+item.name+','+item.lat+','+item.lng+','+item.type);
			if (index==0) {
				siteBounds = L.latLngBounds(L.latLng(item.lat, item.lng), L.latLng(item.lat, item.lng));
				weGotBounds = true;
				mainTakeoffLatLng = L.latLng(item.lat, item.lng);
			}
			else 	{
				siteBounds.extend([item.lat, item.lng]);
			}
			if (item.name) displayName = item.name;
			else displayName = item.type;
			siteItemsLayer.addLayer(L.marker([item.lat, item.lng], {'riseOnHover': true , 'icon': siteItemIcon[item.type] }).bindLabel(displayName,{noHide:true}));
		});

		if (weGotBounds) {
			siteItemsLayer.addTo(map);
			map.fitBounds( siteBounds, {'padding': [120, 120]} );
			if (map.getZoom() > 14) map.setZoom(14);
		} else {
			map.setView(mainTakeoffLatLng, 15);
		}
	});
	var siteIconOpacity = 0.1;
	$(".siteIcon").css( "opacity" , siteIconOpacity);
	$(".siteLabel").css( "opacity" , siteIconOpacity);
}


function deletePicture(picture) {
	$("#deletePicture").hide(200);
	$("#deletePicture").load( "assets/ajax/updateItem/deletePictureForm.php", { "picture": picture } , function(){
		$("#deletePicture").show(200);
	});
}


$('#siteDetailsModal').on('hide.bs.modal', function(){
	$('.nav-tabs a[href="#modal-site-body-description"]').tab('show');
});


/* used to refresh markers icons on the map :
 * - when toggling "flayable?" on/off
 * - or when dragging/zoomming map :
 * Called on toggle event and in updateSite() function
 * */
function toggleFlyableIcons( status , timeOffset ){
	//showFlyable = status;
	sites.eachLayer(function (layer) {
		var flyability=0;
		
		if (layer.feature.properties.closed != "1") { // for non closed sites
			
			if (status) { // if switching "normal -> flyable"
				
				$("#loadingWeather").css("display","block");
				
				// only deal with icons that don't have  right the timeOffset+'flyableIcon' as a className  
				if ( layer.options.icon.options.className.indexOf(timeOffset+'flyableIcon') == -1 ) {
					
					$.get("assets/ajax/flyable/flyability.php?site="+layer.feature.id+"&timeOffset="+timeOffset, function(data) {
						fly = data['flyability'];
						if (typeof data['flyability'] == 'undefined') fly = "-1";
						// icon image
						siteIcon = L.icon({
							iconUrl: 'assets/img/siteFly'+fly+'.png',
							className: timeOffset+'flyableIcon siteIcon '+layer.feature.id+'siteIcon '+layer.feature.properties.filters+'siteIcon' ,
							iconSize: [24, 28],
							iconAnchor: [12, 28],
							popupAnchor: [0, -25],
							labelAnchor: [4, -8]
						} );
						layer.setIcon(siteIcon);
						// marker label 
						if (fly == -1) // if no orientations
							fReason = "Site orientations missing to tell about flyability here..."
						else
							fReason = flyableReportHTML(data);
							//console.log(fReason);
						$("#"+layer.feature.id+"flyableReason").html(fReason);
					});
				}
			} else {  //  if switching "normal <- flyable"
				siteIcon = L.icon({
					iconUrl:"assets/img/site"+layer.feature.properties.filters+".png",
					iconSize: [24, 28],
					iconAnchor: [12, 28],
					popupAnchor: [0, -25],
					labelAnchor: [4, -8],
					className: 'siteIcon '+layer.feature.id+'siteIcon '+layer.feature.properties.filters+'siteIcon' 
					} );
				layer.setIcon(siteIcon);
				$("#"+layer.feature.id+"flyableReason").html("");
			}
		}
	});
}

function flyableReportHTML(data){
	
	localDate = new Date(data['localTime']).toLocaleString();
	html = localDate+"<br />";
	if (data['flyability'] == 0) {
		html += "Flyable : <i class='far fa-thumbs-down unflyable'></i> no...<ul style='list-style: none'>";
		if ( data['reason'].indexOf('windSpeed') >= 0) html += "<li class='unflyable'><i class='fas fa-wind'></i> strong wind</li>";
		if ( data['reason'].indexOf('windDir') >= 0) html += "<li class='unflyable'><i class='far fa-compass'></i> wrong wind direction</li>";
		if ( data['reason'].indexOf('orientationsUnknown') >= 0) html += "<li class='unflyable'><i class='far fa-compass'></i> site orientation unknown.. :(</li>";
		if ( data['reason'].indexOf('rain') >= 0) html += "<li class='unflyable'><i class='fas fa-cloud-rain'></i> rain</li>";
		if ( data['reason'].indexOf('cloudbase') >= 0) html += "<li class='unflyable'><i class='fas fa-smog'></i> low cloudbase</li>";
		html += "</ul>";
	} else if (data['flyability'] == -50 ) {
		html += "Flyable : <i class='far fa-question-circle maybeflyable'></i> maybe...<br /><ul style='list-style: none'>";
		if ( data['reason'].indexOf('orientationsUnknown') >= 0) html += "<li class='maybeflyable'><i class='far fa-compass'></i> site orientation unknown.. :(</li>";
		html += "</ul>";
	}
	else html += "Flyable : <i class='far fa-thumbs-up flyable'></i> yes!<br />";
	
	return html;
}
/*
function setRelevantMarkerIcon(feature, id, closed, filters) {
	if (closed == "1")	icon = "assets/img/site-closed.png";
	else if (showFlyable) {
		$.get("assets/ajax/flyable/flyability.php?site="+id+"&date=0", function(data) {
			fly = data['flyability'];
			if (typeof data['flyability'] == 'undefined') fly = "-1";
			icon = 'assets/img/siteFly'+fly+'.png';
		});
	}
	else icon = "assets/img/site"+filters+".png";
	
	siteIcon = L.icon({ iconUrl: icon });
	
	return icon;
}
*/
