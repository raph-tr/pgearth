/* ************ PROS    ************************************ */
/* Empty layer placeholder to add to layer control for listening when to add/remove pros to markerClusters layer */

var  pros = L.geoJson(null, {
  pointToLayer: function (feature, latlng) {
    return L.marker(latlng, {
      icon: L.icon({
        iconUrl: "assets/img/pro.png",
        iconSize: [24, 28],
        iconAnchor: [12, 28],
        popupAnchor: [0, -25],
        labelAnchor: [4, -8],
        className: 'proIcon'
      }),
      title: feature.properties.NAME,
      riseOnHover: true
       }).bindLabel(feature.properties.NAME,{
		  noHide : true,
		  direction:'right',
		  className : 'proLabel'
	});
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
//      var content = "<table class='table table-striped table-bordered table-condensed'>" + "<tr><th>Name</th><td>" + feature.properties.NAME + "</td></tr>" + "<tr><th>Phone</th><td>" + feature.properties.tel + "</td></tr>" + "<tr><th>Address</th><td>" + feature.properties.adress + "</td></tr>" + "<tr><th>Website</th><td><a class='url-break' href='" + feature.properties.url + "' target='_blank'>" + feature.properties.url + "</a></td></tr>" + "<table>";
      layer.on({
        click: function (e) {
			proModal(feature.id);
        }
      });
      prosOnMap.push(feature.id);
      if (showProsLayer) $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/pro.png"></td><td class="feature-name">' + layer.feature.properties.NAME + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      proSearch.push({
        name: layer.feature.properties.NAME,
        source: "pros",
        id: L.stamp(layer),
        lat: layer.feature.geometry.coordinates[1],
        lng: layer.feature.geometry.coordinates[0]
      });
    }
  }
});

function proModal(id){
	$.getJSON("assets/ajax/proModalJSON.php",{id: id},  function (json) {
		
		// name
		proName = json.flag+"&nbsp;";
		if  (json.member_edit==1){
			proName += "<span onclick='clickEditIcon(\"name\", "+json.body['id']+")' id='name_"+json.body['id']+"' data-name='name' class='editable' data-type='text' data-pk='"+json.body['id']+"' data-url='assets/ajax/updateItem/proTextField.php' data-title='Enter name'>"+ json.body['name'] +"</span>";
			editNotice = "<p class='small'> <i class='fa fa-user'></i> <i class='fa fa-pencil-square-o'></i> <em>edit any field by clicking/tapping it twice.</em></p>";
			editNotice += "<p class='small'><a href='#' onClick='dragProMarker("+json.body['id']+", \""+addslashes(json.body['name'])+"\", "+json.body['lat']+", "+json.body['lng']+")'><i class='fa fa-arrows-alt'></i> Drag pro marker to edit its place on the map.</a>";
			editNotice += '<br /><a href="#" modalToOpen="reportItemModal" class="openAnotherModal" itemType="pro" itemName="'+json.body['name']+'" itemId="'+json.body['id']+'"><i class="fa fa-trash"></i> Delete this pro ?</a>';			
			editNotice += "</p>"; 
		} else {
			proName += json.body["name"] +".";
			editNotice = '<p class="small"><a href="#" modalToOpen="reportItemModal" class="openAnotherModal" itemType="pro" itemName="'+json.body['name']+'" itemId="'+json.body['id']+'"><i class="fa fa-trash"></i> Delete this pro ?</a>';
			editNotice +=  '<br /><em> <a href="#" class="openAnotherModal" modalToOpen="loginModal" ><i class="fa fa-user"></i> Login to edit information</a></em>';
			editNotice += '</p>';
		}
		
		var bodyHTML = "<table id='proDescTable' class='table table-striped table-bordered table-condensed'><tbody>";	

		// activities
		var activities = "<ul id='activitiesDiv'>";
		var none = "<li>none</li>"; var some="";
		if(json.body['activity_tandem']==1) {
			none=""; some += "<li style='list-style-image:url(assets/img/icons/tandem.png);'> tandem flights</li>";
		}
		if(json.body['activity_school']==1) {
			none=""; some += "<li style='list-style-image:url(assets/img/icons/school.png);'>free flight school</li>";
		}
		if(json.body['activity_shop']==1) {
			none=""; some += "<li style='list-style-image:url(assets/img/icons/shop.png);'>gear shop</li>";
		}
		if(json.body['activity_repair']==1) {
			none=""; some += "<li style='list-style-image:url(assets/img/icons/repair.png);'>repair</li>";
		}
		activities += none+some+"</ul>";
		if  (json.member_edit == 1) {
			activities += "<div id='activitiesForm'>.</div>";
		}

		
		bodyHTML  += "<tr><th><strong>what we do</strong></th><td><small> "+ activities +"</small></td></tr>";

		
		// text infos
		var items= ['tel', 'fax', 'website', 'adress', 'city', 'comments'];
			
		items.forEach(function(entry) {
		    if (json.member_edit==1){
				entryContent = json.body[entry]; if (entryContent == "") entryContent = "Empty";
				data_type = "text"; if (entry=="comments") data_type = "textarea";
				bodyHTML += "<tr><th><strong>" + entry + "</strong></th>";
				bodyHTML += "<td><div onclick='clickEditIcon(\""+entry+"\", "+json.body['id']+")' id='" + entry + "_"+json.body['id']+"' data-name='" + entry + "' class='editable' data-type='"+data_type+"' data-pk='"+json.body['id']+"' data-url='assets/ajax/updateItem/proTextField.php' data-title='Enter "+entry+"'> "+ entryContent +"</div></td>";
				bodyHTML += "</tr>";
			} else {
				bodyHTML += "<tr><th><strong>"+entry+"</strong></th><td><small> "+ textToLink(json.body[entry]) +"</small></td></tr>";
			}
		});
		
		
		// sites used
		var sitesHTML = "<ul>";
		var none = "<li>none</li>"; var some="";
		for(var k in json.sites) {
			none=""; some += "<li style='list-style-image:url(assets/img/flag/_tn_"+json.sites[k].iso+".png);'><a href='#' class='openAnotherModal' modalToOpen='siteDetailsModal' id='"+json.sites[k].id+"'>"+json.sites[k].site+"</a></li>";
		}
				if  (json.member_edit==1){
			some += '<li style="list-style-image:url(assets/img/icons/famfamfam/add.png);"><a href="#" onclick="$(\'#addSiteToItemInfo\').show(\'slow\');addSiteToItem =\'pro-'+json.body["id"]+'\';console.log(addSiteToItem);" > Add a site...</a></li>';
			sitesHTML += none+some+"</ul>";
			sitesHTML += '<div id="addSiteToItemInfo">Your request has been heard !<br />Go now to the flying site window  you want to add (without refreshing the main page...),<br />you shall find a link under the site name inviting you to add the site to the pro\'s list</div>';
		} else {
				sitesHTML += none+some+"</ul>";
		}
		bodyHTML  += "<tr><th><strong>where we fly</strong></th><td><small> "+ sitesHTML +"</small></td></tr>";
		
		// end of body 	
		bodyHTML+="</tbody></table>";
		
		// set html
		$("#feature-title").html( editNotice+ proName );
		$("#feature-info").html( bodyHTML );
		$("#addSiteToItemInfo").hide();
		if  (json.member_edit == 1) {
			$("#activitiesForm").toggle();
				// ajaxLoading form for activities
			$("#activitiesDiv").on("click", function(){
				$("#activitiesForm").load("assets/ajax/updateItem/proActivitiesForm.php", { "id": id, "shop": json.body['activity_shop'], "tandem": json.body['activity_tandem'], "repair": json.body['activity_repair'], "school": json.body['activity_school']  },
					function() {
							$("#activitiesDiv").toggle("slow");
							$("#activitiesForm").toggle("slow");
					});
			});
		}

		// set view
		map.setView([json.body['lat'], json.body['lng']], 13 );
		// syncSidebar(); 

		if(history.pushState) {
			history.pushState({}, "", "?pro="+json.body["id"]);
			document.title = json.body["name"]+" on paragliding Earth";

		}
	});
	
	$("#featureModal").modal("show");
	openModal = 'featureModal';

}


function updatePros(){

	// keep pros on map if not zoom out or not too many pros
	if ( zoomEnd >= zoomStart || prosOnMap.length < 32) { 
		pros.eachLayer(function (layer) {
			var latlng = L.latLng(layer.feature.geometry.coordinates[1], layer.feature.geometry.coordinates[0]);
			if (map.getBounds().contains(latlng)){
			//	prosOnMap.push(layer.feature.id);  // we keep this one (will not be querried)
			} else {
				markerClusters.removeLayer(layer);
				pros.removeLayer(layer);
				//numberOfSites --; 
				prosOnMap.splice( $.inArray(layer.feature.id, prosOnMap), 1 );
			}
		});
	} else {     //  Zoom out : clean all and pick new collection
 		markerClusters.removeLayer(pros);
		pros.clearLayers();
		prosOnMap.length = 0;
	}

	$.getJSON("assets/ajax/prosJSON.php",{
			south: map.getBounds().getSouth(), 
			north: map.getBounds().getNorth(), 
			east: map.getBounds().getEast(), 
			west: map.getBounds().getWest(),
			country: filterCountry, 
			ignore: prosOnMap },  function (data) {
		pros.addData(data); 
		if (showProsLayer){
			markerClusters.addLayer(pros);
		}
		$("#prosCount").html(" Pros ("+data.displayed+"/"+data.total+")");
		if (data.displayed < data.total) {
			prosZoomAdvice = true;
			$("#prosCount").addClass("redCount");
			$("#prosCount").removeClass("greenCount");
		} else {
			prosZoomAdvice = false;
			$("#prosCount").addClass("greenCount");
			$("#prosCount").removeClass("redCount");
		}

    $(".proIcon").css( "opacity" , siteIconOpacity);
    $(".proLabel").css( "opacity" , siteIconOpacity);
		
	});
	zoomAdvice();
//	console.log(prosOnMap);

}

// updatePros();

// if (showProsLayer) map.addLayer(proLayer);

if (zoomOnItem === 'pro') proModal(zoomOnItemId);




function dragProMarker(id, name, lat, lng) {
		$('#featureModal').modal("hide");
		
		draggableMarker = L.marker(
										[lat, lng],
										{	
											draggable: true
										}
								   ).addTo(map).bindPopup("<strong>"+name+"</strong><br />Drag me to correct club location...").openPopup();
								  
		draggableMarker.on("dragend",function(e){
			var chagedPos = e.target.getLatLng();
			this.bindPopup("<strong>"+name+"</strong><br /><span id='draggableMarkerPopupContent'><a href='#' onclick='saveProPosition( "+e.target.getLatLng().lng+","+e.target.getLatLng().lat+", "+id+")'>save this location</a></span>").openPopup();
        });

}


function saveProPosition( lng, lat ,id ) {
		console.log(lat+', '+lng+' - '+id);
		$.post('assets/ajax/updateItem/proPositionSave.php', {'id': id, 'lat': lat, 'lng':lng}, function(data){
			console.log(data);
			$('#draggableMarkerPopupContent').html('saved, thx !');
			location.reload();
		});
} 

