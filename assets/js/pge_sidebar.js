$(document).on("click", ".feature-row", function(e) {
  $(document).off("mouseout", ".feature-row", clearHighlight);
  sidebarClick(parseInt($(this).attr("id"), 10));
});

if ( !("ontouchstart" in window) ) {
  $(document).on("mouseover", ".feature-row", function(e) {
    highlight.clearLayers().addLayer(L.circleMarker([$(this).attr("lat"), $(this).attr("lng")], highlightStyle));
  });
}

$(document).on("mouseout", ".feature-row", clearHighlight);

function animateSidebar() {
  $("#sidebar").animate({
    width: "toggle"
  }, 350, function() {
    map.invalidateSize();
  });
}

function sizeLayerControl() {
  $(".leaflet-control-layers").css("max-height", $("#map").height() - 50);
}

function clearHighlight() {
  highlight.clearLayers();
}

function sidebarClick(id) {
  var layer = markerClusters.getLayer(id);
  map.setView([layer.getLatLng().lat, layer.getLatLng().lng], 13);
  layer.fire("click");
  /* Hide sidebar and go to the map on small screens */
  if (document.body.clientWidth <= 767) {
    $("#sidebar").hide();
    map.invalidateSize();
  }
}

// sidebar hidden at start :(
$("#sidebar").css('left', '-262px');
$("#sidebar-hide-btn").html( '<i class="fa fa-chevron-right">');


function toggleSidebar () {
  if ($("#sidebar").css('left') == '-262px') {
    $("#sidebar").animate({ left: "0"});
    $("#sidebar-hide-btn").html( '<i class="fa fa-chevron-left">');
  } else {
    $("#sidebar").animate({ left: "-262px"});
    $("#sidebar-hide-btn").html( '<i class="fa fa-chevron-right">');
  }
}

$("#sidebar-hide-btn").click(toggleSidebar);
$("#list-btn").click(toggleSidebar);
