/* Typeahead search functionality */
$(document).one("ajaxStop", function () {
  $("#loading").hide();
  sizeLayerControl();

  featureList = new List("features", {valueNames: ["feature-name"]});
  featureList.sort("feature-name", {order:"asc"});

  var boroughsBH = new Bloodhound({
    name: "Boroughs",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: boroughSearch,
    limit: 10
  });

  var sitesBH = new Bloodhound({
    name: "sites",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: siteSearch,
    limit: 10
  });

  var prosBH = new Bloodhound({
    name: "pros",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: proSearch,
    limit: 10
  });

  var sitesDBBH = new Bloodhound({
    name: "sitesDB",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: "assets/ajax/searchSitesJSON.php?name=%QUERY",
      filter: function (data) {
        return $.map(data.features, function (result) {
          return {
            name: result.name + ", " + result.countryCode,
            flag: "<img src='assets/img/flags/_tn_"+ result.countryCode + ".png' />",
            lat: result.lat,
            lng: result.lng,
            source: "sitesDB"
          };
        });
      },
      ajax: {
        beforeSend: function (jqXhr, settings) {
          // settings.url += "&east=" + map.getBounds().getEast() + "&west=" + map.getBounds().getWest() + "&north=" + map.getBounds().getNorth() + "&south=" + map.getBounds().getSouth();
          $("#searchicon").removeClass("fa-search").addClass("fa-refresh fa-spin");
        },
        complete: function (jqXHR, status) {
          $('#searchicon').removeClass("fa-refresh fa-spin").addClass("fa-search");
        }
      }
    },
    limit: 5
  });
  
  var geonamesBH = new Bloodhound({
    name: "GeoNames",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: "https://secure.geonames.org/searchJSON?username=raf&featureClass=P&maxRows=5&name_startsWith=%QUERY",
      filter: function (data) {
        return $.map(data.geonames, function (result) {
          return {
            name: result.name + ", " + result.countryCode + ", " + result.adminName1,
            lat: result.lat,
            lng: result.lng,
            source: "GeoNames"
          };
        });
      },
      ajax: {
        beforeSend: function (jqXhr, settings) {
         // settings.url += "&east=" + map.getBounds().getEast() + "&west=" + map.getBounds().getWest() + "&north=" + map.getBounds().getNorth() + "&south=" + map.getBounds().getSouth();
          $("#searchicon").removeClass("fa-search").addClass("fa-refresh fa-spin");
        },
        complete: function (jqXHR, status) {
          $('#searchicon').removeClass("fa-refresh fa-spin").addClass("fa-search");
        }
      }
    },
    limit: 10
  });


  boroughsBH.initialize();
  prosBH.initialize();
  geonamesBH.initialize();
  sitesDBBH.initialize();

  /* instantiate the typeahead UI */
  $("#searchbox").typeahead({
    minLength: 3,
    highlight: true,
    hint: false
  }, {
    name: "Boroughs",
    displayKey: "name",
    source: boroughsBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'>Boroughs</h4>"
    }
  }, {
    name: "sites",
    displayKey: "name",
    source: sitesBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/site.png' width='24' height='28'>&nbsp;sites</h4>",
      suggestion: Handlebars.compile(["{{name}}<br>&nbsp;<small>{{address}}</small>"].join(""))
    }
 }, {
    name: "pros",
    displayKey: "name",
    source: prosBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/pro.png' width='24' height='28'>&nbsp;Pros</h4>",
      suggestion: Handlebars.compile(["<small>{{name}}</small>"].join(""))
    }
  },  {
    name: "SitesDB",
    displayKey: "name",
    source: sitesDBBH.ttAdapter(),
    templates: {
      header: "<h4 class'typeahead-header'><img src='assets/img/site.png' width='25' height='25'>&nbsp;Sites</h4>",
      suggestion: Handlebars.compile(["<small>{{name}}</small>"].join(""))
    }
  },  {
    name: "GeoNames",
    displayKey: "name",
    source: geonamesBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/globe.png' width='25' height='25'>&nbsp;Place name</h4>",
      suggestion: Handlebars.compile(["<small>{{name}}</small>"].join(""))
    }
  }
  ).on("typeahead:selected", function (obj, datum) {
    if (datum.source === "Boroughs") {
      map.fitBounds(datum.bounds);
    }
    if (datum.source === "sites") {
      if (!map.hasLayer(siteLayer)) {
        map.addLayer(siteLayer);
      }
      map.setView([datum.lat, datum.lng], 12);
      if (map._layers[datum.id]) {
        map._layers[datum.id].fire("click");
      }
    }
    if (datum.source === "pros") {
      if (!map.hasLayer(proLayer)) {
        map.addLayer(proLayer);
      }
      map.setView([datum.lat, datum.lng], 12);
      if (map._layers[datum.id]) {
        map._layers[datum.id].fire("click");
      }
    }
    if (datum.source === "GeoNames") {
      map.setView([datum.lat, datum.lng], 11);
    }
    if (datum.source === "sitesDB") {
      if (!map.hasLayer(siteLayer)) {
        map.addLayer(siteLayer);
      }
      map.setView([datum.lat, datum.lng], 12);
      if (map._layers[datum.id]) {
        map._layers[datum.id].fire("click");
      }
     }
    if ($(".navbar-collapse").height() > 50) {
      $(".navbar-collapse").collapse("hide");
    }
  }).on("typeahead:opened", function () {
    $(".navbar-collapse.in").css("max-height", $(document).height() - $(".navbar-header").height());
    $(".navbar-collapse.in").css("height", $(document).height() - $(".navbar-header").height());
  }).on("typeahead:closed", function () {
    $(".navbar-collapse.in").css("max-height", "");
    $(".navbar-collapse.in").css("height", "");
  });
  $(".twitter-typeahead").css("position", "static");
  $(".twitter-typeahead").css("display", "block");
});
