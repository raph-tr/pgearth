//  on modal close, we modify the url in the adress bar
$('.modal').on('hidden.bs.modal', function () {
	if(history.pushState) {
		history.pushState({}, "", "/pgearth/"); // put in 3rd argument the website root url (if website not at root at the server)
		document.title = "paragliding Earth";
	}
});
