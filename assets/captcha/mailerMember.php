<?php
//session_start(); // this MUST be called prior to any output including whitespaces and line breaks!

$GLOBALS['ct_recipient']   = 'raphael@disroot.org'; // Change to your email address!
$GLOBALS['ct_msg_subject'] = 'Someone contacted you from Paragliding Earth Contact Form';

$GLOBALS['DEBUG_MODE'] = 0;
// CHANGE TO 0 TO TURN OFF DEBUG MODE
// IN DEBUG MODE, ONLY THE CAPTCHA CODE IS VALIDATED, AND NO EMAIL IS SENT

include '../../config/connection.php';
$queryMember  = "SELECT email FROM `as_users` WHERE `username` LIKE '".$_POST['dest']."'";
if (! $rMember = mysqli_query($bdd, $queryMember)) die('error on query');
$vMember = mysqli_fetch_array($rMember);


$GLOBALS['ct_recipient']   =  $vMember['email'];



// Process the form, if it was submitted
process_si_contact_form();


// The form processor PHP code
function process_si_contact_form()

{
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && @$_POST['do'] == 'contact') {
        // if the form has been submitted

        foreach($_POST as $key => $value) {
            if (!is_array($key)) {
                // sanitize the input data
                if ($key != 'ct_message') $value = strip_tags($value);
                $_POST[$key] = htmlspecialchars(stripslashes(trim($value)));
            }
        }

        $name    = @$_POST['ct_name'];    // name from the form
        $email   = @$_POST['ct_email'];   // email from the form
        $URL     = @$_POST['ct_URL'];     // url from the form
        $message = @$_POST['ct_message']; // the message from the form
        $captcha = @$_POST['ct_captcha']; // the user's entry for the captcha code
        $name    = substr($name, 0, 64);  // limit name to 64 characters

        $errors = array();  // initialize empty error array

        if (isset($GLOBALS['DEBUG_MODE']) && $GLOBALS['DEBUG_MODE'] == false) {
            // only check for errors if the form is not in debug mode

            if (strlen($name) < 3) {
                // name too short, add error
                $errors['name_error'] = 'Your name is required';
            }

            if (strlen($email) == 0) {
                // no email address given
                $errors['email_error'] = 'Email address is required';
            } else if ( !preg_match('/^(?:[\w\d-]+\.?)+@(?:(?:[\w\d]\-?)+\.)+\w{2,4}$/i', $email)) {
                // invalid email format
                $errors['email_error'] = 'Email address entered is invalid';
            }

            if (strlen($message) < 20) {
                // message length too short
                $errors['message_error'] = 'Please enter a message';
            }
        }

        // Only try to validate the captcha if the form has no errors
        // This is especially important for ajax calls
        if (sizeof($errors) == 0) {
            require_once dirname(__FILE__) . '/securimage.php';
            $securimage = new Securimage();

            if ($securimage->check($captcha) == false) {
                $errors['captcha_error'] = 'Incorrect security code entered';
            }
        }

        if (sizeof($errors) == 0) {
            // no errors, send the form
            $time       = date('r');
            $message = "A message was sent to you from the paraglidingEarth message form.  The following information was provided.<br /><br />"
                     . "Name: $name<br />"
                     . "Email: $email<br />"
                     . "Message:<br />"
                     . "<pre>$message</pre>"
                     . "Time: $time<br />"
                     . " <br />"
                     . "<em><small>You received this message because <a href='https://paragliding.earth/member/profile.php'>you agreed that pgEarth visitors could contact you</a>. Click <a href='https://paragliding.earth/member/profile.php'>here</a> (or visit this link : ' https://paragliding.earth/member/profile.php ') if you wish to change this.<br />"
                     . "The sender of this message does not know your email adress, but he will if you reply this message to him or her.</small></em>";
 
 
 
            if (isset($GLOBALS['DEBUG_MODE']) && $GLOBALS['DEBUG_MODE'] == false) {
                // send the message with mail()
              if (   mail($GLOBALS['ct_recipient'], $GLOBALS['ct_msg_subject'], $message, "From: {$GLOBALS['ct_recipient']}\r\nReply-To: {$email}\r\nContent-type: text/html; charset=ISO-8859-1\r\nMIME-Version: 1.0")) {
					$return = array('error' => 0, 'message' => 'good', 'dest' => $_POST['dest'], 'destMail' => $GLOBALS['ct_recipient'] );
					die(json_encode($return));
			  } else {
			 	  $return = array('error' => 1, 'message' => 'error sending mail');
				  die(json_encode($return));
			  }
                
            }

            $return = array('error' => 0, 'message' => 'OK', 'dest' => $_POST['dest']);
            die(json_encode($return));
        } else {
            $errmsg = '';
            foreach($errors as $key => $error) {
                // set up error messages to display with each field
                $errmsg .= " - {$error}\n";
            }

            $return = array('error' => 1, 'message' => $errmsg);
            die(json_encode($return));
        }
    } // POST
} // function process_si_contact_form()
?>
