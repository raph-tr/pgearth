	<script type="text/javascript">

		//=================== Show windy overlay ==========
		var showWindyOverlay = false;
		<?php
		if (isset($_GET['windy'])) {
			echo "$('.windyContainer').css('visibility','visible');
			showWindyOverlay = true;";
		} else {
			echo "$('.windyContainer').css('visiblity','hidden');";
		}
		?>
		var isWindyMapClickable = false;

		//=================== Get user timezone ==========

		var userTimezone = new Date().getTimezoneOffset() / 60.0;

		//==================== Refresh sites on updateSites() call is false by default

		var refreshSites = false;

		//=================== Show flyable Icons==========
		var showFlyable= false;
		var timeOffset=0;
		
		//=================== Zoom on items ==============
		var zoomOnItem = "none";
		var zoomOnItemId = 0;
<?php
		if (isset($_GET['club'])) {
			echo "zoomOnItem = 'club';
			zoomOnItemId = ".$_GET['club'].";";
		}
		if (isset($_GET['pro'])) {
			echo "zoomOnItem = 'pro';
			zoomOnItemId = ".$_GET['pro'].";";
		}
		if (isset($_GET['site'])) {
			echo "zoomOnItem = 'site';
			zoomOnItemId = ".$_GET['site'].";";
		}
		if (isset($_GET['bounds'])) {
			echo "zoomOnItem = 'bounds';";
			$coords = explode(",", $_GET['bounds']);
			echo "
			NWlat = ".$coords[0].";
			NWlng = ".$coords[1].";
			SElat = ".$coords[2].";
			SElng = ".$coords[3].";
			";
			if (isset($_GET['showClubs']))echo " showClubs = true;";
			else echo " showClubs = false;";
			if (isset($_GET['showPros'])) echo " showPros = true;";
			else  echo " showPros = false;"; 
		}
?>
		
		//=================== Filters ==============
		var hasFilters       = false;
		var hasCountryFilter = false;
		var hasKindFilter    = false;
		var hasWindFilter    = false;
		var zoomOnFilteredItems = false;
		var filterCountry    = "all";
		var filterKinds      = [];
		var filterWinds      = [];
		var filterHTMLString = "<ul style='padding-left: 14px;margin-bottom: 0px;'>";
		
<?php
		if (isset($_GET['country']) and $_GET['country']<>'all') {
		echo 'hasFilters= true;
		hasCountryFilter = true;
		filterCountry = "'.$_GET['country'].'";
		filterHTMLString += "<li>country : <img src=\"assets/img/flag/'.$_GET['country'].'.png\" /> "+ filterCountry +"</li>" ;
		
		if(history.pushState) {
			history.pushState({}, "", "?country="+filterCountry );
			document.title = getCountryName ( filterCountry.toUpperCase() ) + " sites  on paragliding Earth";
		}

';

/*		if(history.pushState) {
			history.pushState({}, "", "?country="+filterCountry );
			document.title = filterCountry + "sites  on paragliding Earth";
		}
*/
		}

		$kindsOfFlights = array('hike', 'soaring', 'thermal', 'xc', 'flatland', 'winch', 'hangglider');
		foreach ($kindsOfFlights as $kind){
			if ($_GET[$kind] == 1 ){
				echo 'hasFilters = true;
				hasKindFilter = true;
				filterHTMLString += "<li>flying : <img src=\'assets/img/flying/'.$kind.'25.png\'> '.$kind.'</li>";
				filterKinds.push("'.$kind.'");
				';
			}
		}
		
		$winds = array('n', 'ne', 'e', 'se', 's', 'sw', 'w', 'nw');
		foreach ($winds as $wind){
			if ($_GET[$wind] == 1 ){
				echo 'hasFilters = true;
				hasWindFilter = true;
				filterHTMLString += "<li>wind : '.$wind.'</li>";
				filterWinds.push("'.$wind.'");
				';
			}
		}
		
?>
		filterHTMLString += "</ul>";
//console.log(filterKinds);


		//=================== Member modal ===============
		var showMember = false;
<?php
		if (isset($_GET['member'])) {
			echo "
			showMember = true;
			userName = '".$_GET['member']."';
";
		}
		?>	
		
		//=================== center map to latlng  ===============
		var centerMap = false;
<?php
		if (isset($_GET['latlng'])) {
			echo "
		centerMap = true;
		centerLatLng = [".$_GET['latlng']."];
			";
		}
		?>	
		

		//=================== FLYABLE mode ?  ===============
		var flyableMode = false;
<?php
		if (isset($_GET['flyable']) and $_GET['flyable'] == 1 ) {
			echo "
		flyableMode = true;
			";
		}
		?>	
		



		//=================== Any static modal, with the 'view' param 
<?php
		if (isset($_GET['view'])) {
			if ( $_GET['view'] == 'login'
			  or $_GET['view'] == 'about'
			  or $_GET['view'] == '404'
			  or $_GET['view'] == 'api'
			  or $_GET['view'] == 'news' ) {
?>
		$(document).ready( function() {
			openAModal('<?php echo $_GET['view']; ?>');
			<?php if (isset($_GET['tab'])) {  ?>
			 $('.nav-tabs a[href="#<?php echo $_GET['tab'];?>"]').tab('show');
		//	$("#<?php echo $_GET['tab'];?>").tab('show');
			<?php } ?>		
		});
<?php
			}
		}
		?>	
			
		//======== Logged in member
		var currentUser = 0;

<?php
		if ($isMember) {
			echo "		currentUser = ".$currentUser->id." ;
		var currentUserName = '".$currentUser->username."' ;";
		}
		?>
				
	</script>
