<div class="modal fade" id="contactModal" tabindex="-1" role="dialog">

	<div class="modal-dialog modal-lg">

		<div class="modal-content">

			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>

				<h3 class="modal-title">Get in touch !</h3>
			</div>  <!-- /.modal header -->

			<div class="modal-body">

				<ul class="nav nav-tabs" id="contactTabs">

					<li class="active">	<a href="#contact" data-toggle="tab"><i class="fa fa-pencil"></i>&nbsp;Contact</a></li>
					<li>				<a href="#help"    data-toggle="tab"><i class="fa fa-ambulance"></i>&nbsp;You can help us!!!</a></li>
					<li>				<a href="#report"  data-toggle="tab"><i class="fa fa-bullhorn "></i>&nbsp;Suggestions / bug report</a></li>

				</ul>

				<div class="tab-content" id="contactTabsContent">

					<div class="tab-pane fade  active in" id="contact">
					<!--
					  !!!!!!!!!!!!!
						the form content is loaded
						from a function in assets/js/pge_utils_functions.js
					  !!!!!!!!!!!!!

						<p><i class="fa fa-envelope"></i>&nbsp;<em>raphael /at/ disroot /dot/ org</em></p>
					-->

					<i class="fa fa-circle-notch fa-spin"></i>&nbsp;Loading contact form
					</div><!-- /#contact -->



					<div class="tab-pane fade" id="help">
						<div class="panel panel-primary">
							<div class="panel-heading">You like PgEarth ? YOU CAN HELP US !</div>
								<ul class="list-group">
									<li class="list-group-item"><i class="fa fa-pencil-square-o"></i> <strong>by editing the information</strong>: log in and help keeping the information up to date. The website is efficient only if people like you keep the information correct.</li>
									<li class="list-group-item"><i class="fa fa-bullhorn"></i> <strong>by reporting errors</strong>: no need to be logged in : report to the moderators the information that you think should be deleted (trash icons).</li>
									<li class="list-group-item"><i class="fa fa-share-alt"></i> <strong>by bringing your friends</strong>: the more the merrier. Advertise around you ! (We are not big fans of social networks, but if you are, go ahead and give us some buzz! :)</li>
									<li class="list-group-item"><i class="fa fa-commenting-o"></i> <strong>by translating/improving the english</strong>: information on pgearth is meant to be readen by as many people as possible, that is why we chose to publish in english : translate the data fields if you can or contact us to improve the english of the website itself.</li>
									<li class="list-group-item"><i class="fa fa-balance-scale"></i> <strong>by beeing a moderator</strong>: help us keep the data clean by deleting incorrect information.</li>
									<li class="list-group-item"><i class="fa fa-laptop"></i> <strong>by joining the code git</strong>: the code git <a href="https://framagit.org/raph-tr/paraglidingearth" target="_blank">is here</a>.</li>
									<li class="list-group-item"><i class="fa fa-coffee"></i> <strong>by offering a coffee</strong>: to be frank we already have our own coffee, but that would help with hosting, domain names, bought scripts, etc. <br />
									Big thanks to our tipers: J.S.Grigsby, M.Zijderveld, A.Keyes :)<br />
									<a href="https://paypal.me/pgearth"><em><i class="fa fa-paypal"></i> pgearth on paypal</em></a><br />
									<a href="#" onClick="toggleFinances()">Have a look at our 2017 finances for example...</a>
										<div id="finances" class="alert-info" style="display:none">
											<h4>2017 pgearth finances</h4>
											<div class="meter orange">
												<span style="width: 42%"></span>
											</div>

											<table class="table table-striped table-bordered table-hover table-condensed" >
												<thead>
													<tr>
														<th>Expenses</th>
														<th>&euro;</th>
														<th>Income</th>
														<th>&euro;</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>hosting</td>
														<td>43</td>
														<td>hosting offered by <a href="?club=1">guc parapente</a></td>
														<td>43</td>
													</tr>
													<tr>
														<td>domain name pgearth.com</td>
														<td>10</td>
														<td>ads on the site</td>
														<td>0</td>
													</tr>
													<tr>
														<td>domain name pg.earth</td>
														<td>31</td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td>membership script bought on code canyon</td>
														<td>13</td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<th>total</th>
														<th>97</th>
														<th></th>
														<th>43</th>
													</tr>
												</tbody>
											</table>
											<p>Funded : 42%, missing 51 &euro;, updated : sept. 2017<br /><a href="https://paypal.me/pgearth"><i class="fa fa-paypal"></i> pgearth on paypal</a>.</p>
										</div>
									</li>
								</ul>
							</div>
						</div><!-- /#help -->



						<div class="tab-pane fade" id="report">

							<p>You want to see a new feature ? <br />
							You found a bug ?
							</p>

							<p>Please <a href="https://framagit.org/raph-tr/paraglidingearth/issues" target="_blank">let us know on our git </a>. (registration required)
							</p>
						</div><!-- /#report -->


					</div>

				</div><!-- /#contact -TabsContent  -->

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>

			</div> <!-- /.modal body -->


		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
