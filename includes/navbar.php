    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">

          <div class="navbar-icon-container">
            <a href="#" class="navbar-icon pull-right visible-xs" id="nav-btn"><i class="fa fa-bars fa-lg white"></i></a>
	      </div>
          <a class="navbar-brand" href="http://paragliding.earth"><i class="fa fa-cloud"></i>Paragliding.</i>Earth</a>
        </div>
        
        <div class="navbar-collapse collapse">
          <form class="navbar-form navbar-right" role="search">
            <div class="form-group has-feedback">
                <input id="searchbox" type="text" placeholder="Search" class="form-control">
                <span id="searchicon" class="fa fa-search form-control-feedback"></span>
            </div>
          </form>
          
          <ul class="nav navbar-nav">

			<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="about-btn"><i class="fa fa-question-circle white"></i>&nbsp;&nbsp;About</a></li>

            <?php if( ! $isMember ) { ?>
            <li><a href="#" id="login-btn"><i class="fa fa-user"></i> Login</a></li>
 	<?php } else { ?>
            <li class="dropdown"> 
				<a href="#"  id="memberDrop" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-user"></i><?php echo "&nbsp;&nbsp;".$userName; ?> <i class="fa fa-caret-down"></i></a>
				<ul class="dropdown-menu">
					<li>
						<a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="profile-btn">
							<i class="fa fa-user"></i>&nbsp;&nbsp;My profile
						</a>
					</li>
					<li>
						<a href="#" member="<?php echo $userName; ?>" data-toggle="collapse" data-target=".navbar-collapse.in" id="member-btn">
						  <i class="fa fa-calendar-check-o"></i>&nbsp;&nbsp;My activity
						</a>
					</li>
					<li>
						<a href="/member/logout.php" data-toggle="collapse" data-target=".navbar-collapse.in" id="logout-btn">
							<i class="fa fa-sign-out"></i>&nbsp;&nbsp;Logout
						</a>
					</li>
				</ul>
			</li>
	<?php } ?>


            <li class="dropdown">
              <a id="toolsDrop" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench white"></i>&nbsp;&nbsp;Tools <i class="fa fa-caret-down"></i></a>
              <ul class="dropdown-menu">
                <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="filters-btn"> <i class="fa fa-filter"></i>&nbsp;&nbsp;Filter sites</a></li>
                <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="news-btn">	 <i class="fa fa-newspaper"></i>&nbsp;&nbsp;This week news</a></li>
                <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="link-btn" onclick="sharePage();"><i class="fa fa-link"></i> Share page link</a></li>
                <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="teleport-btn"><i class="fa fa-rocket"></i>&nbsp;&nbsp;Teleport to GPS coords</a></li>
                <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="export-btn"><i class="fa fa-share-square-o"></i>&nbsp;&nbsp;Export</a></li>
                <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="api-btn">	 <i class="fa fa-exchange"></i>&nbsp;&nbsp;API</a></li>
              </ul>
            </li>
            
            <li class="hidden-xs"><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="list-btn"><i class="fa fa-list white"></i>&nbsp;&nbsp;Sidebar</a></li>


			<!-- ***********    Add new menu item ************  --> 
            <li class="dropdown">
              <a id="addDrop" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add new.. <i class="fa fa-caret-down"></i></a>
              <ul class="dropdown-menu">
			<?php if ($isMember) { ?>
                <li><a href="#" id="newSite"  onclick="addNew('site');"><i class="fa fa-map-marker-alt "></i>&nbsp;&nbsp;.. flying site</a></li>
                <li><a href="#" id="newClub" onclick="addNew('club');"><i class="fa fa-users "></i>&nbsp;&nbsp;...club</a></li>
                <li><a href="#" id="newPro" onclick="addNew('pro');"><i class="fa fa-user-check "></i>&nbsp;&nbsp;...pro</a></li>
            <?php } else { ?>
               <li><a href="#" id="login-btn2" ><i class="fa fa-user"></i> Please login to edit information</a></li>
			<?php }?>
              </ul>
            </li>  

			<!-- ***********    Contact menu item ************  --> 
            <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="contact-btn"><i class="fa fa-envelope white"></i>&nbsp;&nbsp;Contact / Help us</a></li>  






<?php		if ( $currentUser->role_id > 1 /*is admin or moderator*/) {    ?>
<!--*************Admin menu *************-->
			<li class="dropdown">
				<a href="#" id="admin-btn" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-edit"></i> Admin</a>
				<ul class="dropdown-menu">
					<li><a href="#" id="reported-btn"><i class="fa fa-minus-circle"></i>&nbsp;&nbsp;Reported items</a></li>
				</ul>
			</li>
<?php		}     ?>           
         
         
          </ul>
        </div><!--/.navbar-collapse -->
      </div>
    </div>
