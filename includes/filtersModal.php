<form method="GET" action="" name="filtersForm">

	<div class="control-group form-group">

		<div class="row form-group">

			<div class=" controls col-lg-12">

				<label class="control-label" for="country">
					Country
				</label>

				<select name="country" id="countrySelect" class="select-large form-control">
					<option value="all" label="Select a country ... " selected="selected">Select a country ... </option>
					<option value="af" label="Afghanistan">Afghanistan</option>
					<option value="al" label="Albania">Albania</option>
					<option value="dz" label="Algeria">Algeria</option>
					<option value="as" label="American Samoa">American Samoa</option>
					<option value="ad" label="Andorra">Andorra</option>
					<option value="ao" label="Angola">Angola</option>
					<option value="ai" label="Anguilla">Anguilla</option>
					<option value="aq" label="Antarctica">Antarctica</option>
					<option value="ag" label="Antigua and Barbuda">Antigua and Barbuda</option>
					<option value="ar" label="Argentina">Argentina</option>
					<option value="am" label="Armenia">Armenia</option>
					<option value="aw" label="Aruba">Aruba</option>
					<option value="au" label="Australia">Australia</option>
					<option value="at" label="Austria">Austria</option>
					<option value="az" label="Azerbaijan">Azerbaijan</option>
					<option value="bs" label="Bahamas">Bahamas</option>
					<option value="bh" label="Bahrain">Bahrain</option>
					<option value="bd" label="Bangladesh">Bangladesh</option>
					<option value="bb" label="Barbados">Barbados</option>
					<option value="by" label="Belarus">Belarus</option>
					<option value="be" label="Belgium">Belgium</option>
					<option value="bz" label="Belize">Belize</option>
					<option value="bj" label="Benin">Benin</option>
					<option value="bm" label="Bermuda">Bermuda</option>
					<option value="bt" label="Bhutan">Bhutan</option>
					<option value="bo" label="Bolivia">Bolivia</option>
					<option value="ba" label="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
					<option value="bw" label="Botswana">Botswana</option>
					<option value="bv" label="Bouvet Island">Bouvet Island</option>
					<option value="br" label="Brazil">Brazil</option>
					<option value="bq" label="British Antarctic Territory">British Antarctic Territory</option>
					<option value="io" label="British Indian Ocean Territory">British Indian Ocean Territory</option>
					<option value="vg" label="British Virgin Islands">British Virgin Islands</option>
					<option value="bn" label="Brunei">Brunei</option>
					<option value="bg" label="Bulgaria">Bulgaria</option>
					<option value="bf" label="Burkina Faso">Burkina Faso</option>
					<option value="bi" label="Burundi">Burundi</option>
					<option value="kh" label="Cambodia">Cambodia</option>
					<option value="cm" label="Cameroon">Cameroon</option>
					<option value="ca" label="Canada">Canada</option>
					<option value="ct" label="Canton and Enderbury Islands">Canton and Enderbury Islands</option>
					<option value="cv" label="Cape Verde">Cape Verde</option>
					<option value="ky" label="Cayman Islands">Cayman Islands</option>
					<option value="cf" label="Central African Republic">Central African Republic</option>
					<option value="td" label="Chad">Chad</option>
					<option value="cl" label="Chile">Chile</option>
					<option value="cn" label="China">China</option>
					<option value="cx" label="Christmas Island">Christmas Island</option>
					<option value="cc" label="Cocos [Keeling] Islands">Cocos [Keeling] Islands</option>
					<option value="co" label="Colombia">Colombia</option>
					<option value="km" label="Comoros">Comoros</option>
					<option value="cg" label="Congo - Brazzaville">Congo - Brazzaville</option>
					<option value="cd" label="Congo - Kinshasa">Congo - Kinshasa</option>
					<option value="ck" label="Cook Islands">Cook Islands</option>
					<option value="cr" label="Costa Rica">Costa Rica</option>
					<option value="hr" label="Croatia">Croatia</option>
					<option value="cu" label="Cuba">Cuba</option>
					<option value="cy" label="Cyprus">Cyprus</option>
					<option value="cz" label="Czech Republic">Czech Republic</option>
					<option value="ci" label="Côte d’Ivoire">Côte d’Ivoire</option>
					<option value="dk" label="Denmark">Denmark</option>
					<option value="dj" label="Djibouti">Djibouti</option>
					<option value="dm" label="Dominica">Dominica</option>
					<option value="do" label="Dominican Republic">Dominican Republic</option>
					<option value="nq" label="Dronning Maud Land">Dronning Maud Land</option>
					<option value="dd" label="East Germany">East Germany</option>
					<option value="ec" label="Ecuador">Ecuador</option>
					<option value="eg" label="Egypt">Egypt</option>
					<option value="sv" label="El Salvador">El Salvador</option>
					<option value="gq" label="Equatorial Guinea">Equatorial Guinea</option>
					<option value="er" label="Eritrea">Eritrea</option>
					<option value="ee" label="Estonia">Estonia</option>
					<option value="et" label="Ethiopia">Ethiopia</option>
					<option value="fk" label="Falkland Islands">Falkland Islands</option>
					<option value="fo" label="Faroe Islands">Faroe Islands</option>
					<option value="fj" label="Fiji">Fiji</option>
					<option value="fi" label="Finland">Finland</option>
					<option value="fr" label="France">France</option>
					<option value="gf" label="French Guiana">French Guiana</option>
					<option value="pf" label="French Polynesia">French Polynesia</option>
					<option value="tf" label="French Southern Territories">French Southern Territories</option>
					<option value="fq" label="French Southern and Antarctic Territories">French Southern and Antarctic Territories</option>
					<option value="ga" label="Gabon">Gabon</option>
					<option value="gm" label="Gambia">Gambia</option>
					<option value="ge" label="Georgia">Georgia</option>
					<option value="de" label="Germany">Germany</option>
					<option value="gh" label="Ghana">Ghana</option>
					<option value="gi" label="Gibraltar">Gibraltar</option>
					<option value="gr" label="Greece">Greece</option>
					<option value="gl" label="Greenland">Greenland</option>
					<option value="gd" label="Grenada">Grenada</option>
					<option value="gp" label="Guadeloupe">Guadeloupe</option>
					<option value="gu" label="Guam">Guam</option>
					<option value="gt" label="Guatemala">Guatemala</option>
					<option value="gg" label="Guernsey">Guernsey</option>
					<option value="gn" label="Guinea">Guinea</option>
					<option value="gw" label="Guinea-Bissau">Guinea-Bissau</option>
					<option value="gy" label="Guyana">Guyana</option>
					<option value="ht" label="Haiti">Haiti</option>
					<option value="hm" label="Heard Island and McDonald Islands">Heard Island and McDonald Islands</option>
					<option value="hn" label="Honduras">Honduras</option>
					<option value="hk" label="Hong Kong SAR China">Hong Kong SAR China</option>
					<option value="hu" label="Hungary">Hungary</option>
					<option value="is" label="Iceland">Iceland</option>
					<option value="in" label="India">India</option>
					<option value="id" label="Indonesia">Indonesia</option>
					<option value="ir" label="Iran">Iran</option>
					<option value="iq" label="Iraq">Iraq</option>
					<option value="ie" label="Ireland">Ireland</option>
					<option value="im" label="Isle of Man">Isle of Man</option>
					<option value="il" label="Israel">Israel</option>
					<option value="it" label="Italy">Italy</option>
					<option value="jm" label="Jamaica">Jamaica</option>
					<option value="jp" label="Japan">Japan</option>
					<option value="je" label="Jersey">Jersey</option>
					<option value="jt" label="Johnston Island">Johnston Island</option>
					<option value="jo" label="Jordan">Jordan</option>
					<option value="kz" label="Kazakhstan">Kazakhstan</option>
					<option value="ke" label="Kenya">Kenya</option>
					<option value="ki" label="Kiribati">Kiribati</option>
					<option value="kw" label="Kuwait">Kuwait</option>
					<option value="kg" label="Kyrgyzstan">Kyrgyzstan</option>
					<option value="la" label="Laos">Laos</option>
					<option value="lv" label="Latvia">Latvia</option>
					<option value="lb" label="Lebanon">Lebanon</option>
					<option value="ls" label="Lesotho">Lesotho</option>
					<option value="lr" label="Liberia">Liberia</option>
					<option value="ly" label="Libya">Libya</option>
					<option value="li" label="Liechtenstein">Liechtenstein</option>
					<option value="lt" label="Lithuania">Lithuania</option>
					<option value="lu" label="Luxembourg">Luxembourg</option>
					<option value="mo" label="Macau SAR China">Macau SAR China</option>
					<option value="mk" label="Macedonia">Macedonia</option>
					<option value="mg" label="Madagascar">Madagascar</option>
					<option value="mw" label="Malawi">Malawi</option>
					<option value="my" label="Malaysia">Malaysia</option>
					<option value="mv" label="Maldives">Maldives</option>
					<option value="ml" label="Mali">Mali</option>
					<option value="mt" label="Malta">Malta</option>
					<option value="mh" label="Marshall Islands">Marshall Islands</option>
					<option value="mq" label="Martinique">Martinique</option>
					<option value="mr" label="Mauritania">Mauritania</option>
					<option value="mu" label="Mauritius">Mauritius</option>
					<option value="yt" label="Mayotte">Mayotte</option>
					<option value="fx" label="Metropolitan France">Metropolitan France</option>
					<option value="mx" label="Mexico">Mexico</option>
					<option value="fm" label="Micronesia">Micronesia</option>
					<option value="mi" label="Midway Islands">Midway Islands</option>
					<option value="md" label="Moldova">Moldova</option>
					<option value="mc" label="Monaco">Monaco</option>
					<option value="mn" label="Mongolia">Mongolia</option>
					<option value="me" label="Montenegro">Montenegro</option>
					<option value="ms" label="Montserrat">Montserrat</option>
					<option value="ma" label="Morocco">Morocco</option>
					<option value="mz" label="Mozambique">Mozambique</option>
					<option value="mm" label="Myanmar [Burma]">Myanmar [Burma]</option>
					<option value="na" label="Namibia">Namibia</option>
					<option value="nr" label="Nauru">Nauru</option>
					<option value="np" label="Nepal">Nepal</option>
					<option value="nl" label="Netherlands">Netherlands</option>
					<option value="an" label="Netherlands Antilles">Netherlands Antilles</option>
					<option value="nt" label="Neutral Zone">Neutral Zone</option>
					<option value="nc" label="New Caledonia">New Caledonia</option>
					<option value="nz" label="New Zealand">New Zealand</option>
					<option value="ni" label="Nicaragua">Nicaragua</option>
					<option value="ne" label="Niger">Niger</option>
					<option value="ng" label="Nigeria">Nigeria</option>
					<option value="nu" label="Niue">Niue</option>
					<option value="nf" label="Norfolk Island">Norfolk Island</option>
					<option value="kp" label="North Korea">North Korea</option>
					<option value="vd" label="North Vietnam">North Vietnam</option>
					<option value="mp" label="Northern Mariana Islands">Northern Mariana Islands</option>
					<option value="no" label="Norway">Norway</option>
					<option value="om" label="Oman">Oman</option>
					<option value="pc" label="Pacific Islands Trust Territory">Pacific Islands Trust Territory</option>
					<option value="pk" label="Pakistan">Pakistan</option>
					<option value="pw" label="Palau">Palau</option>
					<option value="ps" label="Palestinian Territories">Palestinian Territories</option>
					<option value="pa" label="Panama">Panama</option>
					<option value="pz" label="Panama Canal Zone">Panama Canal Zone</option>
					<option value="pg" label="Papua New Guinea">Papua New Guinea</option>
					<option value="py" label="Paraguay">Paraguay</option>
					<option value="yd" label="People's Democratic Republic of Yemen">People's Democratic Republic of Yemen</option>
					<option value="pe" label="Peru">Peru</option>
					<option value="ph" label="Philippines">Philippines</option>
					<option value="pn" label="Pitcairn Islands">Pitcairn Islands</option>
					<option value="pl" label="Poland">Poland</option>
					<option value="pt" label="Portugal">Portugal</option>
					<option value="pr" label="Puerto Rico">Puerto Rico</option>
					<option value="qa" label="Qatar">Qatar</option>
					<option value="ro" label="Romania">Romania</option>
					<option value="ru" label="Russia">Russia</option>
					<option value="rw" label="Rwanda">Rwanda</option>
					<option value="re" label="Réunion">Réunion</option>
					<option value="bl" label="Saint Barthélemy">Saint Barthélemy</option>
					<option value="sh" label="Saint Helena">Saint Helena</option>
					<option value="kn" label="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
					<option value="lc" label="Saint Lucia">Saint Lucia</option>
					<option value="mf" label="Saint Martin">Saint Martin</option>
					<option value="pm" label="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
					<option value="vc" label="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
					<option value="ws" label="Samoa">Samoa</option>
					<option value="sm" label="San Marino">San Marino</option>
					<option value="sa" label="Saudi Arabia">Saudi Arabia</option>
					<option value="sn" label="Senegal">Senegal</option>
					<option value="rs" label="Serbia">Serbia</option>
					<option value="cs" label="Serbia and Montenegro">Serbia and Montenegro</option>
					<option value="sc" label="Seychelles">Seychelles</option>
					<option value="sl" label="Sierra Leone">Sierra Leone</option>
					<option value="sg" label="Singapore">Singapore</option>
					<option value="sk" label="Slovakia">Slovakia</option>
					<option value="si" label="Slovenia">Slovenia</option>
					<option value="sb" label="Solomon Islands">Solomon Islands</option>
					<option value="so" label="Somalia">Somalia</option>
					<option value="za" label="South Africa">South Africa</option>
					<option value="gs" label="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
					<option value="kr" label="South Korea">South Korea</option>
					<option value="es" label="Spain">Spain</option>
					<option value="lk" label="Sri Lanka">Sri Lanka</option>
					<option value="sd" label="Sudan">Sudan</option>
					<option value="sr" label="Suriname">Suriname</option>
					<option value="sj" label="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
					<option value="sz" label="Swaziland">Swaziland</option>
					<option value="se" label="Sweden">Sweden</option>
					<option value="ch" label="Switzerland">Switzerland</option>
					<option value="sy" label="Syria">Syria</option>
					<option value="st" label="São Tomé and Príncipe">São Tomé and Príncipe</option>
					<option value="tw" label="Taiwan">Taiwan</option>
					<option value="tj" label="Tajikistan">Tajikistan</option>
					<option value="tz" label="Tanzania">Tanzania</option>
					<option value="th" label="Thailand">Thailand</option>
					<option value="tl" label="Timor-Leste">Timor-Leste</option>
					<option value="tg" label="Togo">Togo</option>
					<option value="tk" label="Tokelau">Tokelau</option>
					<option value="to" label="Tonga">Tonga</option>
					<option value="tt" label="Trinidad and Tobago">Trinidad and Tobago</option>
					<option value="tn" label="Tunisia">Tunisia</option>
					<option value="tr" label="Turkey">Turkey</option>
					<option value="tm" label="Turkmenistan">Turkmenistan</option>
					<option value="tc" label="Turks and Caicos Islands">Turks and Caicos Islands</option>
					<option value="tv" label="Tuvalu">Tuvalu</option>
					<option value="um" label="U.S. Minor Outlying Islands">U.S. Minor Outlying Islands</option>
					<option value="pu" label="U.S. Miscellaneous Pacific Islands">U.S. Miscellaneous Pacific Islands</option>
					<option value="vi" label="U.S. Virgin Islands">U.S. Virgin Islands</option>
					<option value="ug" label="Uganda">Uganda</option>
					<option value="ua" label="Ukraine">Ukraine</option>
					<option value="su" label="Union of Soviet Socialist Republics">Union of Soviet Socialist Republics</option>
					<option value="ae" label="United Arab Emirates">United Arab Emirates</option>
					<option value="gb" label="United Kingdom">United Kingdom</option>
					<option value="us" label="United States">United States</option>
					<option value="uy" label="Uruguay">Uruguay</option>
					<option value="uz" label="Uzbekistan">Uzbekistan</option>
					<option value="vu" label="Vanuatu">Vanuatu</option>
					<option value="va" label="Vatican City">Vatican City</option>
					<option value="ve" label="Venezuela">Venezuela</option>
					<option value="vn" label="Vietnam">Vietnam</option>
					<option value="wk" label="Wake Island">Wake Island</option>
					<option value="wf" label="Wallis and Futuna">Wallis and Futuna</option>
					<option value="eh" label="Western Sahara">Western Sahara</option>
					<option value="ye" label="Yemen">Yemen</option>
					<option value="zm" label="Zambia">Zambia</option>
					<option value="zw" label="Zimbabwe">Zimbabwe</option>
					<option value="ax" label="Åland Islands">Åland Islands</option>
				</select>
			</div>
		</div>

		<div class="row form-group">

			<div class=" controls col-sm-6 kindsInputs">
				<div>
					<label class="control-label" for="flying">
						Kind of flight
					</label>
				</div>

				<div>
					<input type="checkbox" name="hike" value="1" id="hikeBox"></input>
						<label for="hikeBox"> <img src="assets/img/flying/hike40.png" /> Hike and fly</label><br />
					<input type="checkbox" name="thermal" value="1" id="thermalBox"></input>
						<label for="thermalBox"> <img src="assets/img/flying/thermal40.png" /> Flying 'local' in thermals</label><br />
					<input type="checkbox" name="soaring" value="1" id="soaringBox"></input>
						<label for="soaringBox"> <img src="assets/img/flying/soaring40.png" /> Soaring</label><br />
					<input type="checkbox" name="xc" value="1" id="xcBox"></input>
						<label for="xcBox"> <img src="assets/img/flying/xc40.png" /> XC - great cross country potential</label><br />
					<input type="checkbox" name="flatland" value="1" id="flatlandBox"></input>
					 	<label for="flatlandBox"> <img src="assets/img/flying/flatland40.png" /> Flatland XC Flying</label><br />
					<input type="checkbox" name="winch" value="1" id="winchBox"></input>
						<label for="winchBox"> <img src="assets/img/flying/winch40.png" /> Winch start</label><br />
					<input type="checkbox" name="hanggliding" value="1" id="hangglidingBox"></input>
						<label for="hangglidingBox"> <img src="assets/img/flying/hanggliding40.png" /> Hanggliding site</label><br />
				</div>
			</div>

			<div class=" controls col-sm-6 windsInputs">

				<label class="control-label " for="wind">
					Orientations
				</label>

				<div class="controls ">
					<div id="main">
						<div class="circle" id="n"  style="top: 80px; left: 150px;">N</div>
						<div class="circle" id="ne" style="top: 101px; left: 199px;">NE</div>
						<div class="circle" id="e"  style="top: 150px; left: 220px;">E</div>
						<div class="circle" id="se" style="top: 199px; left: 199px;">SE</div>
						<div class="circle" id="s"  style="top: 220px; left: 150px;">S</div>
						<div class="circle" id="sw" style="top: 199px; left: 101px;">SW</div>
						<div class="circle" id="w"  style="top: 150px; left: 80px;">W</div>
						<div class="circle" id="nw" style="top: 101px; left: 101px;">NW</div>
						<input type="checkbox" class="hidden-checkbox" name="n"  value="1" />
						<input type="checkbox" class="hidden-checkbox" name="ne" value="1" />
						<input type="checkbox" class="hidden-checkbox" name="e"  value="1" />
						<input type="checkbox" class="hidden-checkbox" name="se" value="1" />
						<input type="checkbox" class="hidden-checkbox" name="s"  value="1" />
						<input type="checkbox" class="hidden-checkbox" name="sw" value="1" />
						<input type="checkbox" class="hidden-checkbox" name="w"  value="1" />
						<input type="checkbox" class="hidden-checkbox" name="nw" value="1" />
					</div>
				</div>
			</div>
		</div>

		<div class="row form-group">
			<div class="controls col-sm-12 text-center">
				<input class="btn btn-primary" value="Apply filters !" onclick="updateFilters(); $('#filtersModal').modal('hide');"></input>
				<input class="btn btn-warning" value="Delete all filters" onclick="resetFilters();  $('#filtersModal').modal('hide');"></input>
			</div>
		</div>

		<div>
			<label class="control-label">
				Results legend :
			</label>
			<ul class=".list-group-flush">
				<li class="list-group-item"> <img src="assets/img/site1.png" width="18px" /> Site matches the filters </li>
				<li class="list-group-item"> <img src="assets/img/site0.png" width="18px" /> Site doesn't match the filters </li>
				<li class="list-group-item"> <img src="assets/img/site-1.png" width="18px" /> We don't have enough information on site to state (feel free to add relevant information... ;) ) </li>
			</ul>

		</div>

	</div>

</form>

<script>
	$('#countrySelect option[value="'+filterCountry+'"]').attr('selected', true);


	$.each(filterKinds, function(index, item) {
		console.log(item + " found");
		$("input[name="+item+"]").prop('checked', true);
	});

	$.each(filterWinds, function(index, item) {
		console.log(item + " found");
		$("#"+item).css('background', 'lightgreen');
		$("input[name="+item+"]").prop('checked', true);
	});

	$('.circle').on('click', function () {
		var direction = $(this).attr("id");
		if ($.inArray(direction, filterWinds) >= 0) {
//			console.log("found");
			$(this).css("background", "white");
			filterWinds.splice( $.inArray(direction, filterWinds), 1 );
			$("input[name="+direction+"]").prop('checked', false);
		} else {
//			console.log("not found");
			$(this).css("background", "lightgreen");
			filterWinds.push( direction );
			$("input[name="+direction+"]").prop('checked', true);
		}
		console.log("winds : "+filterWinds);
	});




///  update Filters js Arrays as the form is validated
	function updateFilters(){

		hasFilters = false;
		filterHTMLString = "<ul style='padding-left: 14px;margin-bottom: 0px;'>";

		//  update country
		var selectedCountry = $('#countrySelect').val();
		var countryHasChanged = false;  // used to pan the map or not
		if (selectedCountry != "all") {
			if ( selectedCountry != filterCountry )
				countryHasChanged = true;
			// map.setZoom(1);
			filterCountry = selectedCountry;
			zoomOnFilteredItems = false;
			hasCountryFilter= true;
			filterHTMLString += "<li>country : <img src=\"assets/img/flag/"+filterCountry+".png\" /> "+ filterCountry +"</li>" ;

		} else {
			hasCountryFilter = false;
			filterCountry = "all";
		}

		//  update kinds filters variables
		hasKindsFilter = false;
		filterKinds      = [];
		$.each([ 'hike', 'thermal', 'xc', 'flatland', 'soaring', 'winch', 'hanggliding' ], function( index, val ) {
			if (  $("input[name="+val+"]").is(":checked" ) ) {
				filterKinds.push(val);
				filterHTMLString += "<li>flying : <img src=\'assets/img/flying/"+val+"25.png\'> "+val+" </li>";
				hasKindFilter = true;
			}
		});

		//  update winds filters variables (array is updated on clicks of the buttons : no need to do it here
		hasWindFilter = false;
		$.each( filterWinds , function( index, val ) {
				filterHTMLString += "<li>wind : "+val+" </li>";
				hasWindFilter = true;
		});

		if (hasKindFilter || hasWindFilter || hasCountryFilter) {
			hasFilters = true;
		}

		filterHTMLString += "</ul>";


		if (hasFilters){
			updateMapFilterControl() ;
		} else {
			resetFilters();
		}

		refreshSites = true;



		if (countryHasChanged) {
			var countryBounds = [];
			countryHasChanged = false; // we reset the country chang flag..
			$.getJSON("https://nominatim.openstreetmap.org/",
				{
					async: false,
					country : filterCountry,
					format : 'json'
				},
				function(data){})
			.done( function (data){
				 north = data[0].boundingbox[1];
				 south = data[0].boundingbox[0];
				 west = data[0].boundingbox[2];
				 east = data[0].boundingbox[3];
				countryBounds =  [[south, west], [north, east]];
				console.log ( countryBounds );
				map.fitBounds(countryBounds);
			});

		}

		if (hasWindFilter || hasKindFilter ) {
			//filterSites();
			updateSites();
		}
	}

	$('#filtersModal').on('hidden.bs.modal', function () {
		if ( ! hasFilters ) myToggleFilters.toggle(false, false, true);
	});

</script>

<style>
	#main {
	    position: relative;
	    margin: 0 auto;
	    /*left: 10%;
	    top: -5%;
	    */
	    transform: translate(-20%, -20%);
	    /*border-radius: 50%;
	    */
	    width: 300px;
	    height: 300px;
	}

	.circle {
	    position: absolute;
	    background: white none repeat scroll 0% 0%;
	    color: black;
	    text-align: center;
	    border-radius: 50%;
	    transition: transform 0.2s ease 0s;
	    width: 40px;
	    height: 40px;
	    line-height: 37px;
	    border: 1px solid lightgray;
	    font-size: small;
	    font-weight: bold;
	    cursor: pointer;
	}

	.hidden-checkbox {
		margin-left: 9999999px;
		opacity: 0;
	}
</style>
