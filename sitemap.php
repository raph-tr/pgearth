<?php
include "config/connection.php";

// get sites id
$sites = mysqli_query($bdd, "SELECT id FROM site WHERE 1");

$isos = mysqli_query($bdd, "SELECT iso FROM country WHERE 1");

// Affichage
header('Content-Type: text/xml; charset=UTF-8');
echo '<?xml version="1.0" encoding="UTF-8"?>
';
?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?php
	echo "
	<url>
		<loc>http://".$_SERVER['SERVER_NAME']."</loc>
		<changefreq>monthly</changefreq>
		<priority>1</priority>
	</url>
	<url>
		<loc>http://".$_SERVER['SERVER_NAME']."/?view=about</loc>
		<changefreq>monthly</changefreq>
		<priority>0.9</priority>
	</url>";


// Boucle qui liste les pays
while($val = mysqli_fetch_array($isos)){
	echo "
	<url>
		<loc>http://".$_SERVER['SERVER_NAME']."/?country=".strtolower($val['iso'])."</loc>
		<changefreq>monthly</changefreq>
		<priority>0.85</priority>
	</url>";
}


// Boucle qui liste les sites
while($val = mysqli_fetch_array($sites)){
	echo "
	<url>
		<loc>http://".$_SERVER['SERVER_NAME']."/?site=".$val['id']."</loc>
		<changefreq>monthly</changefreq>
		<priority>0.8</priority>
	</url>";
}

?>
</urlset>
