<?php
header("Content-Type: text/xml");
include '../config/connectionReadOnly.php';

$latref = 3.14159265 * $_GET['lat']   / 180;
$longref= 3.14159265 * $_GET['lng']   / 180;
$distance = $_GET['distance'];
$compteur=0;
$style='brief';
if (isset ($_GET['style'] )) $style= $_GET['style'] ; 

$query = "SELECT * FROM site LEFT JOIN country ON site.country=country.id;";
$result = mysqli_query($query);
	while ($val=mysqli_fetch_array($result)){
		$lat = 3.14159265 * $val['latd']   / 180;
		$long= 3.14159265 * $val['longd']   / 180;
		$d = round(6366 * acos(cos($lat) * cos($latref) * cos($longref-$long) + sin($lat) * sin($latref)),2);
		if ($d<$distance){
			$nom[$compteur]=$val['name'];
			$distance_meter[$compteur]=$d * 1000;
			$id_site[$compteur]=$val['id'];
			$takeoff_altitude[$compteur]=$val['altitude_takeoff'];
			$takeoff_description[$compteur]=$val['deco'];
			$takeoff_comments[$compteur]=$val['comments'];
			$takeoff_going_there[$compteur]=$val['access'];
			$takeoff_flight_rules[$compteur]=$val['rules'];
			$takeoff_contacts[$compteur]=$val['contact'];
			$weather[$compteur]=$val['weather'];
			$longit[$compteur]=$val['lng'];
			$latit[$compteur]=$val['lat'];
			$winch[$compteur]=$val['winch'];
			$web[$compteur]=$val['web'];
			$iso[$compteur]=$val['iso'];
			//$region[$compteur]=$val['nom_region_en'];
			$paragliding[$compteur]=$val['paragliding'];
			$hanggliding[$compteur]=$val['hanggliding'];
			$condition[$compteur]=$val['condition'];
			$orientationN[$compteur] = $val['N'] ;
			$orientationNE[$compteur] = $val['NE'] ;
			$orientationE[$compteur] = $val['E'] ;
			$orientationSE[$compteur] = $val['SE'] ;
			$orientationS[$compteur] = $val['S'] ;
			$orientationSW[$compteur] = $val['SW'] ;
			$orientationW[$compteur] = $val['W'] ;
			$orientationNW[$compteur] = $val['NW'] ;
			$lat_takeoff_parking[$compteur] = $val['takeoff_parking_lat'] ;
			$lng_takeoff_parking[$compteur] = $val['takeoff_parking_lng'] ;
			$description_takeoff_parking[$compteur] = $val['takeoff_parking_description'] ;
			$lat_landing[$compteur] = $val['lata'] ;
			$lng_landing[$compteur] = $val['longa'] ;
			$altitude_landing[$compteur] = $val['altitude_landing'] ;
			$description_landing[$compteur] = $val['landing'] ;
			$lat_landing_parking[$compteur] = $val['landing_parking_lat'] ;
			$lng_landing_parking[$compteur] = $val['landing_parking_lng'] ;
			$description_landing_parking[$compteur] = $val['landing_parking_description'] ;
			//echo $compteur;
			/*$ranking[$compteur]=$val['ranking'];
			$nombre_votes_toto[$compteur]=$val['nombre_votes'];
			$orientation[$compteur]=$val['orientation'];
			$original_compteur[$compteur]= $compteur;*/
			$compteur++;
                }
	}

$limit= count($nom);
if (isset ($_GET['limit'])) { $limit = min ($_GET['limit'], count ($nom));}



//echo count($nom);

if ($limit>0){
	array_multisort($distance_meter,SORT_ASC,$nom,$iso,$takeoff_altitude,$takeoff_description, $winch,
		$latit,$longit,$id_site,$region,$paragliding,$hanggliding,$condition,$takeoff_comments,$weather,
		$takeoff_going_there, $takeoff_flight_rules, $takeoff_contacts, $web,
		$orientationN,$orientationNE,$orientationE,$orientationSE,$orientationS,$orientationSW,$orientationW,$orientationNW,
		$lat_takeoff_parking, $lng_takeoff_parking, $description_takeoff_parking,
		$lat_landing_parking, $lng_landing_parking, $description_landing_parking,
		$lat_landing, $lng_landing, $altitude_landing, $description_landing
		);
}
echo "<"."?xml  version=\"1.0\"  encoding=\"UTF-8\" ?".">
<search>";

for ($k=0;$k<$limit;$k++){

  $N = 0; if ($orientationN[$k]=="possible") $N = 1; if ($orientationN[$k]=="perfect") $N = 2; 
  $NE = 0; if ($orientationNE[$k]=="possible") $NE = 1; if ($orientationNE[$k]=="perfect") $NE = 2; 
  $E = 0; if ($orientationE[$k]=="possible") $E = 1; if ($orientationE[$k]=="perfect") $E = 2; 
  $SE = 0; if ($orientationSE[$k]=="possible") $SE = 1; if ($orientationSE[$k]=="perfect") $SE = 2; 
  $S = 0; if ($orientationS[$k]=="possible") $S = 1; if ($orientationS[$k]=="perfect") $S = 2; 
  $SW = 0; if ($orientationSW[$k]=="possible") $SW = 1; if ($orientationSW[$k]=="perfect") $SW = 2; 
  $W = 0; if ($orientationW[$k]=="possible") $W = 1; if ($orientationW[$k]=="perfect") $W = 2; 
  $NW = 0; if ($orientationNW[$k]=="possible") $NW = 1; if ($orientationNW[$k]=="perfect") $NW = 2; 
 
  $soaring = 0 ;  if (strpos($condition[$k], "soar") !== false) $soaring=1;
  $thermals = 0 ;  if (strpos($condition[$k], "thermal") !== false) $thermals=1;
  if ($condition[$k]=="") {$soaring="na"; $thermals="na";}

	echo "
<takeoff>
    <name>".$nom[$k]."</name>
    <distance>".$distance_meter[$k]."</distance>
    <countryCode>".$iso[$k]."</countryCode>
    <area>".$region[$k]."</area>
    <takeoff_altitude>".$takeoff_altitude[$k]."</takeoff_altitude>
    <takeoff_description><![CDATA[".$takeoff_description[$k]."]]></takeoff_description>
    <lat>".$latit[$k]."</lat>
    <lng>".$longit[$k]."</lng>
    <paragliding>1</paragliding>
    <hanggliding>".$hanggliding[$k]."</hanggliding>
    <thermals>".$thermals."</thermals>
    <soaring>".$soaring."</soaring>
    <winch>".$winch[$k]."</winch>
    <orientations>
	<N>".$N."</N>
	<NE>".$NE."</NE>
	<E>".$E."</E>
	<SE>".$SE."</SE>
	<S>".$S."</S>
	<SW>".$SW."</SW>
	<W>".$W."</W>
	<NW>".$NW."</NW>
    </orientations>
    <pe_link>http://www.paraglidingearth.com/en-html/index.php?site=".$id_site[$k]."</pe_link>
    <pe_site_id>".$id_site[$k]."</pe_site_id>";

	if ($style=='detailled') {
		echo "
			<comments><![CDATA[".$takeoff_comments[$k]."]]></comments>
			<weather><![CDATA[".$weather[$k]."]]></weather>
			<going_there><![CDATA[".$takeoff_going_there[$k]."]]></going_there>
			<flight_rules><![CDATA[".$takeoff_flight_rules[$k]."]]></flight_rules>
			<contacts><![CDATA[". $takeoff_contacts[$k]."]]></contacts> 
			<related_website><![CDATA[". $web[$k]."]]></related_website> ";

		if ($lat_takeoff_parking[$k]<>0 and $lng_takeoff_parking[$k]<>0) {
		echo "
			<parking>
				<parking_lat>".$lat_takeoff_parking[$k]."</parking_lat>
				<parking_lng>".$lng_takeoff_parking[$k]."</parking_lng>
				<parking_description><![CDATA[".$description_takeoff_parking[$k]."]]></parking_description>
			</parking>";
		}
	}
	echo "
</takeoff>";

	if ($style=='detailled') {
		if ($lat_landing[$k]<>0 and $lng_landing[$k]<>0) {
		echo "
	<landing>
		<landing_name>".$nom[$k]."</landing_name>
		<landing_countryCode>".$iso[$k]."</landing_countryCode>
		<landing_area>".$region[$k]."</landing_area>
		<landing_lat>".$lat_landing[$k]."</landing_lat>
		<landing_lng>".$lng_landing[$k]."</landing_lng>
		<landing_altitude>".$altitude_landing[$k]."</landing_altitude>
		<landing_description><![CDATA[".$description_landing[$k]."]]></landing_description>
		<landing_pe_site_id>".$id_site[$k]."</landing_pe_site_id>";

		if ($lat_landing_parking[$k]<>0 and $lng_landing_parking[$k]<>0) {
			echo "
				<landing_parking>
					<landing_parking_lat>".$lat_landing_parking[$k]."</landing_parking_lat>
					<landing_parking_lng>".$lng_landing_parking[$k]."</landing_parking_lng>
					<landing_parking_description><![CDATA[".$description_landing_parking[$k]."]]></landing_parking_description>
				</landing_parking>";
			}

			echo "

			</landing>";
		}
	}
}
echo "</search>";
?>
