<?php
header("Content-type: application/json; charset=utf-8");
include '../../config/connectionReadOnly.php';


$since = $_GET['since'];

function validateDate($date, $format = 'Y-m-d')
{
    $d = DateTime::createFromFormat($format, $date);
    // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
    return $d && $d->format($format) === $date;
}

if ( validateDate($since) ) {
	$query = "SELECT  sites_users.item_id , site.name, site.iso , MAX(sites_users.date) as date
				  FROM sites_users LEFT JOIN site ON item_id = site.id 
                    WHERE item_type = 'site' AND sites_users.date > '".$since."' 
                       GROUP BY item_id
                         ORDER BY date DESC";
	
	//echo $query;

	$result = mysqli_query($bdd, $query);
	$results = array();

	while ( $val = mysqli_fetch_array($result) ) {

		//$hit = array(
				$hit['name'] = $val['name']; 		  
				$hit['id'] = $val['item_id'];
				$hit['iso'] =  $val['iso'];
				$hit['date'] = $val['date'];
		//    );
				//print_r($hit);

		 array_push($results, $hit);

	}
} else {
	$results = "Please enter a valid date (in 'yyyy-mm-dd' format) as the 'since' parameter of your request.";
}

echo json_encode($results);

?>
