<?php
header('Content-type: application/json; charset=utf-8');
include '../../config/connectionReadOnly.php';

$timestamp = $_GET['timestamp'] ;
$dateEdited = $_GET['date'];

$count = 0;

$style='brief';
if (isset ($_GET['style'] )) $style = $_GET['style'] ; 

if (isset ($_GET['timestamp'] )) {
	$timestamp = $_GET['timestamp'] ; 
	$whereTime = " UNIX_TIMESTAMP(sites_users.date) >= ".$timestamp." ";
} else {
	$whereTime = " `sites_users`.`date` >= '".$dateEdited."' ";
}

$query = "SELECT site.* , max(`sites_users`.`date`) AS last_edit 
			FROM site 
				LEFT JOIN `sites_users` ON site.id = sites_users.item_id 
					WHERE `item_type` LIKE 'site' and $whereTime
						GROUP BY item_id 
							ORDER BY last_edit 
								DESC";

//echo $query;

$result = mysqli_query($bdd, $query);

include "geoJsonSitesList.php";

?>
