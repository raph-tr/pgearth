<?php include '../config/connectionReadOnly.php'; ?>

<html>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<head>
	</head>
	
	<body>
			<?php 
			$fields = [name];//, takeoff, landing, comments, access];
			
			foreach ($fields as $field) {
				$query = "select id, $field, LENGTH(".$field.") as length, CHAR_LENGTH(".$field.") as char_length from site where LENGTH(".$field.") != CHAR_LENGTH(".$field.");";
				echo $query;
					$result = mysqli_query($bdd, $query);
					$i=0;
					echo "		<h2>Field : ".$field." </h2>
					<table>
					<tr>
						<td>raw from db</td>
						<td>suggested correction</td>
						<td>utf8_encoded</td>
						<td>utf8_encoded twice</td>
						<td>utf8_decoded</td>
						<td>utf8_decoded twice</td>
<!--					<td>corrected</td>
-->						<td>site id</td>
						<td>site page</td>
					</tr>
		";
					while ($val = mysqli_fetch_array($result)){
						
						$fieldContent = $val[$field];
						$suggested = $fieldContent;
						
						 
//						if ((utf8_encode($field) <> $field or utf8_decode($field) <> $field)) {
							$i++;
							echo "
		<tr id='".$val['id']."-row'>
			<td id='".$val['id']."-name'>
				<a href=\"javascript:updateName(".$val['id'].", '".$fieldContent."', '$field', '".$val['id']."-name', '".$val['id']."-row');\" >".$fieldContent."</a>
			</td>
			<td id='".$val['id']."-suggested'>";
						for ($loop = $val['char_length']; $loop < $val['length']; $loop++) {
							$suggested = utf8_encode($suggested);
							//echo $suggested." - ";
						}

			echo "".$val['char_length']." / ".$val['length']." / ".$loop." - <a href=\"javascript:updateName(".$val['id'].", '".$suggested."', '$field', '".$val['id']."-encoded', '".$val['id']."-row');\" >".utf8_encode($fieldContent)."</a>
			</td>
			<td id='".$val['id']."-encoded'>
				<a href=\"javascript:updateName(".$val['id'].", '".utf8_encode($fieldContent)."', '$field', '".$val['id']."-encoded', '".$val['id']."-row');\" >".utf8_encode($fieldContent)."</a>
			</td>
			<td id='".$val['id']."-encoded-twice'>
				<a href=\"javascript:updateName(".$val['id'].", '".utf8_encode(utf8_encode($fieldContent))."', '$field', '".$val['id']."-encoded', '".$val['id']."-row');\" >".utf8_encode(utf8_encode($fieldContent))."</a>
			</td>
			<td id='".$val['id']."-decoded'>
				<a href=\"javascript:updateName(".$val['id'].", '".utf8_decode($fieldContent)."', '$field', '".$val['id']."-decoded', '".$val['id']."-row');\" >".utf8_decode($fieldContent)."</a>
			</td>
			<td id='".$val['id']."-decoded'>
				<a href=\"javascript:updateName(".$val['id'].", '".utf8_decode(utf8_decode($fieldContent))."', '$field', '".$val['id']."-decoded-twice', '".$val['id']."-row');\" >".utf8_decode(utf8_decode($fieldContent))."</a>
			</td>";
/*			<td id='".$val['id']."-corrected'>
				<a href=\"javascript:updateName(".$val['id'].", '".$val['corrected_name']."', '".$val['id']."-corrected', '".$val['id']."-row');\" >".$val['corrected_name']."</a>
			</td>
*/			echo "<td id='".$val['id']."-id'>
				".$val['id']."
			</td>
			<td id='".$val['id']."-link'>
				<a href='../?site=".$val['id']."'>&rarr;</a>
			</td>
		</tr>
		";
/*							$query_replace = "UPDATE `pge`.`site` SET `shows_as_utf8_correct` = 0 WHERE `site`.`id` =".$val['id']." LIMIT 1 ;";
							$result_replace = mysqli_query($bdd, $query_replace);

						} else {
							$query_replace = "UPDATE `pge`.`site` SET `shows_as_utf8_correct` = 1 WHERE `site`.`id` =".$val['id']." LIMIT 1 ;";
							$result_replace = mysqli_query($bdd, $query_replace);
						}
*/
					}
					echo "</table>";
		
				echo $i." changes suggested<hr />";
				
			}
?>
	</body>
	<script>
/*		function ajax(id, newName) {
		  var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		  };
		  xhttp.open("GET", "updateNamesUtf8.php?newName="+newName+"&id="+id, true);
		  xhttp.send();
		}
*/		
		function updateName(id, content, field, cellId, rowId){
		  var row = document.getElementById(rowId);
//			ajax(id, newName);
		  var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() { };
		  xhttp.open("GET", "updateNamesUtf8.php?field="+field+"&content="+content+"&id="+id, true);
		  xhttp.send();
		  row.style.display="none";
		}
	</script>
</html>
